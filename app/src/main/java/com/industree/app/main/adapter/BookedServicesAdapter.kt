package com.industree.app.main.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.BookingListingDataItem
import com.industree.app.main.activity.ReviewOrderActivity
import com.whinc.widget.ratingbar.RatingBar
import java.util.ArrayList
import kotlin.math.roundToInt

class BookedServicesAdapter(
    var mActivity: Activity,
    var bookingServiceItemList: ArrayList<BookingListingDataItem?>?,
    var loggedInUserID: String,

) : RecyclerView.Adapter<BookedServicesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.booked_services_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.nameBookedServices.text= bookingServiceItemList!!.get(position)!!.brandName
        holder.addressBookedServices.text=bookingServiceItemList!!.get(position)!!.address
        holder.distanceBookedServices.text=bookingServiceItemList!!.get(position)!!.distance
        if(!bookingServiceItemList!!.get(position)!!.count.equals("0")) {
            holder.textBookedServices.text = "(" + bookingServiceItemList!!.get(position)!!.count + ")"
        }
        holder.exp_rating.count=bookingServiceItemList!!.get(position)!!.rating!!.toDouble().roundToInt()
        Glide.with(mActivity!!)
            .load(bookingServiceItemList?.get(position)?.brandImage)
            .centerCrop()
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(holder.BookedServicesImage)

        holder.bookedServiceLL.setOnClickListener {
            val intent = Intent( mActivity, ReviewOrderActivity::class.java)
            intent.putExtra("user_id",loggedInUserID)
            intent.putExtra("booking_id",bookingServiceItemList!!.get(position)!!.bookingId)
            intent.putExtra("brand_name",bookingServiceItemList!!.get(position)!!.brandName)
            intent.putExtra("brand_image",bookingServiceItemList!!.get(position)!!.brandImage)
            intent.putExtra("address",bookingServiceItemList!!.get(position)!!.address)
            intent.putExtra("totalamount",bookingServiceItemList!!.get(position)!!.totalAmount)
            intent.putExtra("type","2")
            intent.putExtra("email",bookingServiceItemList!!.get(position)!!.email)
            mActivity!!.startActivity(intent)
        }
    }
    override fun getItemCount(): Int {
        return bookingServiceItemList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var BookedServicesImage: ImageView
        var BookedServicesImage1: LinearLayout
        var nameBookedServices: TextView
        var distanceBookedServices: TextView
        var addressBookedServices: TextView
        var cardView: CardView
        var textBookedServices:TextView
        var bookedServiceLL:LinearLayout
        var exp_rating:RatingBar
        init {
            BookedServicesImage = itemView.findViewById(R.id.bookedServicesIV)
            BookedServicesImage1 = itemView.findViewById(R.id.bookedServicesIV1)
            nameBookedServices = itemView.findViewById(R.id.nameBookedServicesTV)
            distanceBookedServices = itemView.findViewById(R.id.distanceBookedServicesTV)
            addressBookedServices = itemView.findViewById(R.id.addressBookedServicesTV)
            textBookedServices = itemView.findViewById(R.id.textBookedServicesTV)
            cardView = itemView.findViewById(R.id.bookedServicesCV)
            bookedServiceLL=itemView.findViewById(R.id.bookedServiceLL)
            exp_rating=itemView.findViewById(R.id.exp_rating)
        }
    }

}