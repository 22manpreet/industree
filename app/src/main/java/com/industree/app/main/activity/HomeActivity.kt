package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.Nullable
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingClientStateListener
import com.android.billingclient.api.BillingResult
import com.android.billingclient.api.PurchasesUpdatedListener
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.SkuDetails
import com.anjlab.android.iab.v3.TransactionDetails
import com.industree.app.R
import com.industree.app.main.Model.CheckPlanModel
import com.industree.app.main.Model.SubscriptionModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.fragment.*
import com.industree.app.main.utils.*
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class HomeActivity : BaseActivity() , BillingProcessor.IBillingHandler {
    @BindView(R.id.imgHomeIV)
    lateinit var actionHome: ImageView

    @BindView(R.id.imgFavouriteIV)
    lateinit var actionExplore: ImageView

    @BindView(R.id.imgSubscriptionIV)
    lateinit var actionNotification: ImageView

    @BindView(R.id.imgChatIV)
    lateinit var actionChat: ImageView

    @BindView(R.id.imgProfileIV)
    lateinit var actionProfile: ImageView

    @BindView(R.id.chatBadgeCountTV)
    lateinit var chatBadgeCountTV: TextView

    var notification_type:String?=null

    var mBillingProcessor: BillingProcessor? = null

    var SUBSCRIPTION_ID = ""
    private var billingClient: BillingClient? = null

        private val purchasesUpdatedListener =
        PurchasesUpdatedListener { billingResult, purchases ->
            // To be implemented in a later section.
        }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        ButterKnife.bind(mActivity)

        initializeSubscription()

        notification_type=intent.getStringExtra("Notification_type")
        if(notification_type != null)
        {
            performSubscriptionClick()
        }
        else{
          //  getPlayStoreSubscriptionDetails()
           setUpFirstFragment()
        }
    }
//    private fun mParam( productId: String, orderId: String,purchase_time:String): MutableMap<String?, String?> {
//        val mMap: MutableMap<String?, String?> = java.util.HashMap()
//        mMap["plan_id"] = "1"
//        mMap["user_id"] = getLoggedInUserID()
//        mMap["plan_price"] = "$14.99"
//        mMap["expire_date"] = ""
//        mMap["plan_duration"] = "30"
//        mMap["purchase_plan"] = "monthly"
//        mMap["product_id"]= productId
//        mMap["subscription_id"]= orderId
//        mMap["type"]="2"
//        mMap["expire_time"] = ""
//        mMap["purchase_plan"]=purchase_time
//        Log.e(TAG, "**PARAM**$mMap")
//        return mMap
//    }
    private fun executeSubscriptionDataRequest(
        purchaseTime: Long,
        orderId: String,
        productId: String
    ) {
     //   var productId= "com.industree.app_monthly_bronze_3453dfgdfs4dfw435"
        var date: Long = purchaseTime
        var purchase_time =SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(Date(date.toLong())).toString()


//        val formatter = SimpleDateFormat("dd/mm/yyyy HH:mm:ss zzzz")
//        formatter.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
//        val dateString :String =formatter.format(date)
      //  val expire_time:String= DateFormat.format("hh:mm:ss",date).toString()
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["plan_id"] = "1"
        mMap["user_id"] = getLoggedInUserID()
        mMap["plan_price"] = "$14.99"
        mMap["expire_date"] = ""
        mMap["plan_duration"] = "30"
        mMap["purchase_plan"] = "monthly"
        mMap["product_id"]= productId
        mMap["subscription_id"]= orderId
        mMap["type"]="2"
        mMap["expire_time"] = ""
        mMap["purchase_plan"]=purchase_time
        Log.e(TAG, "**PARAM**$mMap")
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.getSubscriptionDataRequest(mHeaders,mMap)
        call.enqueue(object : Callback<SubscriptionModel> {
            override fun onFailure(call: Call<SubscriptionModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<SubscriptionModel>, response: Response<SubscriptionModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mSubscriptionModel=  response.body()!!
                if (mSubscriptionModel!!.status == 1) {
                    showToast(mActivity,"Successfully Updated")
//                    val intent = Intent(mActivity, SubscriptionFirst::class.java)
//                    intent.putExtra("category",category)
//                    startActivity(intent)
                }
                else {
                    showAlertDialog(mActivity, mSubscriptionModel.message)
                }
            }
        })
    }

    private fun isUserHasSubscription(context: Activity) {
        billingClient = BillingClient.newBuilder(context).enablePendingPurchases().setListener(purchasesUpdatedListener).build()
        billingClient!!.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(billingResult: BillingResult) {
//               val  purchasesResult:   Purchase.PurchaseState = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
                //  val purchasesResult: PurchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP)
                billingClient!!.queryPurchasesAsync(
                    BillingClient.SkuType.SUBS) { billingResult, list ->
                    if (billingResult.responseCode === BillingClient.BillingResponseCode.OK && list != null) {
                        for (purchase in list) {
                    //        val obj = JSONObject(purchase.toString())
                     //       val listPathObject: JSONObject = obj.getJSONObject("purchase .json")
                          //  val object149 = listPathObject.getJSONObject("149")

//                            showToast(mActivity,"purchase : "+  purchase.products.get(0))

                            executeSubscriptionDataRequest(purchase!!.purchaseTime,purchase!!.orderId,purchase.products.get(0))
                        }
                    }

                    else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                        showToast(mActivity,"Purchase Canceled")
                    } else {
                        Toast.makeText(mActivity, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
                    }
                }

//                billingClient.queryPurchaseHistoryAsync(
//                    BillingClient.SkuType.SUBS
//                ) { billingResult1: BillingResult, list: List<PurchaseHistoryRecord?>? ->
//                    Log.d(
//                        "billingprocess", "purchasesResult.getPurchasesList():" + purchasesResult.purchasesList
//                    )
//                    if (billingResult1.responseCode == BillingClient.BillingResponseCode.OK && !Objects.requireNonNull(
//                            purchasesResult.purchasesList
//                        ).isEmpty()
//                    ) {
//
//                        //here you can pass the user to use the app because he has an active subscription
////                        val myIntent = Intent(context, MainActivity::class.java)
////                        startActivity(myIntent)
//                    }
//                }
            }

            override fun onBillingServiceDisconnected() {
                // Try to restart the connection on the next request to
                // Google Play by calling the startConnection() method.
                Log.d("billingprocess", "onBillingServiceDisconnected")
            }
        })
    }
    private fun getPlayStoreSubscriptionDetails() {
        showToast(mActivity,"onPurchaseHistoryRestored")
        for (sku in mBillingProcessor!!.listOwnedProducts())
            Log.d("LOG_TAG", "Owned Managed Product: $sku")
        for (sku in mBillingProcessor!!.listOwnedSubscriptions()) {
            Log.d("LOG_TAG", "Owned Subscription: $sku")
            SUBSCRIPTION_ID = sku
            showSunscriptionDetails()
        }
    }

    /*
   * In App  Subscriptions
   * */
    @SuppressLint("UseRequireInsteadOfGet")
    open fun initializeSubscription() {
        mBillingProcessor = BillingProcessor(mActivity!!, LICENCE_KEY, this)
        mBillingProcessor!!.initialize()
    }
    override fun onBillingInitialized() {
        Log.e(TAG, "onBillingInitialized: ")
        isUserHasSubscription(mActivity)
    }
    override fun onProductPurchased(productId: String, details: TransactionDetails?) {

    }

    override fun onPurchaseHistoryRestored() {
      //  getPlayStoreSubscriptionDetails()
    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {

    }


    private fun showSunscriptionDetails() {
        val subs: SkuDetails? = mBillingProcessor!!.getSubscriptionListingDetails(SUBSCRIPTION_ID)
        println("***__SUBSCRIPTION__*** $subs")
        //showToast(subs != null ? subs.toString() : "Failed to load subscription details");
        if (subs == null) {
            println("Failed to load subscription details")
        } else {
            subs.toString()
            showAlertDialog(mActivity, "Subscribed Plan : " + subs!!.title)
            showAlertDialog(mActivity, "description " + subs.description)
            showAlertDialog(mActivity, "Price " + subs.priceText)
            showAlertDialog(mActivity, "Subscription Period" + subs.introductoryPriceCycles)
        }
    }

    @OnClick(
        R.id.imgHomeIV,
        R.id.imgFavouriteIV,
        R.id.imgSubscriptionIV,
        R.id.imgChatIV,
        R.id.imgProfileIV
    )
    fun onViewClicked(view: View){
        when(view.id) {
            R.id.imgHomeIV -> performHomeClick()
            R.id.imgFavouriteIV -> performExploreClick()
            R.id.imgSubscriptionIV ->performSubscriptionClick()
            R.id.imgChatIV -> performChatClick()
            R.id.imgProfileIV -> performProfileClick()
        }
    }
    fun badgeChatCount(chatCount: Int?) {
        if(chatCount == 0 ){ chatBadgeCountTV.visibility=View.GONE}
        else if(chatCount!!.equals(null)){chatBadgeCountTV.visibility=View.GONE}
        else if(chatCount!! > 0){chatBadgeCountTV.visibility=View.VISIBLE
            chatBadgeCountTV.setText(""+chatCount)}
    }
    override fun onBackPressed() {
        //super.onBackPressed()
        finish()
  //      showToast(mActivity,"back pressed")
    }
    private fun setUpFirstFragment() {
            performHomeClick()
    }
    private fun performHomeClick() {
        tabSelection(0)
        switchFragment(HomeFragment(), HOME_TAG, false, null)
    }
    private fun performExploreClick() {
        tabSelection(1)
        switchFragment(FavouriteFragment(), FAVOURITE_TAG, false, null)
    }
    private fun performSubscriptionClick() {
        tabSelection(2)
      //  switchFragment(SubscriptionFragment(), SUBSCRIPTION_TAG ,false,null )
        if (isNetworkAvailable(mActivity)) {
            executeCheckPlanRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeCheckPlanRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.CheckPlanRequest(mHeaders, mParam())
        call.enqueue(object : Callback<CheckPlanModel> {
            override fun onFailure(call: Call<CheckPlanModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CheckPlanModel>,
                response: Response<CheckPlanModel>
            ) {
                Log.e(TAG, response.body().toString())
                 var mCheckPlanModel = response.body()!!
                if (mCheckPlanModel.status == 1) {
                   AppPrefrences().writeString(mActivity, PLANID, mCheckPlanModel.data!!.planId)
                   AppPrefrences().writeString(mActivity, ISSUBSCRIBED, mCheckPlanModel.data!!.isSubscribed)
                   if(mCheckPlanModel.data!!.planId != "" && mCheckPlanModel.data!!.isSubscribed != ""){
                       if(mCheckPlanModel.data!!.isSubscribed == "1"){
                           val intent = Intent(mActivity, SubscriptionFirst::class.java)
                           intent.putExtra("category",mCheckPlanModel.data!!.planId)
                           startActivity(intent)
                       }
                       else if(mCheckPlanModel.data!!.isSubscribed == "2"){
                          switchFragment(BookingServicesFragment(), SUBSCRIPTION_TAG ,false,null)
                       }
                       else if(mCheckPlanModel.data!!.isSubscribed == "0"){
                           switchFragment(SubscriptionFragment(), SUBSCRIPTION_TAG ,false,null )
                       }
                   }
                    else{
                       switchFragment(SubscriptionFragment(), SUBSCRIPTION_TAG ,false,null )
                    }
                }
                else if(mCheckPlanModel.status == 0){
                    switchFragment(SubscriptionFragment(), SUBSCRIPTION_TAG ,false,null )
                }
                else if(mCheckPlanModel.status == 3 || mCheckPlanModel.status ==2){
                    switchFragment(SubscriptionFragment(), SUBSCRIPTION_TAG ,false,null )
                }
                else{
                    switchFragment(SubscriptionFragment(), SUBSCRIPTION_TAG ,false,null )
                }
                }
            })
    }

    private fun performChatClick() {
        tabSelection(3)
        switchFragment(ChatFragment(), CHAT_TAG,false, null)
    }
    private fun performProfileClick() {
        tabSelection(4)
        switchFragment(ProfileFragment() , PROFILE_TAG , false, null)
    }
    private fun tabSelection(mPos: Int) {
        imgHomeIV.setImageResource(R.drawable.home_icon)
        imgFavouriteIV.setImageResource(R.drawable.fav_icon)
        imgSubscriptionIV.setImageResource(R.drawable.subscription_icon)
        imgChatIV.setImageResource(R.drawable.chat_icon)
        imgProfileIV.setImageResource(R.drawable.user_icon)
        when(mPos){
            0 ->{
                imgHomeIV.setImageResource(R.drawable.ic_home_sel)
            }
            1 ->{
                imgFavouriteIV.setImageResource(R.drawable.ic_fav_sel)
            }
            2 ->{
                imgSubscriptionIV.setImageResource(R.drawable.ic_subscription_sel)
            }
            3 ->{
                imgChatIV.setImageResource(R.drawable.ic_chat_sel)
            }
            4 ->{
                imgProfileIV.setImageResource(R.drawable.ic_user_sel)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, @Nullable data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        for (fragment in supportFragmentManager.fragments) {
            fragment.onActivityResult(requestCode, resultCode, data)
        }
    }
}

