package com.industree.app.main.utils

// - - Constant Values
const val SPLASH_TIME_OUT = 1500
//LIVE
//const val BASE_URL ="https://industree.online/webservices/"

// latest LIVe link
//const val BASE_URL = "https://industreeapp.com/webservices/"

//Staging url

//const val BASE_URL = "http://industree.online/stagingwebservices/"
const val BASE_URL= "http://industreeapp.com/stagingwebservices/"



const val PRIVACY_POLICY = BASE_URL+"privacyPolicy.html"
const val TERMS_OF_USE = BASE_URL+"TermsAndConditions.html"

// PAYPAL KEY
// SANDBOX
//const val PAYPAL_KEY = "AUlhCZwSybK6x7fJHDbpIKvhSFuKPv2b1KEh-M9zLVBiXvjFkWkHWqvnTOm3q3nTjdldT05KBwWhxOJ6"

// LIVE
const val PAYPAL_KEY = "AYDwTurZuULFvFgKZTEUPM4lmVf-4ptyqzyzXDU2LO_PGa3m7BOneuK319o8Vl2IShROcyxUE_gf1_yd"


// places api key
const val PLACES_API_KEY = "AIzaSyAnFybP6NfVXxiGtAZW3rUvbZDrqwgdN7o"
const val GOOGLE_PLACES_SEARCH = "https://maps.googleapis.com/maps/api/place/autocomplete/json?"
const val GOOGLE_PLACES_LAT_LONG = "https://maps.googleapis.com/maps/api/place/details/json?place_id="

/*
* Data Transfer
* */
const val DEVICE_TYPE = "2"
const val NOTI_TYPE = "noti_type"
/*
* Shared Preferences Keys
* */
const val IS_LOGIN = "is_login"
const val USERID = "user_id"
const val FIRSTNAME = "firstname"
const val LASTNAME="lastname"
const val EMAIL = "email"
const val PASSWORD ="password"
const val PROFILEPIC = "photo"

const val LATITUDE ="latitude"
const val LONGITUDE ="logitude"
const val AUTHTOKEN = "authToken"
const val PHONENO="phoneNo"

// - - Fragment Tags
const val HOME_TAG = "home_tag"
const val FAVOURITE_TAG="favourite_tag"
const val SUBSCRIPTION_TAG = "subscription_tag"
const val CHAT_TAG = "message_tag"
const val PROFILE_TAG = "profile_tag"

const val ALL_BOOKING_TAG = "all_booking_tag"
const val ON_GOING_BOOKING_TAG ="on_going_booking_tag"
const val COMPLETE_BOOKING_TAG ="complete_booking_tag"

const val SOCKET_URL = "http://161.97.132.85:3020/"
const val DEVICE_TOKEN="device_token"
const val GOOGLE_SIGN_IN= 123


const val PLANID="plan_id"
const val ISSUBSCRIBED="is_subscribed"
/*
     *uf
     * Subscriptions Keys & Details
     * */
const val MERCHANT_ID: String = "627288377185569699"
const val LICENCE_KEY: String = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAoyVgQtiK8MfyJ+7x6XTtPgPwl7TAoKZCV2sjHw9FbRNhNzwVfSo0+IVZT26mRj8ytJqdWHkf7zFci6WbAB2mKk4p87GcEwyCiNcvNMf8IOdtjM+WqSIJ9i949ScGvzSG+brXFGp+6yz5c6Hs5gMqjnG0ZRdKRjKitqOD6ri8TtbeifRT4joVVBz+D7ykID0FWjGkop1E00dFwM2iK8QCbQNC2+ARtBHfWZqMLB5o2d/12gIZU3IpOT0FjUV7jqEIsFvxSTVyv+u1+AejfVER5dlf38COrPRV9QogRhtKwsKxJJA/es+NbnkS4virM+zo6DzV8VG4STVUsQVeWNE89wIDAQAB"
const val BRONZE_SUBSCRIPTION_PLAN_KEY: String = "com.industree.app_monthly_bronze_3453dfgdfs4dfw435"
const val GOLD_SUBSCRIPTION_PLAN_KEY: String = "com.industree.app_monthly_gold_wueh324h23o434lijh"
const val PLATINUM_SUBSCRIPTION_PLAN_KEY: String = "com.industree.app_monthly_platinum_23kj4khk459df0d"

// for testing offer
//const val BRONZE_SUBSCRIPTION_PLAN_KEY: String = "com.industree.app_monthly_bronze_new_3j4"


const val IS_SUBSCRIPTION = "is_subscriptions"


const val FILTER_BACK_PRESSED = false


const val FULL_ADDRESS="full Address"
const val FROM_PARENT="from_parent"
const val LOCATION_ROLE="location_role"
const val LOCATION="location"

