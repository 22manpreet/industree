package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookingSubmitModel(

	@field:SerializedName("start_time")
	var startTime: String? = null,

	@field:SerializedName("amount")
	var amount: String? = null,

	@field:SerializedName("user_id")
	var userId: String? = null,

	@field:SerializedName("vendor_id")
	var vendorId: String? = null,

	@field:SerializedName("services")
	var services: ArrayList<ServicesItemData?>? = null,

	@field:SerializedName("book_date")
	var bookDate: String? = null,

	@field:SerializedName("brand_id")
    var brandId: String? = null

) : Parcelable

@Parcelize
data class ServicesItemData(

	@field:SerializedName("service_quantity")
	var serviceQuantity: String? = "",

	@field:SerializedName("service_id")
	var serviceId: String? = ""
) : Parcelable
