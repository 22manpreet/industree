package com.industree.app.main.`interface`

interface BookingClickListener {
    fun onItemClickListener(
        totalArrayList: IntArray,
        position: Int,
        size: Int,
        count: Int,
        serviceId: Int,
        serviceIdArrayList: IntArray
    )
}