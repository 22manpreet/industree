package com.industree.app.main.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.industree.app.R
import de.hdodenhof.circleimageview.CircleImageView

class AccountInformationActivity : BaseActivity() {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.txtNameTV)
    lateinit var txtNameTV: TextView

    @BindView(R.id.txtemailTV)
    lateinit var txtemailTV: TextView

    @BindView(R.id.txtphoneTV)
    lateinit var txtphoneTV: TextView

    @BindView(R.id.editProfileIV)
    lateinit var editProfileIV: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account_information)
        ButterKnife.bind(mActivity)
        txtNameTV.setText(getFirstName()+" "+getLastName())
        txtemailTV.setText(getUserEmail())
        txtphoneTV.setText(getPhoneNo())
        Glide.with(mActivity).load(getUserProfilePic())
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(editProfileIV)
    }
    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }
}
