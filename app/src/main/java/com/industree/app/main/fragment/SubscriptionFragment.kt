package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.cardview.widget.CardView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.android.billingclient.api.*
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.SkuDetails
import com.anjlab.android.iab.v3.TransactionDetails
import com.industree.app.R
import com.industree.app.main.Model.SubscriptionModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.activity.PrivacyPolicy
import com.industree.app.main.activity.SubscriptionFirst
import com.industree.app.main.activity.TermsOfUse
import com.industree.app.main.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.security.Security
import java.util.*


class SubscriptionFragment : BaseFragment(), BillingProcessor.IBillingHandler, PurchasesUpdatedListener
 {
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.subscriptionCV1)
    lateinit var subscriptionCV1: CardView

    @BindView(R.id.subscriptionCV2)
    lateinit var subscriptionCV2: CardView

    @BindView(R.id.subscriptionCV3)
    lateinit var subscriptionCV3: CardView

    @BindView(R.id.termsConditionsTV)
    lateinit var termsConditionsTV: TextView

    @BindView(R.id.privacyPolicyTV)
    lateinit var privacyPolicyTV: TextView

    @BindView(R.id.txtRestorePurchaseTV)
    lateinit var txtRestorePurchaseTV: TextView

    var mBillingProcessor: BillingProcessor? = null

    var category:String?=null
    var subscription_price:String?=null
    var plan_Id:String?=null

    private var billingClient: BillingClient? = null

    var SUBSCRIPTION_ID = ""
     companion object {
         const val PREF_FILE = "MyPref"

         //note add unique product ids
         //use same id for preference key
         private val purchaseItemIDs: ArrayList<String> = object : ArrayList<String>() {
             init {
                 add(BRONZE_SUBSCRIPTION_PLAN_KEY)
                 add(GOLD_SUBSCRIPTION_PLAN_KEY)
                 add(PLATINUM_SUBSCRIPTION_PLAN_KEY)
             }
         }
         private val purchaseItemDisplay = ArrayList<String>()
     }
//    private val purchasesUpdatedListener =
//        PurchasesUpdatedListener { billingResult, purchases ->
//            // To be implemented in a later section.
//        }

//    @SuppressLint("UseRequireInsteadOfGet")
//    private var billingClient = BillingClient.newBuilder(activity!!)
//        .setListener(purchasesUpdatedListener)
//        .enablePendingPurchases()
//        .build()

//   lateinit var billingClient : BillingClient

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_subscription, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        initializeSubscription()

        showToast(activity,"onPurchaseHistoryRestored")
        for (sku in mBillingProcessor!!.listOwnedProducts())
            Log.d("LOG_TAG", "Owned Managed Product: $sku")
        for (sku in mBillingProcessor!!.listOwnedSubscriptions()) {
            Log.d("LOG_TAG", "Owned Subscription: $sku")
            SUBSCRIPTION_ID = sku
            showSunscriptionDetails()
        }
      //  isUserHasSubscription(activity!!)
        return view
    }

//    private fun isUserHasSubscription(context: Activity) {
//        billingClient = BillingClient.newBuilder(context).enablePendingPurchases().setListener(purchasesUpdatedListener).build()
//        billingClient!!.startConnection(object : BillingClientStateListener {
//            override fun onBillingSetupFinished(billingResult: BillingResult) {
////               val  purchasesResult:   Purchase.PurchaseState = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
//              //  val purchasesResult: PurchasesResult = billingClient.queryPurchases(BillingClient.SkuType.INAPP)
//                billingClient!!.queryPurchasesAsync(
//                    BillingClient.SkuType.SUBS) { billingResult, list ->
//                    if (billingResult.responseCode === BillingClient.BillingResponseCode.OK && list != null) {
//                        for (purchase in list) {
//                           showToast(activity,"purchase : "+  purchase.products.get(0))
//                        }
//                    }
//                }
//
////                billingClient.queryPurchaseHistoryAsync(
////                    BillingClient.SkuType.SUBS
////                ) { billingResult1: BillingResult, list: List<PurchaseHistoryRecord?>? ->
////                    Log.d(
////                        "billingprocess", "purchasesResult.getPurchasesList():" + purchasesResult.purchasesList
////                    )
////                    if (billingResult1.responseCode == BillingClient.BillingResponseCode.OK && !Objects.requireNonNull(
////                            purchasesResult.purchasesList
////                        ).isEmpty()
////                    ) {
////
////                        //here you can pass the user to use the app because he has an active subscription
//////                        val myIntent = Intent(context, MainActivity::class.java)
//////                        startActivity(myIntent)
////                    }
////                }
//            }
//
//            override fun onBillingServiceDisconnected() {
//                // Try to restart the connection on the next request to
//                // Google Play by calling the startConnection() method.
//                Log.d("billingprocess", "onBillingServiceDisconnected")
//            }
//        })
//    }

    @OnClick(
        R.id.subscriptionCV1,R.id.subscriptionCV2,R.id.subscriptionCV3,R.id.termsConditionsTV,R.id.privacyPolicyTV,R.id.txtRestorePurchaseTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.subscriptionCV1 -> onFirstSubscriptionClick()
            R.id.subscriptionCV2 -> onSecondSubscriptionClick()
            R.id.subscriptionCV3 -> onThirdSubscriptionClick()
            R.id.termsConditionsTV -> onClickTermsOfUse()
            R.id.privacyPolicyTV -> onClickPrivacyPolicy()
            R.id.txtRestorePurchaseTV -> onRestoreClick()
        }
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun onRestoreClick() {
         if(getPlanId().equals("1")){
             val intent = Intent(activity, SubscriptionFirst::class.java)
             intent.putExtra("category","1")
             startActivity(intent)
         }
        else if(getPlanId().equals("2")){
             val intent = Intent(activity, SubscriptionFirst::class.java)
             intent.putExtra("category","2")
             startActivity(intent)
        } else if(getPlanId().equals("3")){
             val intent = Intent(activity, SubscriptionFirst::class.java)
             intent.putExtra("category","3")
             startActivity(intent)
        }
        else{
          showAlertDialog(activity!!, getString(R.string.there_are_no_purchases_with_these_subscriptions_to_restore_related_to_your_account))
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onClickPrivacyPolicy() {
        val i = Intent(activity!!, PrivacyPolicy::class.java)
        startActivity(i)
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onClickTermsOfUse() {
        val i = Intent(activity!!, TermsOfUse::class.java)
        startActivity(i)
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun onThirdSubscriptionClick() {
        category="3"
        subscription_price="$24.99"
        plan_Id ="3"
        //Actual
  //      if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
            subscriptionCallBack(PLATINUM_SUBSCRIPTION_PLAN_KEY)
//        } else {
//            showToast(activity!!,getString(R.string.subscription_update_not))
//        }
//        val intent = Intent(activity, SubscriptionFirst::class.java)
//        intent.putExtra("category","3")
//        startActivity(intent)
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onSecondSubscriptionClick() {
        category="2"
        subscription_price="$19.99"
        plan_Id ="2"
        //Actual
    //    if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
            subscriptionCallBack(GOLD_SUBSCRIPTION_PLAN_KEY)
//        } else {
//            showToast(activity!!,getString(R.string.subscription_update_not))
//
//        }
//        val intent = Intent(activity, SubscriptionFirst::class.java)
//        intent.putExtra("category","2")
//        startActivity(intent)
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun onFirstSubscriptionClick() {
        category="1"
        subscription_price="$14.99"
        plan_Id ="1"
        //Actual
            subscriptionCallBack(BRONZE_SUBSCRIPTION_PLAN_KEY)

        //For Testing
//        AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, true)
//        showToast(activity,getString(R.string.purchase_subscription_Successfully))
//        if(isNetworkAvailable(activity!!)){
//            var date=Date()
//            executeSubscriptionDataRequest(date)
//        } else {
//            showToast(activity, getString(R.string.internet_connection_error))
//        }
//        val intent = Intent(activity, SubscriptionFirst::class.java)
//        intent.putExtra("category","1")
//        startActivity(intent)

       // activity!!.finish()
    }

    /*
    * In App  Subscriptions
    * */
    @SuppressLint("UseRequireInsteadOfGet")
    open fun initializeSubscription() {
        mBillingProcessor = BillingProcessor(activity!!, LICENCE_KEY, this)
        mBillingProcessor!!.initialize()
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun subscriptionCallBack(SUBSCRIPTION_PLAN_KEY: String) {
        // With Testing  PayLoads for Product Purchase & Subscription
        //mBillingProcessor.purchase(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        //mBillingProcessor.subscribe(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        if (isNetworkAvailable(activity!!)) {
            val extraParams = Bundle()
            extraParams.putString("accountId", MERCHANT_ID)
            mBillingProcessor!!.subscribe(activity!!, SUBSCRIPTION_PLAN_KEY, null , extraParams)
        } else {
            showToast(activity!!, getString(R.string.internet_connection_error))
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onProductPurchased(productId: String, mTransactionDetails: TransactionDetails?) {
        Log.e(TAG, "Purchase DATA :- " + mTransactionDetails!!.purchaseInfo.purchaseData)
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        val mPurchaseData = mTransactionDetails.purchaseInfo.purchaseData
        var mSubscriptionPlanKey = mTransactionDetails.purchaseInfo.purchaseData.productId

        Log.e(TAG, "onProduct Purchased_Data: " + mTransactionDetails.purchaseInfo.purchaseData.toString())
        Log.e(TAG, "onProduct Purchased_OrderID: " + mPurchaseData!!.orderId)
        Log.e(TAG, "onProduct Purchased_Token: " + mPurchaseData.purchaseToken)
        Log.e(TAG, "onProduct Purchased_Time: " + mPurchaseData.purchaseTime)
        Log.e(TAG, "Subscriptions Plan Key: $mSubscriptionPlanKey")

//        if (mTransactionDetails.purchaseInfo == null) {
//            AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, false)
//            showToast(activity,getString(R.string.some_things))
//        } else {
//            AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, true)
//            showToast(activity,getString(R.string.purchase_subscription_Successfully))
//            if(isNetworkAvailable(activity!!)){
//                executeSubscriptionDataRequest(mPurchaseData.purchaseTime)
//            } else {
//                showToast(activity, getString(R.string.internet_connection_error))
//            }
//        }
//        activity!!.finish()
        AppPrefrences().writeBoolean(activity!!, IS_SUBSCRIPTION, true)
        AppPrefrences().writeString(activity!!, PLANID,plan_Id)

        if(isNetworkAvailable(activity!!)){
                executeSubscriptionDataRequest(mPurchaseData.purchaseTime,mPurchaseData.productId,mPurchaseData!!.orderId)
            } else {
                showToast(activity, getString(R.string.internet_connection_error))
            }
    }

    private fun mParam(dateString: String, productId: String, expire_time: String, orderId: String): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["plan_id"] = plan_Id
        mMap["user_id"] = getLoggedInUserID()
        mMap["plan_price"] = subscription_price
        mMap["expire_date"] = ""
        mMap["plan_duration"] = "30"
        mMap["purchase_plan"] = "monthly"
        mMap["product_id"]= productId
        mMap["subscription_id"]= orderId
        mMap["type"]="2"
        mMap["expire_time"] = expire_time
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSubscriptionDataRequest(
        purchaseTime: Date,
        productId: String,
        orderId: String
    ) {
        var date: Date? = purchaseTime
//        val formatter = SimpleDateFormat("dd/mm/yyyy HH:mm:ss zzzz")
//        formatter.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
//        val dateString :String =formatter.format(date)
          val dateString: String =""

        val expire_time:String=DateFormat.format("hh:mm:ss",date).toString()
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.getSubscriptionDataRequest(mHeaders,mParam(dateString,productId,expire_time,orderId))
        call.enqueue(object : Callback<SubscriptionModel> {
            override fun onFailure(call: Call<SubscriptionModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<SubscriptionModel>, response: Response<SubscriptionModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mSubscriptionModel=  response.body()!!
                if (mSubscriptionModel!!.status == 1) {
                    val intent = Intent(activity, SubscriptionFirst::class.java)
                    intent.putExtra("category",category)
                    startActivity(intent)
                }
                else {
                    showAlertDialog(activity, mSubscriptionModel.message)
                }
            }
        })
    }

    override fun onPurchaseHistoryRestored() {
        showToast(activity,"onPurchaseHistoryRestored")
        for (sku in mBillingProcessor!!.listOwnedProducts())
            Log.d("LOG_TAG", "Owned Managed Product: $sku")
        for (sku in mBillingProcessor!!.listOwnedSubscriptions()) {
            Log.d("LOG_TAG", "Owned Subscription: $sku")
            SUBSCRIPTION_ID = sku
            showSunscriptionDetails()
        }

    }
    fun showSunscriptionDetails() {
        val subs: SkuDetails? = mBillingProcessor!!.getSubscriptionListingDetails(SUBSCRIPTION_ID)
        println("***__SUBSCRIPTION__*** $subs")
        //showToast(subs != null ? subs.toString() : "Failed to load subscription details");
        if(subs ==null){
            println("Failed to load subscription details")
        }
        else {
            subs.toString()
            showAlertDialog(activity,"Subscribed Plan : " + subs!!.title)
            showAlertDialog(activity,"description "+ subs.description)
            showAlertDialog(activity,"Price "+subs.priceText)
            showAlertDialog(activity,"Subscription Period"+subs.introductoryPriceCycles)
        }


//        val builderDetails: AlertDialog.Builder = Builder(activity)
//        builderDetails.setPositiveButton("Okay!",
//            DialogInterface.OnClickListener { dialog, which -> activity!!.onBackPressed() })
//        CustomeDialogClass.oKDialogBox(
//            builderDetails, "Subscribed Plan : " + subs!!.title,
//            """
//            ${subs.description}
//            Price : ${subs.priceText}
//            Subscription Period : ${subs.introductoryPriceCycles}
//            """.trimIndent(), R.drawable.ic_wallet, false, false
//        )
    }
    override fun onBillingError(errorCode: Int, error: Throwable?) {
        /*
        * Called when some error occurred. See Constants class for more details
        *
        * Note - this includes handling the case where the user canceled the buy dialog:
        * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
        */
        Log.e(TAG, "onBillingError: ")
        when (errorCode) {
            0 -> Log.e("onBillingError", "> Success - BILLING_RESPONSE_RESULT_OK")
            1 -> Log.e(
                "onBillingError",
                "> User pressed back or canceled a dialog - BILLING_RESPONSE_RESULT_USER_CANCELED"
            )
            3 -> Log.e(
                "onBillingError",
                "> Billing API version is not supported for the type requested - BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE"
            )
            4 -> Log.e(
                "onBillingError",
                "> Requested product is not available for purchase - BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE"
            )
            5 -> Log.e(
                "onBillingError",
                "> Invalid arguments provided to the API. This error can also indicate that the application was not correctly signed or properly set up for In-app Billing in Google Play, or does not have the necessary permissions in its manifest - BILLING_RESPONSE_RESULT_DEVELOPER_ERROR"
            )
            6 -> Log.e(
                "onBillingError",
                "> Fatal error during the API action - BILLING_RESPONSE_RESULT_ERROR"
            )
            7 -> Log.e(
                "onBillingError",
                "> Failure to purchase since item is already owned - BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED"
            )
            8 -> Log.e(
                "onBillingError",
                "> Failure to consume since item is not owned - BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED"
            )
        }
    }

    override fun onBillingInitialized() {
        /*
        * Called when BillingProcessor was initialized and it's ready to purchase
        */
        Log.e(TAG, "onBillingInitialized: ");

//        var controlnumber: Boolean = mBillingProcessor!!.loadOwnedPurchasesFromGoogle()
//        if (controlnumber) {
//            var subscriptionTransactionDetails: TransactionDetails =
//                mBillingProcessor!!.getSubscriptionTransactionDetails(getString(R.string.trackmonth))!!;
//            bp.consumePurchase(getString(R.string.trackmonth));
//            if (subscriptionTransactionDetails != null) {
//                Log.d("TAG", "onBillingInitialized: active");
//            } else {
//                Log.d("TAG", "onBillingInitialized: not");
//            }
//        }


    }

    override fun onDestroy() {
        if (mBillingProcessor != null) mBillingProcessor!!.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!mBillingProcessor!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }

    override fun onPurchasesUpdated(billingResult: BillingResult, purchases: List<Purchase>?) {
//if item newly purchased
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
          //  handlePurchases(purchases)
            showToast(activity,"purchase : ")
        } else if (billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
            val queryAlreadyPurchasesResult= billingClient!!.queryPurchasesAsync(
                    BillingClient.SkuType.SUBS) { billingResult, list ->
                    if (billingResult.responseCode === BillingClient.BillingResponseCode.OK && list != null) {
                        for (purchase in list) {
                           showToast(activity,"purchase : "+  purchase.products.get(0))
                          //  handlePurchases(purchases!!)
                        }
                    }
                }
          //  val queryAlreadyPurchasesResult = billingClient!!.queryPurchases(BillingClient.SkuType.INAPP)
          //  val alreadyPurchases = queryAlreadyPurchasesResult.purchasesList
          //  alreadyPurchases?.let { handlePurchases(it) }
        } else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
            showToast(activity,"Purchase Canceled")
        } else {
            Toast.makeText(activity, "Error " + billingResult.debugMessage, Toast.LENGTH_SHORT).show()
        }
    }
//    fun handlePurchases(purchases: List<Purchase>) {
//        for (purchase in purchases) {
//
//            val index = purchaseItemIDs.indexOf(purchase.sku)
//            //purchase found
//            if (index > -1) {
//
//                //if item is purchased
//                if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
//                    if (!verifyValidSignature(purchase.originalJson, purchase.signature)) {
//                        // Invalid purchase
//                        // show error to user
//                        Toast.makeText(activity, "Error : Invalid Purchase", Toast.LENGTH_SHORT).show()
//                        continue  //skip current iteration only because other items in purchase list must be checked if present
//                    }
//                    // else purchase is valid
//                    //if item is purchased and not  Acknowledged
//                    if (!purchase.isAcknowledged) {
//                        val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
//                            .setPurchaseToken(purchase.purchaseToken)
//                            .build()
//                        billingClient!!.acknowledgePurchase(acknowledgePurchaseParams
//                        ) { billingResult ->
//                            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
//                                //if purchase is acknowledged
//                                //then saved value in preference
//                                savePurchaseItemValueToPref(purchaseItemIDs[index], true)
//                                Toast.makeText(activity, purchaseItemIDs[index] + " Item Purchased", Toast.LENGTH_SHORT).show()
//                              //  notifyList()
//                            }
//                        }
//                    }
//                    else {
//                        // Grant entitlement to the user on item purchase
//                        if (!getPurchaseItemValueFromPref(purchaseItemIDs[index])) {
//                            savePurchaseItemValueToPref(purchaseItemIDs[index], true)
//                            Toast.makeText(activity, purchaseItemIDs[index] + " Item Purchased.", Toast.LENGTH_SHORT).show()
//                          //  notifyList()
//                        }
//                    }
//                }
//                else if (purchase.purchaseState == Purchase.PurchaseState.PENDING) {
//                    Toast.makeText(activity, purchaseItemIDs[index] + " Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT).show()
//                }
//                else if (purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE) {
//                    //mark purchase false in case of UNSPECIFIED_STATE
//                    savePurchaseItemValueToPref(purchaseItemIDs[index], false)
//                    Toast.makeText(activity, purchaseItemIDs[index] + " Purchase Status Unknown", Toast.LENGTH_SHORT).show()
//                 //   notifyList()
//                }
//            }
//        }
//    }
//    private fun verifyValidSignature(signedData: String, signature: String): Boolean {
//        return try {
//            //for old playconsole
//            // To get key go to Developer Console > Select your app > Development Tools > Services & APIs.
//            //for new play console
//            //To get key go to Developer Console > Select your app > Monetize > Monetization setup
//            val base64Key = "Add your key here"
//            Security.verifyPurchase(base64Key, signedData, signature)
//        } catch (e: IOException) {
//            false
//        }
//    }
    private fun savePurchaseItemValueToPref(PURCHASE_KEY: String, value: Boolean) {
       // preferenceEditObject.putBoolean(PURCHASE_KEY, value).commit()
    }
    private fun getPurchaseItemValueFromPref(PURCHASE_KEY: String): Boolean {
        return preferenceObject.getBoolean(PURCHASE_KEY, false)
    }
    private fun notifyList() {
        purchaseItemDisplay.clear()
        for (p in purchaseItemIDs) {
            purchaseItemDisplay.add("Purchase Status of " + p + " = " + getPurchaseItemValueFromPref(p))
        }
       // arrayAdapter!!.notifyDataSetChanged()
    }
private val preferenceObject: SharedPreferences
    private get() = requireActivity().getSharedPreferences(PREF_FILE, 0)
    private val preferenceEditObject: SharedPreferences.Editor
        private get() {
            val pref = requireActivity().getSharedPreferences(PREF_FILE, 0)
            return pref.edit()
        }
}

