package com.industree.app.main.adapter

import android.app.Activity
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.DataItem
import com.industree.app.main.`interface`.PaginationInterface
import com.industree.app.main.activity.DetailActivity
import java.util.ArrayList

class HomeVerticalListAdapter(
    var activity: Activity,
    var homeDetailItemList: ArrayList<DataItem?>?,
    var categoryId: String?,
    var loggedInUserID: String,
    var mInterfaceData: PaginationInterface
) : RecyclerView.Adapter<HomeVerticalListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.home_vertical_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val mModel: DataItem = homeDetailItemList!!.get(position)!!

        holder.nameVerticalList.setText(homeDetailItemList!!.get(position)?.brandName)
        holder.addressVerticalList.setText(homeDetailItemList!!.get(position)?.address)
        holder.distanceVerticalList.setText(homeDetailItemList!!.get(position)?.distance)
        Glide.with(activity!!)
            .load(homeDetailItemList?.get(position)?.brandImage)
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .into(holder.verticalListImage)
        holder.homeVerticalListLL.setOnClickListener {
            val intent = Intent( activity, DetailActivity::class.java)
            intent.putExtra("user_id", homeDetailItemList!!.get(position)!!.userId)
            intent.putExtra("vendor_id",homeDetailItemList!!.get(position)?.vendorId)
            intent.putExtra("brand_id", homeDetailItemList!!.get(position)?.brandId)
            intent.putExtra("brand_image", homeDetailItemList!!.get(position)?.brandImage)
            intent.putExtra("categoryId",categoryId)
            activity!!.startActivity(intent)
        }

        if (homeDetailItemList!!.size % 10 == 0 && homeDetailItemList!!.size - 1 == position){
            mInterfaceData.mHomePagination(mModel)
        }

    }
    override fun getItemCount(): Int {
        return homeDetailItemList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var verticalListImage: ImageView
        var homeVerticalListLL: LinearLayout
        var nameVerticalList: TextView
        var distanceVerticalList: TextView
        var addressVerticalList: TextView

        init {
            homeVerticalListLL = itemView.findViewById(R.id.homeVerticalListLL)
            verticalListImage = itemView.findViewById(R.id.verticalListIV)
            nameVerticalList = itemView.findViewById(R.id.nameVerticalListTV)
            distanceVerticalList = itemView.findViewById(R.id.distanceVerticalListTV)
            addressVerticalList = itemView.findViewById(R.id.addressVerticalListTV)

        }
    }


}