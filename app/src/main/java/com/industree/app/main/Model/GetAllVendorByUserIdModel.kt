package com.industree.app.main.Model

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAllVendorByUserIdModel(

	@field:SerializedName("data")
	val data: getVendorData? ? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("category")
	val category: ArrayList<VendorCategoryItem?>? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class VendorCategoryItem(

	@field:SerializedName("address")
    var address: String? = null,

	@field:SerializedName("category_name")
    var categoryName: String? = "",

	@field:SerializedName("category_id")
	var categoryId: String? = null,

	@field:SerializedName("service_price")
	val servicePrice: String? = null,

	@field:SerializedName("brand_image")
    var brandImage: String? = null,

    var imageByteArray: String? = "",

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("brand_name")
	var brandName: String? = null,

	@field:SerializedName("location")
	var location: String? = null,

	@field:SerializedName("services")
	var services: ArrayList<vendorServicesItem?>? = ArrayList(),

	@field:SerializedName("brand_id")
	val brandId: String? = null,

	@field:SerializedName("latitude")
	var latitude: String? = "",

	@field:SerializedName("longitude")
	var longitude: String? = ""


//	@field:SerializedName("bitmap")
//	var bitmap: Bitmap? = null,

) : Parcelable

@Parcelize
data class getVendorData(

	@field:SerializedName("youtube")
	val youtube: String? = null,

	@field:SerializedName("phone_no")
	val phoneNo: String? = null,

	@field:SerializedName("website")
	val website: String? = null,

	@field:SerializedName("distance")
	val distance: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("facebook")
	val facebook: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("vendor_name")
	val vendorName: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("instagram")
	val instagram: String? = null,

	@field:SerializedName("isFav")
	val isFav: String? = null,

	@field:SerializedName("twitter")
	val twitter: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("brand_image")
	val brandImage: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null
) : Parcelable

@Parcelize
data class vendorServicesItem(

	@field:SerializedName("service_description")
	val serviceDescription: String? = "",

	@field:SerializedName("address")
	val address: String? = "",

	@field:SerializedName("service_price")
	val servicePrice: String? = "",

	@field:SerializedName("service_name")
	var serviceName: String? = "",

	@field:SerializedName("service_id")
	val serviceId: String? = "",

	@field:SerializedName("location")
	val location: String? = "",

	@field:SerializedName("brand_id")
	val brandId: String? = ""
) : Parcelable
