package com.industree.app.main.`interface`

interface FavouriteClickListener {
    fun onItemClickListener(
        vendorId: String?,
        categoryId: String?,
        position: Int,
        brandName: String?
    )
}