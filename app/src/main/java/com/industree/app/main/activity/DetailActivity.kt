package com.industree.app.main.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.StrictMode
import android.util.Log
import android.view.*
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.gson.Gson
import com.industree.app.R
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.BookingClickListener
import com.industree.app.main.adapter.ServicesListAdapter
import com.whinc.widget.ratingbar.RatingBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.time.LocalDate
import kotlin.collections.HashMap
import kotlin.collections.MutableMap
import kotlin.collections.isNotEmpty
import kotlin.collections.set
import kotlin.math.roundToInt
import java.time.format.DateTimeFormatter
import com.industree.app.main.Model.*
import kotlin.collections.ArrayList
import com.industree.app.main.adapter.ReviewAdapter
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*
import android.widget.RatingBar.OnRatingBarChangeListener

import com.industree.app.main.*

class DetailActivity : BaseActivity() {

    @BindView(R.id.backRL)
    lateinit var backRL: RelativeLayout

    @BindView(R.id.imageRL)
    lateinit var imageRL: ImageView

    @BindView(R.id.bookingServiceTV)
    lateinit var bookingServiceTV: TextView

    @BindView(R.id.brand_nameTV)
    lateinit var brand_nameTV: TextView

    @BindView(R.id.descriptionTV)
    lateinit var descriptionTV: TextView

    @BindView(R.id.txtWebsiteTV)
    lateinit var txtWebsiteTV: TextView

    @BindView(R.id.txtInstagramTV)
    lateinit var txtInstagramTV: TextView

    @BindView(R.id.txtFacebookTV)
    lateinit var txtFacebookTV: TextView

    @BindView(R.id.txtTwiterTV)
    lateinit var txtTwiterTV: TextView

    @BindView(R.id.txtYoutubeTV)
    lateinit var txtYoutubeTV: TextView

    @BindView(R.id.txtEmailTV)
    lateinit var txtEmailTV: TextView

    @BindView(R.id.favouriteIV)
    lateinit var favouriteIV: ImageView

    @BindView(R.id.websiteRL)
    lateinit var websiteRL: RelativeLayout

    @BindView(R.id.instagramRL)
    lateinit var instagramRL: RelativeLayout

    @BindView(R.id.facebookRL)
    lateinit var facebookRL: RelativeLayout

    @BindView(R.id.twiterRL)
    lateinit var twiterRL: RelativeLayout

    @BindView(R.id.youTubeRL)
    lateinit var youTubeRL: RelativeLayout

    @BindView(R.id.emailRL)
    lateinit var emailRL: RelativeLayout

    @BindView(R.id.contactRL)
    lateinit var contactRL: RelativeLayout

    @BindView(R.id.chatRL)
    lateinit var chatRL: RelativeLayout

    @BindView(R.id.servicesRV)
    lateinit var servicesRV: RecyclerView

    @BindView(R.id.ratingBarRL)
    lateinit var ratingBarRL: RelativeLayout

    @BindView(R.id.exp_rating)
    lateinit var exp_rating: RatingBar

    @BindView(R.id.showAllReview)
    lateinit var showAllReview: TextView

    @BindView(R.id.reviewRV)
    lateinit var reviewRV: RecyclerView

    @BindView(R.id.noMsgReview)
    lateinit var noMsgReview: TextView

    var vendor_id: String? = null
    var brand_id: String? = null
    var brand_image: String? = null
    var user_id: String? = null
    var categoryId: String? = null
    var youtube_link: String? = null
    var website_link: String? = null
    var instagram_link: String? = null
    var facebook_link: String? = null
    var twiter_link: String? = null
    var email_link: String? = null
    var servicesListAdapter: ServicesListAdapter? = null
    lateinit var mGetVendorDetailModel: GetVendorDetailModel
    var servicesArrayList: ArrayList<ServicesItem?>? = ArrayList()
  //  var reviewArrayList: ArrayList<ReviewItem?>? = ArrayList()
    var reviewAdapter: ReviewAdapter? = null
    private val requestCall = 1

    //    val  newArray = IntArray()
    var newArray = arrayOf<Int>()
    var arrayList = ArrayList<Int>()

    var countArrayList = ArrayList<String>()
    var serviceIdArrayList = ArrayList<String>()
    var totalArrayList = java.util.ArrayList<String>()
    var start:String?= null
    var bookingAmount: String? = null
    var formatted: String? = null
    var vendor_name:String?=null
     var mReview : ReviewItem? = null
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        ButterKnife.bind(mActivity)
        val gfgPolicy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(gfgPolicy)

        user_id = intent.getStringExtra("user_id")
        vendor_id = intent.getStringExtra("vendor_id")
        brand_id = intent.getStringExtra("brand_id")
        brand_image = intent.getStringExtra("brand_image")
        categoryId = intent.getStringExtra("categoryId")
        makeStatusBarTransparent()
        fun View.setMarginTop(marginTop: Int) {
            val menuLayoutParams = this.layoutParams as ViewGroup.MarginLayoutParams
            menuLayoutParams.setMargins(0, marginTop, 0, 0)
            this.layoutParams = menuLayoutParams
        }
        exp_rating.setOnTouchListener(View.OnTouchListener { v, event ->
            if (event.action == MotionEvent.ACTION_UP) {
                // TODO perform your action here
                performYellowStarClick()
            }
            return@OnTouchListener true
        })
    }

    override fun onResume() {
        super.onResume()
            if (isNetworkAvailable(mActivity)) {
                executeVendorDetailRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }

    }

    @RequiresApi(Build.VERSION_CODES.O)
    @OnClick(
    //    R.id.ratingBarRL,
        R.id.backRL,
        R.id.txtWebsiteTV,
        R.id.txtInstagramTV,
        R.id.txtFacebookTV,
        R.id.txtTwiterTV,
        R.id.txtYoutubeTV,
        R.id.txtEmailTV,
        R.id.favouriteIV,
        R.id.contactRL,
        R.id.chatRL,
        R.id.bookingServiceRL,
        R.id.showAllReview
    )
    fun onViewClicked(view: View) {
        when (view.id) {
        //    R.id.ratingBarRL -> performYellowStarClick()
            R.id.backRL -> onBackPressed()
            R.id.txtWebsiteTV -> onWebsiteClick()
            R.id.txtInstagramTV -> onInstagramClick()
            R.id.txtFacebookTV -> onFacebookClick()
            R.id.txtTwiterTV -> onTwiterClick()
            R.id.txtYoutubeTV -> onYoutubeClick()
            R.id.favouriteIV -> onFavouriteClick()
            R.id.contactRL -> onContactClick()
            R.id.chatRL -> onChatClick()
            R.id.bookingServiceRL -> OnBookingClick()
            R.id.showAllReview -> onShowAllReviewClick()
        }
    }

    private fun onShowAllReviewClick() {
        var mIntent : Intent = Intent(mActivity, ReviewActivity::class.java)
        mIntent.putExtra("user_id",user_id)
        mIntent.putExtra("vendor_id",vendor_id)
        mIntent.putExtra("brand_id",brand_id)
        startActivity(mIntent)
    }

    private fun onChatClick() {
        if (isNetworkAvailable(mActivity!!)){
            executeCreateRoomApi()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }
    private fun executeCreateRoomApi() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["other_user_id"] = user_id
        mMap["vendor_id"] = vendor_id
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.createRoomIdRequest(mHeaders,mMap)
        call.enqueue(object : Callback<CreateRoomModel> {
            override fun onFailure(call: Call<CreateRoomModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<CreateRoomModel>, response: Response<CreateRoomModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mCreateRoomModel = response.body()!!
                if (mCreateRoomModel!!.status == 1) {

                    var mIntent : Intent = Intent(mActivity, ChatActivity::class.java)
                    mIntent.putExtra("room_id",mCreateRoomModel.data!!.roomId)
                    mIntent.putExtra("other_user_name",vendor_name)
                    startActivity(mIntent)
                } else {
                    showToast(mActivity,mCreateRoomModel?.message)
                }
            }
        })
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun OnBookingClick() {
        if(bookingServiceTV.text.toString().equals("0.0")){
            showAlertDialog(mActivity,getString(R.string.please_add_atleast_onr_service))
        }
        else {
            if (isNetworkAvailable(mActivity)) {
                executeBookingVendorRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun executeBookingVendorRequest() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val current = LocalDate.now()
            val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
            formatted =  current.format(formatter)
        } else {
            var date = Date()
            val formatter = SimpleDateFormat("dd-MM-yyyy")
            formatted = formatter.format(date)
        }
//        val current = LocalDate.now()
//        val formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy")
//        formatted = current.format(formatter)
        bookingAmount = bookingServiceTV.text.toString()
        start=(System.currentTimeMillis() / 1000L).toString()

        var services: ArrayList<ServicesItemData?>? = ArrayList()

        for(i in 0 until serviceIdArrayList!!.size){
            val list = ServicesItemData(countArrayList.get(i), serviceIdArrayList.get(i))
            services!!.add(list)
        }

       var mBookingSubmitModel = BookingSubmitModel(start,bookingAmount,getLoggedInUserID(),vendor_id,services,formatted,brand_id)

        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.BookingVendorRequest(mHeaders, mBookingSubmitModel)
        call.enqueue(object : Callback<BookingVendorModel> {
            override fun onFailure(call: Call<BookingVendorModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<BookingVendorModel>,
                response: Response<BookingVendorModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mBookingVendorModel = response.body()!!
                if (mBookingVendorModel.status == 1) {
                    showBookedAlertDialog(mActivity,mBookingVendorModel.message)
                } else {
                    showToast(mActivity, mBookingVendorModel.message)
                }
            }
        })
    }
    fun showBookedAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            finish()
        }
        alertDialog.show()
    }
    private fun onContactClick() {
        showContactBottomSheet()
    }

    private fun showContactBottomSheet() {
        val view: View =
            LayoutInflater.from(mActivity).inflate(R.layout.layout_contact_bottom_sheet, null)
        val contactDialog: BottomSheetDialog? =
            mActivity.let { BottomSheetDialog(it, R.style.BottomSheetDialog) }
        contactDialog!!.setContentView(view)
        contactDialog.setCanceledOnTouchOutside(true)
        //disabling the drag down of sheet
        contactDialog.setCancelable(true)
        //cancel button click
        val cancelTV: TextView? = contactDialog.findViewById(R.id.btnCancelTV)
        cancelTV?.setOnClickListener {
            contactDialog.dismiss()
        }
        val callLL: LinearLayout? = contactDialog.findViewById(R.id.callLL)
        val txtCallTV: TextView? = contactDialog.findViewById(R.id.txtCallTV)
        txtCallTV!!.text = mGetVendorDetailModel.data!!.phoneNo
        callLL?.setOnClickListener {
            makePhoneCall()
            contactDialog.dismiss()
        }
        contactDialog.show()
    }

    private fun makePhoneCall() {
        val number: String = mGetVendorDetailModel.data!!.phoneNo!!
        if (number.trim { it <= ' ' }.isNotEmpty()) {
            if (ContextCompat.checkSelfPermission(
                    mActivity,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(
                    mActivity,
                    arrayOf(Manifest.permission.CALL_PHONE),
                    requestCall
                )
            } else {
                val dial = "tel:$number"
                startActivity(Intent(Intent.ACTION_CALL, Uri.parse(dial)))
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == requestCall) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                makePhoneCall()
            } else {
                Toast.makeText(this, "Permission DENIED", Toast.LENGTH_SHORT).show()
            }
        }
    }


    private fun onFavouriteClick() {
        if (isNetworkAvailable(mActivity)) {
            executeFavUnfavExecute()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    private fun mParameters(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["vendor_id"] = vendor_id
        mMap["category_id"] = categoryId
        mMap["brand_id"]=brand_id
        Log.e(TAG, "**PARAM**$mMap").toString()
        return mMap
    }

    private fun executeFavUnfavExecute() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.FavouriteUnfavouriteRequest(mHeaders, mParameters())
        call.enqueue(object : Callback<FavouriteUnfavouriteModel> {
            override fun onFailure(call: Call<FavouriteUnfavouriteModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<FavouriteUnfavouriteModel>,
                response: Response<FavouriteUnfavouriteModel>
            ) {
                dismissProgressDialog()
                val mFavouriteUnfavouriteModel = response.body()
                if (mFavouriteUnfavouriteModel!!.status == 1) {
                    if ((mFavouriteUnfavouriteModel.isFav).equals("0")) {
                        favouriteIV.setImageResource(R.drawable.ic_unfav)
                        showAlertDialog(mActivity, brand_nameTV.text.toString() +" removed from favorites"
                        )
                    } else if ((mFavouriteUnfavouriteModel.isFav).equals("1")) {
                        favouriteIV.setImageResource(R.drawable.stroke_heart)
                        showAlertDialog(
                            mActivity, brand_nameTV.text.toString() + " added to favorites"
                        )
                    }
                } else if (mFavouriteUnfavouriteModel.status == 0) {
                    showAlertDialog(mActivity, mFavouriteUnfavouriteModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    private fun onYoutubeClick() {
        try {
            val intent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/" + youtube_link))
            mActivity.startActivity(intent)
        } catch (e: Exception) {
            showAlertDialog(
                mActivity,
                "This link couldn't be opened. Check the link and try again."
            )
        }
    }

    private fun onTwiterClick() {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://twiter.com/" + twiter_link))
            mActivity.startActivity(intent)
        } catch (e: Exception) {
            showAlertDialog(
                mActivity,
                "This link couldn't be opened. Check the link and try again."
            )
        }
    }

    private fun onFacebookClick() {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://facebook.com/" + facebook_link))
            startActivity(intent)
        } catch (e: Exception) {
            showAlertDialog(
                mActivity,
                "This link couldn't be opened. Check the link and try again."
            )
        }
    }

    private fun onInstagramClick() {
        try {
            val intent =
                Intent(Intent.ACTION_VIEW, Uri.parse("http://instagram.com/" + instagram_link))
            mActivity.startActivity(intent)
        } catch (e: Exception) {
            showAlertDialog(
                mActivity,
                "This link couldn't be opened. Check the link and try again."
            )
        }
    }

    private fun onWebsiteClick() {
        try {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse(website_link))
            mActivity.startActivity(intent)
        } catch (e: Exception) {
            showAlertDialog(
                mActivity,
                "This link couldn't be opened. Check the link and try again."
            )
        }
    }

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["category_id"] = categoryId
        mMap["user_id"] = getLoggedInUserID()
        mMap["vendor_id"] = vendor_id
        mMap["brand_id"] = brand_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeVendorDetailRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.GetVendorDetailRequest(mHeaders, mParam())
        call.enqueue(object : Callback<GetVendorDetailModel> {
            override fun onFailure(call: Call<GetVendorDetailModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetVendorDetailModel>,
                response: Response<GetVendorDetailModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mGetVendorDetailModel = response.body()!!
                if (mGetVendorDetailModel.status == 1) {
                    if (mGetVendorDetailModel.category!!.size != 0) {
                        servicesArrayList = mGetVendorDetailModel.category!!.get(0)?.services
                        if (mGetVendorDetailModel.data!!.review != "") {
                            if (mGetVendorDetailModel.data!!.review is Any) {
                                val getResultCon = mGetVendorDetailModel.data!!.review
                                val gson = Gson()
                                val jsonElement = gson.toJsonTree(getResultCon)
                                val type: Type = object : TypeToken<List<ReviewItem?>?>() {}.type
                                val reviewArrayList: ArrayList<ReviewItem> = gson.fromJson(jsonElement, type)
                                     reviewRV.visibility = View.VISIBLE
                                     noMsgReview.visibility=View.GONE
                                         setReviewAdapter(reviewArrayList)
                                if(reviewArrayList.size >=2){
                                    showAllReview.visibility=View.VISIBLE
                                }
                                else{
                                    showAllReview.visibility=View.GONE
                                }
                             }
                             else{
                                reviewRV.visibility = View.GONE
                                noMsgReview.visibility=View.VISIBLE
                                showAllReview.visibility=View.GONE
                             }
                        }
                        else{
                            reviewRV.visibility = View.GONE
                            noMsgReview.visibility=View.VISIBLE
                            showAllReview.visibility=View.GONE
                        }

                        vendor_name= mGetVendorDetailModel.data!!.vendorName
                        if (servicesArrayList!!.size == 0) {
                            servicesRV.visibility = View.GONE
                        } else {
                            setAdapter()
                            servicesRV.visibility = View.VISIBLE
                        }

                    }
                    if (brand_image.equals("") || brand_image.equals(null)) {
                        Glide.with(mActivity)
                            .load(R.drawable.image_placeholder)
                            .placeholder(R.drawable.ic_placeholder_image)
                            .error(R.drawable.ic_placeholder_image)
                            .into(imageRL)
                    } else {
                        Glide.with(mActivity)
                            .load(brand_image)
                            .placeholder(R.drawable.ic_placeholder_image)
                            .error(R.drawable.ic_placeholder_image)
                            .into(imageRL)
                    }
                    if (mGetVendorDetailModel.category!!.size != 0) {
                        brand_nameTV.text = (mGetVendorDetailModel.category!!.get(0)!!.brandName)
                    } else {
                        brand_nameTV.text = mGetVendorDetailModel.data!!.vendorName
                    }
                    exp_rating.count = mGetVendorDetailModel.data!!.rating!!.toDouble().roundToInt()
                    descriptionTV.text = mGetVendorDetailModel.data!!.description
                    txtEmailTV.text = mGetVendorDetailModel.data!!.email
                    website_link = mGetVendorDetailModel.data!!.website
                    instagram_link = mGetVendorDetailModel.data!!.instagram
                    facebook_link = mGetVendorDetailModel.data!!.facebook
                    twiter_link = mGetVendorDetailModel.data!!.twitter
                    youtube_link = mGetVendorDetailModel.data!!.youtube
                    email_link = mGetVendorDetailModel.data!!.email

                    if (website_link.equals("") || website_link.equals(null)) {
                        websiteRL.visibility = View.GONE
                    } else {
                        websiteRL.visibility = View.VISIBLE
                    }
                    if (instagram_link.equals("") || instagram_link.equals(null)) {
                        instagramRL.visibility = View.GONE
                    } else {
                        instagramRL.visibility = View.VISIBLE
                    }
                    if (facebook_link.equals("") || facebook_link.equals(null)) {
                        facebookRL.visibility = View.GONE
                    } else {
                        facebookRL.visibility = View.VISIBLE
                    }
                    if (twiter_link.equals("") || twiter_link.equals(null)) {
                        twiterRL.visibility = View.GONE
                    } else {
                        twiterRL.visibility = View.VISIBLE
                    }
                    if (youtube_link.equals("") || youtube_link.equals(null)) {
                        youTubeRL.visibility = View.GONE
                    } else {
                        youTubeRL.visibility = View.VISIBLE
                    }
                    if (email_link.equals("") || email_link.equals(null)) {
                        emailRL.visibility = View.GONE
                    } else {
                        emailRL.visibility = View.VISIBLE
                    }
                } else {
                    showToast(mActivity, mGetVendorDetailModel.message)
                }
                if ((mGetVendorDetailModel.data?.isFav).equals("0")) {
                    favouriteIV.setImageResource(R.drawable.ic_unfav)
                } else if ((mGetVendorDetailModel.data?.isFav).equals("1")) {
                    favouriteIV.setImageResource(R.drawable.stroke_heart)
                }
            }
        })
    }

    private fun setReviewAdapter(reviewArrayList: ArrayList<ReviewItem>) {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        reviewRV.layoutManager = layoutManager
        reviewAdapter = ReviewAdapter(mActivity, reviewArrayList)
        reviewRV.adapter = reviewAdapter
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        servicesRV.layoutManager = layoutManager
        servicesListAdapter = ServicesListAdapter(mActivity, servicesArrayList, mBookingClickListener)
        servicesRV.adapter = servicesListAdapter
    }

    var mBookingClickListener: BookingClickListener = object : BookingClickListener {
        override fun onItemClickListener(
            totalArrayList: IntArray,
            position: Int,
            size: Int,
            count: Int,
            serviceId: Int,
            serviceIdIntArrayList: IntArray
        ) {
            serviceIdArrayList.clear()
            for(i in 0 until serviceIdIntArrayList.size){
                serviceIdArrayList.add(i,serviceIdIntArrayList.get(i).toString())
            }
            var countArray = IntArray(size)
            var serviceIdArray = IntArray(size)
            for(i in 0 until size){
                countArrayList.add(i,"0")
       //         serviceIdArrayList.add(i,"0")
            }

            for (i in 0 until size) {
                if (i == position) {
                    if (position >= 0 && position < countArray.size) {
                        countArray.set(position,count)
                        serviceIdArray.set(position, serviceId!!)
                        countArrayList.set(position, count.toString())
       //                 serviceIdArrayList.set(position, serviceId!!.toString())
                    }
                    else {
                        if ( position > arrayList.size) {
                            countArray[position]=0
                            serviceIdArray[position]=serviceId
                            countArrayList.add(position,"0")
        //                    serviceIdArrayList.add(position,serviceId.toString())
                        } else {
                            countArray[position]=count
                            serviceIdArray[position]=serviceId
                            countArrayList.add(position,count.toString())
        //                    serviceIdArrayList.add(position,serviceId.toString())
                        }
                    }

                }
            }
            var sum = 0
            for (number in totalArrayList) {
                sum += number
            }
            bookingServiceTV.text = "" + sum.toDouble()
        }
    }

    private fun performYellowStarClick() {
        val mainIntent = Intent(mActivity, SubmitRatingActivity::class.java)
        mainIntent.putExtra("vendor_id", vendor_id)
        mainIntent.putExtra("brand_id",brand_id)
        startActivity(mainIntent)
    }

    @SuppressLint("ObsoleteSdkInt")
    fun Activity.makeStatusBarTransparent() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.apply {
                clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    decorView.systemUiVisibility =
                        View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN

                } else {
                    decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                }
                statusBarColor = Color.TRANSPARENT
            }
        }
    }
}

