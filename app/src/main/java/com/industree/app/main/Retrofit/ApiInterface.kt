package com.industree.app.main.Retrofit


import com.industree.app.main.Model.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {
    @POST("signUp.php")
    fun signUpDataRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<SignUpModel>

    @POST("logIn.php")
    fun signInDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<SignInModel>
    @POST("forgetPassword.php")
    fun ForgetPasswordDataRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<ForgetPasswordModel>

    @POST("home.php")
    fun HomeDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<HomeDataModel>

    @POST("getVendorDetail.php")
    fun GetVendorDetailRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetVendorDetailModel>


    @POST("categoryListing.php")
    fun categoryListingDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<CategoryListingModel>

    @POST("badgeCount.php")
    fun notificationBadgeDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<NotificationBadgeCountModel>

    @POST("submitRating.php")
    fun AddRatingRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<SubmitReportModel>

    @POST("getAllFavoriteVendor.php")
    fun FavouriteDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllfavouriteVendorModel>

    @POST("favouriteUnfavouriteVendor.php")
    fun FavouriteUnfavouriteRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<FavouriteUnfavouriteModel>

    @POST("bookingListing.php")
    fun bookingServiceDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<BookingListingModel>

    @POST("bookingDetails.php")
    fun BookingServiceDetailRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<BookingServiceDetailModel>

    @POST("sessionEnd.php")
    fun SessionEndRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<SessionEndModel>

    @POST("markAsPayed.php")
    fun MarkAsPaidRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<MarkAsPaidModel>

    @POST("cashPayment.php")
    fun CashPaymentRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<CashPaymentModel>

    @Multipart
    @POST("editProfile.php")
    fun EditProfileDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Part("user_id") user_id: RequestBody?,
        @Part("first_name") first_name: RequestBody?,
        @Part("last_name") last_name: RequestBody?,
        @Part("phone_no") phone_no: RequestBody?,
        @Part("email") email: RequestBody?,
        @Part profileImage: MultipartBody.Part?
    ): Call<EditProfileModel>

    @POST("booking.php")
    fun BookingVendorRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: BookingSubmitModel
    ): Call<BookingVendorModel>

    @POST("help.php")
    fun HelpRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<HelpModel>

    @POST("getAllChatUser.php")
    fun getChatUsersDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllUserChatModel>

    @POST("createRoom.php")
    fun createRoomIdRequest(
        @HeaderMap Token: MutableMap<String, String>,
        @Body mParams: Map<String?, String?>?
    ): Call<CreateRoomModel>


    @POST("getAllChatMessages.php")
    fun getAllChatMsgesRequest(
        @HeaderMap Token: MutableMap<String, String>,
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllChatMessageModel>

    @POST("sendMessage.php")
    fun sendMessageRequest(
        @HeaderMap Token: MutableMap<String, String>,
        @Body mParams: Map<String?, String?>?
    ): Call<SendMessageModel>

    @POST("updateMessageSeenStatus.php")
    fun UpdateMessageSeenRequest(
        @HeaderMap Token: MutableMap<String, String>,
        @Body mParams: Map<String?, String?>?
    ): Call<UpdateMessageSeenModel>

    @POST("logout.php")
    fun logOutProfileRequest(
        @HeaderMap Token: MutableMap<String, String>
    ): Call<LogoutModel>

    @POST("getAllVendorByUserID.php")
    fun getAllVendorByUserIdDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetAllVendorByUserIdModel>

    @POST("ratingDetail.php")
    fun RatingDetailRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<RatingDetailModel>

    @POST("removeService.php")
    fun DeleteServiceDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<DeleteServiceModel>

    @POST("addBusiness.php")
    fun AddBusinessDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: MutableMap<String?, Any?>
    ): Call<AddBusinessModel>

    @POST("googleLogin.php")
    fun googleLogInDataRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<GoogleLoginModel>

    @POST("facebookLogin.php")
    fun facebookLogInDataRequest(
        @Body mParams: Map<String?, String?>?
    ): Call<FacebookLoginModel>

    @POST("notificationActivity.php")
    fun NotificationDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<NotificationModel>

    @POST("vendorBookingListing.php")
    fun VendorBookingListingDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<VendorBookingListingModel>

    @POST("changePassword.php")
    fun changePasswordDataRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<ChangePasswordModel>

    @POST("getProfileDetail.php")
    fun getProfileDetailDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetProfileDetailModel>


    @POST("PaypalLogin.php")
    fun payPalDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<PaypalLoginModel>

    @POST("getAccessTokenForPaypal.php")
    fun GetAccessTokenRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetAccessTokenModel>

    @POST("getPaypalTransactionDetails.php")
    fun GetPaypalTransactionDetailsRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<GetPaypalTransactionDetailsModel>


    @POST("subscription.php")
    fun getSubscriptionDataRequest(
        @HeaderMap Token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<SubscriptionModel>


    @POST("checkPlan.php")
    fun CheckPlanRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<CheckPlanModel>

    @GET
    fun searchPlacesRequest(
        @Url mUrl: String,
    ): Call<PlacesModel>?

    @GET
    fun searchPlacesLatLongRequest(
        @Url mUrl: String,
    ): Call<PlacesLatLongModel>?


    @POST("AcceptRejectbooking.php")
    fun AcceptRejectbookingRequest(
        @HeaderMap token: Map<String, String>?,
        @Body mParams: Map<String?, String?>?
    ): Call<AcceptRejectbookingModel>
}