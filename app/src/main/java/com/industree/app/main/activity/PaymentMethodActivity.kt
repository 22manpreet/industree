package com.industree.app.main.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R

class PaymentMethodActivity : BaseActivity() {
    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.connect_paypal)
    lateinit var connect_paypal: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_method)
        ButterKnife.bind(mActivity)
    }
    @OnClick(
        R.id.backRL,R.id.connect_paypal
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.connect_paypal -> connectpaypal()
        }
    }

    private fun connectpaypal() {
        val intent = Intent(mActivity, PaypalConnectActivity::class.java)
        startActivity(intent)
    }
}