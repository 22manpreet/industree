package com.industree.app.main.utils

import android.app.Application
import com.paypal.checkout.PayPalCheckout
import com.paypal.checkout.config.CheckoutConfig
import com.paypal.checkout.config.Environment
import com.paypal.checkout.config.SettingsConfig
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.UserAction

import io.socket.client.IO
import io.socket.client.Socket
import java.net.URISyntaxException


class IndustreeApplication: Application() {
    /**
     * Getting the Current Class Name
     */
    val TAG: String = "IndustreeApp"

    /**
     * Initialize the Applications Instance
     */
    private var mInstance: IndustreeApplication? = null

    /**
     * Synchronized the Application Class Instance
     */
    @Synchronized
    open fun getInstance(): IndustreeApplication? {
        return mInstance
    }
    /*
    * Socket
    * */
    var mSocket: Socket? = null

    override fun onCreate() {
        super.onCreate()

        mInstance = this

        //Set Socket
        try {
            mSocket = IO.socket(SOCKET_URL)
        } catch (e: URISyntaxException) {
            throw RuntimeException(e)
        }

        //Set Branch ios
//        val branch = Branch.getAutoInstance(this)
//        if (BuildConfig.DEBUG) {
//            branch.setDebug()
////        }
//        val config = CheckoutConfig(
//            application = this,
//            clientId = PAYPAL_KEY,
//            environment = Environment.LIVE,
//            returnUrl = "com.industree.app://paypalpay",
//            currencyCode = CurrencyCode.USD,
//            userAction = UserAction.PAY_NOW,
//            settingsConfig = SettingsConfig(
//                loggingEnabled = true
//            )
//        )
//        PayPalCheckout.setConfig(config)
   }

    fun getSocket(): Socket? {
        return mSocket
    }
}