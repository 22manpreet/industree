package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RatingDetailModel(

	@field:SerializedName("lastPage")
	val lastPage: String? = null,

	@field:SerializedName("data")
	val data: ArrayList<ReviewDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class ReviewDataItem(

	@field:SerializedName("rating_id")
	val ratingId: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("review")
	val review: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null
) : Parcelable
