package com.industree.app.main.fcm

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CustomNotiModel (

    @field:SerializedName("room_id")
    val roomId: String? = "",

    @field:SerializedName("notification_type")
    val notificationType: String? = "",

    @field:SerializedName("notification_read_status")
    val notificationReadStatus: String? = "",

    @field:SerializedName("user_id")
    val userId: String? = "",

    @field:SerializedName("other_user_id")
    val otherUserId: String? = "",

    @field:SerializedName("user_name")
    val userName: String? = "",

    @field:SerializedName("created")
    val created: String? = "",

    @field:SerializedName("description")
    val description: String? = "",

    @field:SerializedName("notification_id")
    val notificationId: String? = "",

    @field:SerializedName("profileImage")
    val profileImage: String? = "",

    @field:SerializedName("title")
    val title: String? = "",

    @field:SerializedName("detail_id")
    val detailId: String? = ""
    ) : Parcelable
