package com.industree.app.main.activity

import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.cardview.widget.CardView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.EasyLocationProvider
import com.facebook.*
import com.facebook.appevents.AppEventsLogger
import com.facebook.internal.CallbackManagerImpl
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.messaging.FirebaseMessaging
import com.industree.app.R
import com.industree.app.main.Model.FacebookLoginModel
import com.industree.app.main.Model.GoogleLoginModel
import com.industree.app.main.Model.SignInModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.utils.*
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.URL
import java.util.*

class SigninActivity : BaseActivity() {
    @BindView(R.id.txtSignUpTV)
    lateinit var txtSignUpTV: TextView

    @BindView(R.id.txtLogInTV)
    lateinit var txtLogInTV: TextView

    @BindView(R.id.txtForgotPasswordTV)
    lateinit var txtForgotPasswordTV: TextView

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: EditText

    @BindView(R.id.editPasswordET)
    lateinit var editPasswordET: EditText

    @BindView(R.id.googleCV)
    lateinit var googleCV: CardView

    @BindView(R.id.facebookCV)
    lateinit var facebookCV: CardView

    @BindView(R.id.toggle_btn)
    lateinit var toggle_btn: TextView

    @BindView(R.id.pwdll)
    lateinit var pwdll: LinearLayout

    var latitude=0.0
    var longitude= 0.0
    var mDeviceToken: String = ""
    var REQUEST_PERMISSION_CODE=325
    var isCurrentLocation=true
    var easyLocationProvider: EasyLocationProvider?=null
    var mAccessFineLocation =android.Manifest.permission.ACCESS_FINE_LOCATION
    var mAccessCourseLocation =android.Manifest.permission.ACCESS_COARSE_LOCATION
    lateinit var  mGoogleSignInClient: GoogleSignInClient
    lateinit var mFirebaseAuth: FirebaseAuth
    var mGoogleUserId:String?=null
    var mGoogleEmail:String?=null
    var mGoogleProfileImage:String?=null
    var mGoogleFirstName:String?=null
    var mGoogleLastName:String?=null
    var facebookId:String?=null
    var fbFirstName:String?=null
    var fbLastName:String?=null
    var fbEmail:String?=null
    var lg_PhotoUrl:String?=null
    var fbToken:String? = null
    var fbUsername:String? = null
    lateinit var  callbackManager: CallbackManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        ButterKnife.bind(mActivity)
        getDeviceToken()
        performLocationClick()
        initializeGoogle()

        editPasswordET.setOnFocusChangeListener { v, hasFocus ->
            if(hasFocus){
                pwdll.background=getDrawable(R.drawable.edittext_pwd)
            }else{
                pwdll.background=getDrawable(R.drawable.edittext)
            }
        }
    }
    @OnClick(
        R.id.txtSignUpTV,
        R.id.txtLogInTV,
        R.id.txtForgotPasswordTV,
        R.id.googleCV,
        R.id.facebookCV,
        R.id.toggle_btn
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtSignUpTV -> performSignUpClick()
            R.id.txtLogInTV -> performLogInClick()
            R.id.txtForgotPasswordTV -> performForgetPasswordClick()
            R.id.googleCV -> onGoogleClick()
            R.id.facebookCV -> onFacebookClick()
            R.id.toggle_btn -> clickOnshowHidePwd(toggle_btn)
        }
    }
    private fun clickOnshowHidePwd(toggle_btn: TextView) {
        if(editPasswordET.getTransformationMethod().equals(PasswordTransformationMethod.getInstance())){
            toggle_btn.background=getDrawable(R.drawable.ic_show_pwd_toggle)

            //Show Password
            editPasswordET.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
        }
        else{
            toggle_btn.background=getDrawable(R.drawable.ic_hide_pwd_toggle)

            //Hide Password
            editPasswordET.setTransformationMethod(PasswordTransformationMethod.getInstance());

        }
    }
    private fun onFacebookClick() {
        if (!isNetworkAvailable(mActivity)) {
            showAlertDialog(mActivity, getString(R.string.internet_connection_error))
        } else {
            FacebookSdk.sdkInitialize(applicationContext)
            LoginManager.getInstance().logOut()
            AppEventsLogger.activateApp(application)
            callbackManager = CallbackManager.Factory.create()
            facebookLogin()
        }
    }
    private fun facebookLogin() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "email"))
        LoginManager.getInstance().registerCallback(callbackManager, object :
            FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {
                Log.e("onSuccess: ", loginResult.accessToken.token)
                getFacebookData(loginResult)
            }

            override fun onCancel() {
                Log.e("FacebookData", "onCancel" + "onCancel")
            }

            override fun onError(error: FacebookException) {
                Log.e("FacebookData", "FacebookException" + "FacebookException")
            }
        })
    }

    fun getFacebookData(loginResult: LoginResult) {
        showProgressDialog(mActivity)
        val graphRequest = GraphRequest.newMeRequest(loginResult.accessToken) { `object`, response ->
            dismissProgressDialog()
            try {
                if (`object`!!.has("id")) {
                    facebookId = `object`.getString("id")
                }
                if (`object`.has("first_name")) {
                    fbFirstName = `object`.getString("first_name")
                }
                if (`object`.has("last_name")) {
                    fbLastName = `object`.getString("last_name")
                }
                if (`object`.has("email")) {
                    try{
                    fbEmail = `object`.getString("email")
                    }
                    catch (e:Exception){
                        println("Exception: "+e)
                    }
                }

                val jsonObject = JSONObject(`object`.getString("picture"))
                if (jsonObject != null) {
                    val dataObject = jsonObject.getJSONObject("data")
                    lg_PhotoUrl = URL("https://graph.facebook.com/" + facebookId.toString() + "/picture?width=500&height=500").toString()
                }
                fbToken = facebookId
                Log.e("fbToken: ", loginResult.accessToken.token)
                Log.e("token ", fbToken!!)
                fbUsername="$fbFirstName $fbLastName"
                val dataObject = jsonObject.getJSONObject("data")
                executeFacebookApi()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        val bundle = Bundle()
        Log.e("SignUpActivity", "bundle set")
        bundle.putString("fields", "id,first_name,last_name,email,picture")
        graphRequest.parameters = bundle
        graphRequest.executeAsync()
    }
    private fun facebookParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["first_name"] = fbFirstName
        mMap["last_name"] = fbLastName
        mMap["email"]= fbEmail
        mMap["device_token"] = mDeviceToken
        mMap["device_type"] = "2"
        mMap["latitude"] = latitude.toString()
        mMap["longitude"] = longitude.toString()
        mMap["facebook_token"] = fbToken
        Log.e("FbParams", mMap.toString())
        return mMap
    }
    private fun executeFacebookApi() {
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.facebookLogInDataRequest(facebookParams())

        call.enqueue(object : Callback<FacebookLoginModel> {
            override fun onFailure(call: Call<FacebookLoginModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<FacebookLoginModel>, response: Response<FacebookLoginModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mFacebookLoginModel = response.body()
                if (mFacebookLoginModel!!.status == 1) {
                    AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                    AppPrefrences().writeString(mActivity, USERID, mFacebookLoginModel.data!!.userId)
                    AppPrefrences().writeString(mActivity, EMAIL, mFacebookLoginModel.data!!.email)
                    AppPrefrences().writeString(mActivity, FIRSTNAME, mFacebookLoginModel.data.firstName)
                    AppPrefrences().writeString(mActivity, LASTNAME, mFacebookLoginModel.data.lastName)
                    AppPrefrences().writeString(mActivity, PASSWORD, mFacebookLoginModel.data.password)
                    AppPrefrences().writeString(mActivity, PROFILEPIC,mFacebookLoginModel.data!!.profileImage)
                    AppPrefrences().writeString(mActivity, AUTHTOKEN, mFacebookLoginModel!!.data!!.authToken)
                    AppPrefrences().writeString(mActivity,PHONENO, mFacebookLoginModel!!.data!!.phoneNo)
                    AppPrefrences().writeString(mActivity, LATITUDE,"" +latitude)
                    AppPrefrences().writeString(mActivity, LONGITUDE, "" + longitude)

                    val i = Intent(mActivity, HomeActivity::class.java)
                    i.putExtra(NOTI_TYPE,"0")
                    startActivity(i)
                    finish()
                } else if (mFacebookLoginModel.status == 0) {
                    showAlertDialog(mActivity, mFacebookLoginModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun onGoogleClick() {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.google_alert_dialog)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnCancelTV = alertDialog.findViewById<TextView>(R.id.btnCancelTV)
        val btnContinueTV = alertDialog.findViewById<TextView>(R.id.btnContinueTV)
        val titleTV=alertDialog.findViewById<TextView>(R.id.titleTV)
        titleTV.text= "\"Industree theodeocampo \" Wants to Use \" google.com\" to Sign In"
        txtMessageTV.text = "This allows the app and website to share information about you."
        btnCancelTV.setOnClickListener {
            alertDialog.dismiss()
        }
        btnContinueTV.setOnClickListener {
            val signInIntent: Intent = mGoogleSignInClient!!.signInIntent
            startActivityForResult(signInIntent, GOOGLE_SIGN_IN)
            alertDialog.dismiss()
        }
        alertDialog.show()

    }
    private fun initializeGoogle() {
        FirebaseApp.initializeApp(this)
        // Configure Google Sign In
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mFirebaseAuth = FirebaseAuth.getInstance()
        FirebaseAuth.getInstance().signOut()
        signOut()
    }

    private fun signOut() {
        mGoogleSignInClient!!.signOut()
            .addOnCompleteListener(this) {
                Log.e(TAG, "==Logout Successfully==")
            }
    }
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode() && requestCode != null) {
            callbackManager!!.onActivityResult(requestCode, resultCode, data)
        }else{
            when (requestCode) {
                GOOGLE_SIGN_IN -> {
                    val completedTask: Task<GoogleSignInAccount> =
                        GoogleSignIn.getSignedInAccountFromIntent(data)
                    handleSignInResult(completedTask)
                }}
        }
    }
    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)
            mGoogleUserId = account.id.toString()
            Log.e("Token", account.idToken.toString())
            val name = account.displayName
            Log.e("ServerAuthCode", account.serverAuthCode.toString())
            mGoogleEmail = account.email.toString()
            mGoogleProfileImage = if (account.photoUrl != null) {
                account.photoUrl.toString()
            } else {
                ""
            }
            val idx = name!!.lastIndexOf(' ')
            if (idx == -1) {
                return
            }
            mGoogleFirstName = name.substring(0, idx)
            mGoogleLastName = name.substring(idx + 1)
            Log.i("mGoogleUserId", mGoogleUserId!!)
            executeGoogleLogInApi()
        } catch (e: ApiException) {
            Log.e("MyTAG", "signInResult:failed code=" + e.statusCode)
            //  showToast(mActivity, "Failed")
        }
    }
    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["first_name"] = mGoogleFirstName
        mMap["last_name"] = mGoogleLastName
        mMap["email"]= mGoogleEmail
        mMap["device_token"] = mDeviceToken
        mMap["device_type"] = "1"
        mMap["latitude"] = latitude.toString()
        mMap["longitude"] = longitude.toString()
        mMap["google_image"] = mGoogleProfileImage
        mMap["google_token"] = mGoogleUserId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeGoogleLogInApi() {
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.googleLogInDataRequest(mParams())

        call.enqueue(object : Callback<GoogleLoginModel> {
            override fun onFailure(call: Call<GoogleLoginModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<GoogleLoginModel>, response: Response<GoogleLoginModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mGoogleLoginModel = response.body()
                if (mGoogleLoginModel!!.status == 1) {
                    AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                    AppPrefrences().writeString(mActivity, USERID, mGoogleLoginModel.data!!.userId)
                    AppPrefrences().writeString(mActivity, EMAIL, mGoogleLoginModel.data!!.email)
                    AppPrefrences().writeString(mActivity, FIRSTNAME, mGoogleLoginModel.data.firstName)
                    AppPrefrences().writeString(mActivity, LASTNAME, mGoogleLoginModel.data.lastName)
                    AppPrefrences().writeString(mActivity, PASSWORD, mGoogleLoginModel.data.password)
                    AppPrefrences().writeString(mActivity, PROFILEPIC,mGoogleLoginModel.data!!.profileImage)
                    AppPrefrences().writeString(mActivity, AUTHTOKEN, mGoogleLoginModel!!.data!!.authToken)
                    AppPrefrences().writeString(mActivity,PHONENO, mGoogleLoginModel!!.data!!.phoneNo)
                    AppPrefrences().writeString(mActivity, LATITUDE,"" +latitude)
                    AppPrefrences().writeString(mActivity, LONGITUDE, "" + longitude)

                    val i = Intent(mActivity, HomeActivity::class.java)
                    i.putExtra(NOTI_TYPE,"0")
                    startActivity(i)
                    finish()
                } else if (mGoogleLoginModel.status == 0) {
                    showAlertDialog(mActivity, mGoogleLoginModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun getDeviceToken() {
        FirebaseApp.initializeApp(this)
        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.e(TAG, "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }
            // Get new FCM registration token
            mDeviceToken = task.result!!
            AppPrefrences().writeString(
                mActivity,
                DEVICE_TOKEN,
                mDeviceToken
            )
            Log.e(TAG, "getDeviceToken: " + mDeviceToken)
        })
    }
    private fun performForgetPasswordClick() {
        val mainIntent = Intent(this@SigninActivity, ForgotPasswordActivity::class.java)
        this@SigninActivity.startActivity(mainIntent)
    }

    private fun performSignUpClick() {
        val mainIntent = Intent(this@SigninActivity, SignupActivity::class.java)
        this@SigninActivity.startActivity(mainIntent)
    }

    private fun performLogInClick() {
        if (isValidate()) {
            if(isNetworkAvailable(mActivity)){
                executeSignInRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }

    // - - Validations for Sign In fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
            !isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
            editPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_password))
                flag = false
            }
        }
        return flag
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"] = editEmailET.text.toString().trim({ it <= ' ' })
        mMap["password"] = editPasswordET.text.toString().trim { it <= ' ' }
        mMap["device_token"] = mDeviceToken
        mMap["device_type"] = DEVICE_TYPE
        mMap["latitude"] = "" +latitude
        mMap["longitude"] = "" + longitude
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSignInRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.signInDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<SignInModel> {
            override fun onFailure(call: Call<SignInModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<SignInModel>, response: Response<SignInModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mSignInModel : SignInModel = response.body()!!
                if (mSignInModel!!.status == 1) {
                    showToast(mActivity, mSignInModel.message)
                    AppPrefrences().writeBoolean(mActivity, IS_LOGIN, true)
                    AppPrefrences().writeString(mActivity, USERID, mSignInModel.data!!.userId)
                    AppPrefrences().writeString(mActivity, EMAIL, mSignInModel.data.email)
                    AppPrefrences().writeString(mActivity, FIRSTNAME, mSignInModel.data.firstName)
                    AppPrefrences().writeString(mActivity, LASTNAME, mSignInModel.data.lastName)
                    AppPrefrences().writeString(mActivity, PASSWORD, mSignInModel.data.password)
                    AppPrefrences().writeString(mActivity, PROFILEPIC, mSignInModel.data.profileImage)
                    AppPrefrences().writeString(mActivity, AUTHTOKEN, mSignInModel.data.authToken)
                    AppPrefrences().writeString(mActivity,PHONENO, mSignInModel.data.phoneNo)
                    AppPrefrences().writeString(mActivity, LATITUDE,"" +latitude)
                    AppPrefrences().writeString(mActivity, LONGITUDE, "" + longitude)
                    AppPrefrences().writeString(mActivity,PASSWORD,editPasswordET.text.toString())

                    AppPrefrences().writeString(mActivity, ISSUBSCRIBED, mSignInModel.data.isSubscribed)
                    AppPrefrences().writeString(mActivity, PLANID, mSignInModel.data.planId)

                    val i = Intent(mActivity, HomeActivity::class.java)
                    i.putExtra(NOTI_TYPE,"0")
                    startActivity(i)
                    finish()
                } else if (mSignInModel!!.status == 2) {
                    showAlertDialog(mActivity, mSignInModel.message)
                    // showDoubleButtonAlertDialog(mActivity, mSignInModel.message, editEmailET.text.toString())
                } else if (mSignInModel!!.status == 0) {
                    showAlertDialog(mActivity, mSignInModel.message)
                }else {
                    showAlertDialog(mActivity, getString(R.string.you_have_not_verified_your_email_address_please_check_your_email))
                }
            }
        })
    }
    private fun performLocationClick() {
        if (checkLocationPermission()) {
            getLocationLatLong()
        } else {
            requestLocationPermission()
        }
    }

    private fun checkLocationPermission(): Boolean {
        val mlocationFineP =  ContextCompat.checkSelfPermission(mActivity, mAccessFineLocation)
        val mlocationCourseP = ContextCompat.checkSelfPermission(mActivity, mAccessCourseLocation)
        return mlocationFineP == PackageManager.PERMISSION_GRANTED && mlocationCourseP == PackageManager.PERMISSION_GRANTED
    }


    private fun requestLocationPermission() {
        ActivityCompat.requestPermissions(mActivity, arrayOf(mAccessFineLocation, mAccessCourseLocation), REQUEST_PERMISSION_CODE)
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_PERMISSION_CODE -> {
                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {
                    Log.e(TAG, "Permission has been denied by user")
                    showDialog(mActivity,getString(R.string.We_need_to_location_Please_permit_the_permission_through_Settings_screen))
                 //   requestLocationPermission()

                } else {
                    if(grantResults.isEmpty() || grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        //Perform Location Click Code
                        getLocationLatLong()
                    }
                    else{
                        checkLocationPermission()
                    }
                }
            }
        }
    }
    fun showDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            val intent = Intent()
            intent.action = Settings.ACTION_APPLICATION_DETAILS_SETTINGS
            val uri: Uri = Uri.fromParts("package", packageName, null)
            intent.data = uri
            startActivity(intent)
            alertDialog.dismiss()
        }
        alertDialog.show()
    }
    fun getLocationLatLong() {
        easyLocationProvider = EasyLocationProvider.Builder(mActivity)
            .setInterval(5000)
            .setFastestInterval(3000)
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setListener(object : EasyLocationProvider.EasyLocationCallback {
                override fun onGoogleAPIClient(googleApiClient: GoogleApiClient?, message: String) {
                    Log.e("EasyLocationProvider", "onGoogleAPIClient: $message")
                }

                override fun onLocationUpdated(mlatitude: Double, mlongitude: Double) {
                    Log.e(TAG, "onLocationUpdated:: Latitude: $mlatitude Longitude: $mlongitude")
                    if (isCurrentLocation) {
                        //dynamically
                        isCurrentLocation = false
                        latitude = mlatitude
                        longitude = mlongitude
                        AppPrefrences().writeString(mActivity, LATITUDE, ""+latitude)
                        AppPrefrences().writeString(mActivity, LONGITUDE, ""+longitude)
                    }
                }

                override fun onLocationUpdateRemoved() {
                    Log.e("EasyLocationProvider", "onLocationUpdateRemoved")
                }
            }).build()
        lifecycle.addObserver(easyLocationProvider!!)
    }

}