package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Base64
import android.util.Log
import android.widget.ImageView
import android.widget.Toast
import butterknife.BindView
import com.android.billingclient.api.BillingClient
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.SkuDetails
import com.anjlab.android.iab.v3.TransactionDetails
import com.industree.app.R
import com.industree.app.main.fcm.CustomNotiModel
import com.industree.app.main.utils.LICENCE_KEY
import com.industree.app.main.utils.SPLASH_TIME_OUT
import org.json.JSONObject
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException

class SplashActivity : BaseActivity() {
    @BindView(R.id.splashIV)
    lateinit var splash: ImageView
    var mNotificationModel: CustomNotiModel? = null
    var notificationId: String? = ""
    var roomId: String? = ""
    var notificationType: String? = ""
    var notificationReadStatus: String? = ""
    var userId: String? = ""
    var otherUserID: String? = ""
    var username: String? = ""
    var created: String? = ""
    var description: String? = ""
    var profileImage: String? = ""
    var title: String? = ""
    var detail_id: String? = ""
    private val SPLASH_DISPLAY_LENGTH = 3000


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        setUpSplash()
        getHashKey()

    }


    //   {"room_id":"WZnQxxq1XMMjAVyPiInBOMZ0eLd82Ge7QZVdQen56RUunGdMHa","notification_type":"1","notification_read_status":"0","user_id":"50","other_user_id":"51","user_name":"Manpreet  Kaur","created":"1650537496","description":"Manpreet  Kaur sent you message","notification_id":"83","title":"New Message","detail_id":"22"}
    private fun setUpSplash() {
        val mThread = object : Thread() {
            override fun run() {
                sleep(SPLASH_TIME_OUT.toLong())
                if (isUserLogin()) {
                    // Set Up Notification
                    if (isUserLogin()) {
                        var mIntent: Intent? = null
                        if (intent != null && intent.extras != null) {
                            for (key in intent.extras!!.keySet()) {
                                val value = intent.extras!!.getString(key)

                                Log.e(TAG, "Key:::: $key Value:::: $value")
                                var mJsonObject: JSONObject
                                if (key == "data") {
                                    mJsonObject = JSONObject(value)
                                    if (!mJsonObject.isNull("room_id")) {
                                        roomId = mJsonObject.getString("room_id")
                                    }
                                    if (!mJsonObject.isNull("notification_type")) {
                                        notificationType =
                                            mJsonObject.getString("notification_type")
                                    }
                                    if (!mJsonObject.isNull("notification_read_status")) {
                                        notificationReadStatus =
                                            mJsonObject.getString("notification_read_status")
                                    }
                                    if (!mJsonObject.isNull("user_id")) {
                                        userId = mJsonObject.getString("user_id")
                                    }
                                    if (!mJsonObject.isNull("other_user_id")) {
                                        otherUserID = mJsonObject.getString("other_user_id")
                                    }
                                    if (!mJsonObject.isNull("user_name")) {
                                        username = mJsonObject.getString("user_name")
                                    }
                                    if (!mJsonObject.isNull("created")) {
                                        created = mJsonObject.getString("created")
                                    }
                                    if (!mJsonObject.isNull("description")) {
                                        description = mJsonObject.getString("description")
                                    }

                                    if (!mJsonObject.isNull("profileImage")) {
                                        profileImage = mJsonObject.getString("profileImage")
                                    }

                                    if (!mJsonObject.isNull("notificationId")) {
                                        notificationId = mJsonObject.getString("notificationId")
                                    }
                                    if (!mJsonObject.isNull("title")) {
                                        title = mJsonObject.getString("title")
                                    }
                                    if (!mJsonObject.isNull("detail_id")) {
                                        detail_id = mJsonObject.getString("detail_id")
                                    }
                                }


//                        if (intent != null && intent.extras != null){
//                            for (key in intent.extras!!.keySet()) {
//                                val value = intent.extras!![key]
//                                Log.d(TAG, "Key: $key Value: $value")
//                                if(key == "data"){
//
//                                }
//                                if (key.equals("room_id", ignoreCase = true)) {
//                                    roomId = value.toString()
//                                }
//                                if (key.equals("notification_type", ignoreCase = true)) {
//                                    notificationType = value.toString()
//                                }
//                                if (key.equals("notification_read_status", ignoreCase = true)) {
//                                    notificationReadStatus = value.toString()
//                                }
//                                if (key.equals("user_id", ignoreCase = true)) {
//                                    userId = value.toString()
//                                }
//                                if (key.equals("other_user_id", ignoreCase = true)) {
//                                    otherUserID = value.toString()
//                                }
//                                if (key.equals("user_name", ignoreCase = true)) {
//                                    username = value.toString()
//                                }
//                                if (key.equals("created", ignoreCase = true)) {
//                                    created = value.toString()
////                                }
//                                if (key.equals("description", ignoreCase = true)) {
//                                    description = value.toString()
////                                }
//                                if (key.equals("notification_id", ignoreCase = true)) {
//                                    notificationId = value.toString()
//                                }
//                                if (key.equals("profileImage", ignoreCase = true)) {
//                                    profileImage = value.toString()
//                                }

//                                if (key.equals("title", ignoreCase = true)) {
//                                    title = value.toString()
//                                }

//                                if (key.equals("detail_id", ignoreCase = true)) {
//                                    detail_id = value.toString()
//                                }

                            }
                            mNotificationModel = CustomNotiModel(
                                roomId,
                                notificationType,
                                notificationReadStatus,
                                userId,
                                otherUserID,
                                username,
                                created,
                                description,
                                notificationId,
                                profileImage,
                                title,
                                detail_id
                            )
                            if (mNotificationModel?.notificationType.equals("1")) {
                                var mIntent: Intent = Intent(mActivity, ChatActivity::class.java)
                                mIntent.putExtra("room_id", mNotificationModel!!.roomId)
                                mIntent.putExtra("other_user_name", mNotificationModel!!.userName)
                                startActivity(mIntent)
                                finish()
                            } else if (mNotificationModel?.notificationType.equals("2")) {
                                var mIntent: Intent = Intent(mActivity, HomeActivity::class.java)
                                mIntent.putExtra("Notification_type", "1")
                                startActivity(mIntent)
                                finish()
                            } else if (mNotificationModel?.notificationType.equals("3")) {
                                var mIntent: Intent = Intent(mActivity, HomeActivity::class.java)
                                mIntent.putExtra("Notification_type", "1")
                                startActivity(mIntent)
                                finish()
                            }else if (mNotificationModel?.notificationType.equals("4")) {
                                var mIntent: Intent = Intent(mActivity, NotificationActivity::class.java)
                                startActivity(mIntent)
                                finish()
                            }else if (mNotificationModel?.notificationType.equals("5")) {
                                var mIntent: Intent = Intent(mActivity, NotificationActivity::class.java)
                                startActivity(mIntent)
                                finish()
                            }else if (mNotificationModel?.notificationType.equals("6")) {
                                var mIntent: Intent = Intent(mActivity, NotificationActivity::class.java)
                                startActivity(mIntent)
                                finish()
                            } else {
                                val i = Intent(mActivity, HomeActivity::class.java)
                                startActivity(i)
                                finish()
                            }
                        } else {
                            val i = Intent(mActivity, HomeActivity::class.java)
                            startActivity(i)
                            finish()
                        }
                    }
                    else {
                        val i = Intent(mActivity, SigninActivity::class.java)
                        startActivity(i)
                        finish()
                    }
                } else {
                    val i = Intent(mActivity, SigninActivity::class.java)
                    startActivity(i)
                    finish()
                }
            }
        }
        mThread.start()
    }

    // - - Code to print out the key hash
    open fun getHashKey() {
        try {
            val info = packageManager.getPackageInfo(
                "com.industree.app",
                PackageManager.GET_SIGNATURES
            )
            for (signature in info.signatures) {
                val md: MessageDigest = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
            }
        } catch (e: PackageManager.NameNotFoundException) {
        } catch (e: NoSuchAlgorithmException) {
        }
    }

}

