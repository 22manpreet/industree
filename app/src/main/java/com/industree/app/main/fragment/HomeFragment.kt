package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.industree.app.R
import com.industree.app.main.Model.*
import com.industree.app.main.`interface`.CategoryClickListener
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.PaginationInterface
import com.industree.app.main.activity.HomeActivity
import com.industree.app.main.activity.NotificationActivity
import com.industree.app.main.activity.SearchActivity
import com.industree.app.main.adapter.HomeHorizontalListAdapter
import com.industree.app.main.adapter.HomeVerticalListAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class HomeFragment : BaseFragment()  {
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.notificationIV)
    lateinit var notificationIV: ImageView

    @BindView(R.id.homeVerticalListRV)
    lateinit var homeVerticalListRV: RecyclerView

    @BindView(R.id.homeHorizontalListRV)
    lateinit var homeHorizontalListRV: RecyclerView

    @BindView(R.id.cancelTV)
    lateinit var cancelTV: TextView

    @BindView(R.id.searchLL)
    lateinit var searchLL: LinearLayout

    @BindView(R.id.homeRL)
    lateinit var homeRL: RelativeLayout

    @BindView(R.id.searchIV)
    lateinit var searchIV: ImageView

    @BindView(R.id.editEmailET)
    lateinit var editEmailET: TextView

    @BindView(R.id.badgeCountTV)
    lateinit var badgeCountTV: TextView
    var homeHorizontalListAdapter: HomeHorizontalListAdapter? = null
    var homeVerticalListAdapter: HomeVerticalListAdapter? = null
    var homeDetailItemList: ArrayList<DataItem?>? = ArrayList()
    var CategoryList: ArrayList<DataItemList?>? = ArrayList()

    lateinit var mHomeDataModel:HomeDataModel
    lateinit var mCategoryListingModel:CategoryListingModel

    var search_categoryId: String? ="1"

    // pagination
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var isLoading: Boolean = true

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_home, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        //  initRecyclerView(view)
        if (isNetworkAvailable(activity!!)) {
            executeHomeDataRequest(search_categoryId)
            executeBadgeNotificationDataRequest()
            executeCategoryListingDataRequest()
        } else {
            showToast(activity!!, getString(R.string.internet_connection_error))
        }
        return view
    }
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onResume() {
        super.onResume()

    }

    private fun executeBadgeNotificationDataRequest() {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        //   showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.notificationBadgeDataRequest(mHeaders,mMap)
        call.enqueue(object : Callback<NotificationBadgeCountModel> {
            override fun onFailure(call: Call<NotificationBadgeCountModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                //            dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<NotificationBadgeCountModel>,
                response: Response<NotificationBadgeCountModel>
            ) {
                Log.e(TAG, response.body().toString())
                //            dismissProgressDialog()
                var mNotificationBadgeCountModel = response.body()!!
                if (mNotificationBadgeCountModel.status == 1) {
                    //   if(mNotificationBadgeCountModel.chatCount!!.toInt()>0){
                    (activity as HomeActivity?)?.badgeChatCount(mNotificationBadgeCountModel.chatCount!!.toInt())
                    //   }
                    if(mNotificationBadgeCountModel.badgeCount!!.toInt() >0) {
                        badgeCountTV.visibility = View.VISIBLE
                    }
                    else{
                        badgeCountTV.visibility = View.GONE
                    }
                }else{
                    showToast(activity,mNotificationBadgeCountModel.message)
                }
            }
        })
    }

    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeCategoryListingDataRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        //   showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.categoryListingDataRequest(mHeaders,mParams())
        call.enqueue(object : Callback<CategoryListingModel> {
            override fun onFailure(call: Call<CategoryListingModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                //            dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CategoryListingModel>,
                response: Response<CategoryListingModel>
            ) {
                Log.e(TAG, response.body().toString())
                //            dismissProgressDialog()
                mCategoryListingModel = response.body()!!
                if (mCategoryListingModel.status == 1) {
                    CategoryList=mCategoryListingModel.data
                    setCatgoryListAdapter()
                }else if (mCategoryListingModel.status == 3){
                    showAccountDisableAlertDialog(activity, mCategoryListingModel.message!!)
                }else{
                    showToast(activity,mCategoryListingModel.message)
                }
            }
        })
    }

    @OnClick(
        R.id.notificationIV,R.id.cancelTV,R.id.searchIV,R.id.editEmailET
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.notificationIV -> performNotificationClick()
            R.id.cancelTV -> performCancelButtonClick()
            R.id.searchIV -> performSearchBtnClick()
            R.id.editEmailET -> performInClickSearchET()
        }
    }

    private fun performInClickSearchET() {
        val intent = Intent(activity, SearchActivity::class.java)
        intent.putExtra("category_id",search_categoryId)
        startActivity(intent)
    }

    private fun performSearchBtnClick() {
        searchLL.visibility=View.VISIBLE
        homeRL.visibility=View.GONE
    }

    private fun performCancelButtonClick() {
        searchLL.visibility=View.GONE
        homeRL.visibility=View.VISIBLE
    }
    private fun mParam(categoryId: String?): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["latitude"] = getLatitude()
        mMap["longitude"] = getLongitude()
        mMap["search"]=""
        mMap["category_id"]=categoryId
        mMap["pageNo"]=mPageNo.toString()
        mMap["perPage"]=mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeHomeDataRequest(categoryId: String?) {
        if (homeDetailItemList != null) {
            homeDetailItemList!!.clear()
        }
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.HomeDataRequest(mHeaders,mParam(categoryId))
        call.enqueue(object : Callback<HomeDataModel> {
            override fun onFailure(call: Call<HomeDataModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeDataModel>,
                response: Response<HomeDataModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mHomeDataModel = response.body()!!
                Log.e("Log", mHomeDataModel.status.toString())
                if (mHomeDataModel.status == 1) {

                    if (mHomeDataModel.lastPage!!.equals(true))
                    {
                        isLoading = false
                    } else {
                        isLoading = true
                    }

                    homeDetailItemList=mHomeDataModel.data
                    if(homeDetailItemList!!.size==0){
                        homeVerticalListRV.visibility=View.GONE
                        showAlertDialog(activity,getString(R.string.no_vendor_around_you))
                    }
                    else{
                        if (homeDetailItemList!!.size != 0) {
                            setAdapter(categoryId)
                            homeVerticalListRV.visibility = View.VISIBLE
                        }
                    }
                }else if (mHomeDataModel.status == 3){
                    showAccountDisableAlertDialog(activity, mHomeDataModel.message!!)
                }
                else if (mHomeDataModel.status == 0){
                    showAlertDialog(activity,getString(R.string.no_vendor_around_you))
                    homeVerticalListRV.visibility=View.GONE
                }
                else{
                    // showToast(activity,mHomeDataModel.message)
                    showAlertDialog(activity,getString(R.string.no_vendor_around_you))
                    homeVerticalListRV.visibility=View.GONE
                }
            }
        })
    }
    private fun setCatgoryListAdapter() {
        // Horizontal List
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        homeHorizontalListRV.setLayoutManager(layoutManager)
        homeHorizontalListAdapter = HomeHorizontalListAdapter(activity,CategoryList,mCategoryClickListener)
        homeHorizontalListRV.setAdapter(homeHorizontalListAdapter)
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter(categoryId: String?) {
        //    Vertical List
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        homeVerticalListRV.setLayoutManager(layoutManager)
        if(activity!=null) {
            homeVerticalListAdapter = HomeVerticalListAdapter(
                activity!!,
                homeDetailItemList,
                categoryId,
                getLoggedInUserID(),mPaginationInterface
            )
            homeVerticalListRV.setAdapter(homeVerticalListAdapter)
        }
    }

    var mPaginationInterface: PaginationInterface = object : PaginationInterface {
        override fun mHomePagination(mModel: DataItem) {
            if (isLoading) {
                ++mPageNo
                executeMoreHomeDataRequest(search_categoryId)
            }
        }
    }

    private fun mParams(categoryId: String?): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["latitude"] = getLatitude()
        mMap["longitude"] = getLongitude()
        mMap["search"]=""
        mMap["category_id"]=categoryId
        mMap["pageNo"]=mPageNo.toString()
        mMap["perPage"]=mPerPage.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeMoreHomeDataRequest(categoryId: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.HomeDataRequest(mHeaders,mParams(categoryId))
        call.enqueue(object : Callback<HomeDataModel> {
            override fun onFailure(call: Call<HomeDataModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<HomeDataModel>,
                response: Response<HomeDataModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mGetFavModel = response.body()!!
                Log.e("Log", mGetFavModel.status.toString())
                if (mGetFavModel.status == 1) {
                    homeDetailItemList!!.addAll(mGetFavModel.data!!)
//                    mGetFavModel.data.let {
//                        homeDetailItemList!!.addAll<DataItem>(
//                            it
//                        )
//                    }
                    homeVerticalListAdapter!!.notifyDataSetChanged()
                    isLoading = !mGetFavModel.lastPage!!.equals(true)

//                    homeDetailItemList=mHomeDataModel.data
//                    if(homeDetailItemList!!.size==0){
//                        homeVerticalListRV.visibility=View.GONE
//                        showAlertDialog(activity,getString(R.string.no_vendor_around_you))
//                    }
//                    else{
//                        if (homeDetailItemList!!.size != 0) {
//                            setAdapter(categoryId)
//                            homeVerticalListRV.visibility = View.VISIBLE
//                        }
//                    }
                }else if (mGetFavModel.status == 3){
                    showAccountDisableAlertDialog(activity, mHomeDataModel.message!!)
                }else{
                    // showToast(activity,mHomeDataModel.message)
                    showAlertDialog(activity,getString(R.string.no_vendor_around_you))
                    homeVerticalListRV.visibility=View.GONE
                }
            }
        })
    }


    var mCategoryClickListener : CategoryClickListener = object : CategoryClickListener {
        override fun onItemClickListener(mModel: DataItemList?) {
            if (isNetworkAvailable(activity!!)){
                search_categoryId=mModel!!.categoryId
                mPageNo=1
                println("mPageNo"+mPageNo)
                executeHomeDataRequest(search_categoryId)

            }else{
                showToast(activity,getString(R.string.internet_connection_error))
            }
        }

    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun performNotificationClick() {
        badgeCountTV.visibility = View.GONE
        val intent = Intent(activity, NotificationActivity::class.java)
        startActivity(intent)
    }

}
