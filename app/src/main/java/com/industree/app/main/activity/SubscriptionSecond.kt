package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.net.toUri
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.AddBusinessModel
import com.industree.app.main.Model.VendorCategoryItem
import com.industree.app.main.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream

class SubscriptionSecond : BaseActivity() {
    @BindView(R.id.subscriptionSubmitTV)
    lateinit var subscriptionSubmitTV: TextView

//    @BindView(R.id.vendorNameET)
//    lateinit var vendorNameET: EditText

    @BindView(R.id.emailSubscriptionET)
    lateinit var emailSubscriptionET: EditText

    @BindView(R.id.phoneNumberSubscriptionET)
    lateinit var phoneNumberSubscriptionET: EditText

    @BindView(R.id.descriptionSubscriptionET)
    lateinit var descriptionSubscriptionET: EditText

    @BindView(R.id.websiteSubscriptionET)
    lateinit var websiteSubscriptionET: EditText

    @BindView(R.id.instaSubscriptionET)
    lateinit var instaSubscriptionET: EditText

    @BindView(R.id.facebookSubscriptionET)
    lateinit var facebookSubscriptionET: EditText

    @BindView(R.id.twitterSubscriptionET)
    lateinit var twitterSubscriptionET: EditText

    @BindView(R.id.youtubeSubscriptionET)
    lateinit var youtubeSubscriptionET: EditText

    @BindView(R.id.subsciptionIV2)
    lateinit var subsciptionIV2: ImageView

    var category: ArrayList<VendorCategoryItem?>? = ArrayList()
//    var vendor_name:String?=null
//    var email:String?=null
//    var phone_no:String?=null
//    var description:String?=null
//    var website:String?=null
//    var instagram:String?=null
//    var facebook:String?=null
//    var twiter:String?=null
//    var youtube:String?=null
    var plan_id_var:String?=null
//    var vendor_id:String?=null
var vendor_name:String?=null
    var email:String?=null
    var phone_no:String?=null
    var description:String?=null
    var website:String?=null
    var instagram:String?=null
    var facebook:String?=null
    var twiter:String?=null
    var youtube:String?=null
    var plan_id:String?=null
    var vendor_id:String?=null
    @SuppressLint("WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_second2)
        ButterKnife.bind(mActivity)
        category=intent.getParcelableArrayListExtra("category")
        plan_id_var=intent.getStringExtra("plan_id")

    vendor_name=intent.getStringExtra("vendor_name")
    email= intent.getStringExtra("email")
    plan_id=intent.getStringExtra("plan_id")
    phone_no=intent.getStringExtra("phone_num")
    description=intent.getStringExtra("description")
    website=intent.getStringExtra("website")
    instagram=intent.getStringExtra("instagrem")
    facebook= intent.getStringExtra("facebook")
    twiter=intent.getStringExtra("twiter")
    youtube=intent.getStringExtra("youtube")
    vendor_id=intent.getStringExtra("vendor_id")

 //       vendorNameET.setText(vendor_name)
        emailSubscriptionET.setText(email)
        phoneNumberSubscriptionET.setText(phone_no)
        descriptionSubscriptionET.setText(description)
        if(website == null || website == ""){
            websiteSubscriptionET.setText("https://")
            websiteSubscriptionET.setSelection(websiteSubscriptionET.length())
        }
        else {
            websiteSubscriptionET.setText(website)
        }
        instaSubscriptionET.setText(instagram)
        facebookSubscriptionET.setText(facebook)
        twitterSubscriptionET.setText(twiter)
        youtubeSubscriptionET.setText(youtube)


    for (position in  0..category!!.size-1) {
        if (category?.get(position)?.brandImage!!.contains("http") || category?.get(position)?.brandImage.equals("")){

        }
        else
        {
            val uri = category!!.get(position)!!.brandImage!!.toUri()
            val mBitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, uri)
            val byteArrayOutputStream = ByteArrayOutputStream()
            mBitmap!!.compress(Bitmap.CompressFormat.PNG, 30, byteArrayOutputStream)
            var byteArray: ByteArray = byteArrayOutputStream.toByteArray()
            var encoded: String = Base64.encodeToString(byteArray, Base64.DEFAULT)
            category?.get(position)?.brandImage = encoded
        }
    }
    Log.e(TAG,"**Images Size**"+category?.get(0)!!.brandImage)

    }
    @OnClick(
        R.id.subscriptionSubmitTV,R.id.subsciptionIV2
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.subsciptionIV2 -> onBackPressed()
            R.id.subscriptionSubmitTV -> onSubmitClick()
        }
    }
    private fun onSubmitClick() {
        if (isNetworkAvailable(mActivity!!)) {
            executeSubmitDataRequest()
        } else {
            showToast(mActivity!!, getString(R.string.internet_connection_error))
        }
//        val mainIntent = Intent(mActivity, BookingHistoryActivity::class.java)
//        mainIntent.putExtra("abc", 123)
//        startActivity(mainIntent)
    }
    private fun mParam(): MutableMap<String?, Any?> {
        val mMap: MutableMap<String?, Any?> = java.util.HashMap()
        mMap["vendor_id"] = vendor_id
        mMap["user_id"] = getLoggedInUserID()
        mMap["plan_id"] = plan_id_var
        mMap["website"] = websiteSubscriptionET.text.toString()
        mMap["youtube"] = youtubeSubscriptionET.text.toString()
        mMap["twitter"] = twitterSubscriptionET.text.toString()
        mMap["instagram"] = instaSubscriptionET.text.toString()
        mMap["facebook"] = facebookSubscriptionET.text.toString()
        mMap["phone_no"] = phoneNumberSubscriptionET.text.toString()
        mMap["email"] = emailSubscriptionET.text.toString()
  //      mMap["vendor_name"]= vendorNameET.text.toString()
        mMap["description"]=descriptionSubscriptionET.text.toString()
        mMap["category"] = category
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSubmitDataRequest() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddBusinessDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<AddBusinessModel> {
            override fun onFailure(call: Call<AddBusinessModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<AddBusinessModel>, response: Response<AddBusinessModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mAddBusinessModel=  response.body()!!
                if (mAddBusinessModel!!.status == 1) {
                    showSubmitAlertDialog(mActivity,mAddBusinessModel.message)
                }
                else if(mAddBusinessModel!!.status == 0){
                    showAlertDialog(mActivity, mAddBusinessModel.message)
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    fun showSubmitAlertDialog(mActivity: Activity?, strMessage: String?) {

        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            val intent = Intent(mActivity, HomeActivity::class.java)
            startActivity(intent)
            finish()
        }
        alertDialog.show()
    }
}