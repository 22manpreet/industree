package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.anjlab.android.iab.v3.BillingProcessor
import com.anjlab.android.iab.v3.TransactionDetails
import com.industree.app.R
import com.industree.app.main.Model.SubscriptionModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class EditBusinessProfileActivity : BaseActivity() , BillingProcessor.IBillingHandler {
    @BindView(R.id.subscriptionCV1)
    lateinit var subscriptionCV1: CardView

    @BindView(R.id.subscriptionCV2)
    lateinit var subscriptionCV2: CardView

    @BindView(R.id.subscriptionCV3)
    lateinit var subscriptionCV3: CardView

    @BindView(R.id.category1RL)
    lateinit var category1RL: RelativeLayout

    @BindView(R.id.category2RL)
    lateinit var category2RL: RelativeLayout

    @BindView(R.id.category3RL)
    lateinit var category3RL: RelativeLayout

    @BindView(R.id.backRL)
    lateinit var backRL: RelativeLayout

    @BindView(R.id.termsConditionsTV)
    lateinit var termsConditionsTV: TextView

    @BindView(R.id.privacyPolicyTV)
    lateinit var privacyPolicyTV: TextView
    var plan_id:String?=null
    var mBillingProcessor: BillingProcessor? = null
    var subscription_price:String?=null
    var updated_plan_id:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_business_profile)
        ButterKnife.bind(mActivity)
        plan_id=intent.getStringExtra("plan_id")
        if(plan_id!!.equals("1")){
            category1RL.setBackgroundResource(R.drawable.sel_subscription_category_bg)
            category2RL.setBackgroundResource(R.drawable.subscription_category_bg)
            category3RL.setBackgroundResource(R.drawable.subscription_category_bg)
        }
        else if(plan_id!!.equals("2")){
            category2RL.setBackgroundResource(R.drawable.sel_subscription_category_bg)
            category1RL.setBackgroundResource(R.drawable.subscription_category_bg)
            category3RL.setBackgroundResource(R.drawable.subscription_category_bg)
        }
        else if(plan_id!!.equals("3")){
            category3RL.setBackgroundResource(R.drawable.sel_subscription_category_bg)
            category1RL.setBackgroundResource(R.drawable.subscription_category_bg)
            category2RL.setBackgroundResource(R.drawable.subscription_category_bg)
        }
        initializeSubscription()
    }
    @OnClick(
        R.id.subscriptionCV1,R.id.subscriptionCV2,R.id.subscriptionCV3,R.id.backRL,R.id.termsConditionsTV,R.id.privacyPolicyTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.subscriptionCV1 -> onFirstSubscriptionClick()
            R.id.subscriptionCV2 -> onSecondSubscriptionClick()
            R.id.subscriptionCV3 -> onThirdSubscriptionClick()
            R.id.termsConditionsTV -> onClickTermsOfUse()
            R.id.privacyPolicyTV -> onClickPrivacyPolicy()
            R.id.backRL -> onBackPressed()
        }
    }
    private fun onClickPrivacyPolicy() {
        val i = Intent(mActivity, PrivacyPolicy::class.java)
        startActivity(i)
    }

    private fun onClickTermsOfUse() {
        val i = Intent(mActivity, TermsOfUse::class.java)
        startActivity(i)
    }
    private fun onFirstSubscriptionClick() {
        plan_id ="1"
        subscription_price="14.99"

        if(IsSubscribed().equals("0") ) {
            //Actual
        //    if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                subscriptionCallBack(BRONZE_SUBSCRIPTION_PLAN_KEY)
                updated_plan_id="1"
//            } else {
//                showToast(mActivity, getString(R.string.subscription_update_not))
//            }
            //For Testing
//            AppPrefrences().writeBoolean(mActivity, IS_SUBSCRIPTION, true)
//            showToast(mActivity,getString(R.string.purchase_subscription_Successfully))
            //  finish()
        }
        else if(IsSubscribed().equals("1")){
            if(IsPlanId().equals("1")) {
                val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
                intent.putExtra("plan_id",IsPlanId())
                startActivity(intent)
            }else{
                //Actual
    //            if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                    subscriptionCallBack(BRONZE_SUBSCRIPTION_PLAN_KEY)
                    updated_plan_id="1"
//                } else {
//                    showToast(mActivity, getString(R.string.subscription_update_not))
//                }
            }
        }
        else if(IsSubscribed().equals("2")){
            if(IsPlanId().equals("1")) {
                val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
                intent.putExtra("plan_id",IsPlanId())
                startActivity(intent)
            }else{
                //Actual
   //             if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                    subscriptionCallBack(BRONZE_SUBSCRIPTION_PLAN_KEY)
                    updated_plan_id="1"
//                } else {
//                    showToast(mActivity, getString(R.string.subscription_update_not))
//                }
            }
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onSecondSubscriptionClick() {
        plan_id ="2"
        subscription_price="19.99"

        if(IsSubscribed().equals("0")) {
            //Actual
   //         if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                subscriptionCallBack(GOLD_SUBSCRIPTION_PLAN_KEY)
            updated_plan_id="2"
//            } else {
//                showToast(mActivity,getString(R.string.subscription_update_not))
//            }
        }
        else if(IsSubscribed().equals("1")){
            if(IsPlanId().equals("2")) {
                val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
                intent.putExtra("plan_id",IsPlanId())
                startActivity(intent)
            }
            else{
                //Actual
   //             if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                    subscriptionCallBack(GOLD_SUBSCRIPTION_PLAN_KEY)
                updated_plan_id="2"
//                } else {
//                    showToast(mActivity,getString(R.string.subscription_update_not))
//                }
            }
        }
        else if (IsSubscribed().equals("2")){
            if(IsPlanId().equals("2")) {
                val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
                intent.putExtra("plan_id",IsPlanId())
                startActivity(intent)
            }
            else{
                //Actual
    //            if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                    subscriptionCallBack(GOLD_SUBSCRIPTION_PLAN_KEY)
                updated_plan_id="2"
//                } else {
//                    showToast(mActivity,getString(R.string.subscription_update_not))
//                }
            }
        }
    }




    @SuppressLint("UseRequireInsteadOfGet")
    private fun onThirdSubscriptionClick() {
        plan_id ="3"
        subscription_price="24.99"
        if(IsSubscribed().equals("0")) {
        //Actual
   //     if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
            subscriptionCallBack(PLATINUM_SUBSCRIPTION_PLAN_KEY)
            updated_plan_id="3"
//        } else {
//            showToast(mActivity,getString(R.string.subscription_update_not))
//        }
//        val intent = Intent(activity, SubscriptionFirst::class.java)
//        intent.putExtra("category","3")
//        startActivity(intent)
            //  finish()
        }
        else if(IsSubscribed().equals("1")){
            if(  IsPlanId().equals("3")) {
                val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
                intent.putExtra("plan_id",IsPlanId())
                startActivity(intent)
            }
            else{
    //            if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                    subscriptionCallBack(PLATINUM_SUBSCRIPTION_PLAN_KEY)
                updated_plan_id="3"
//                } else {
//                    showToast(mActivity,getString(R.string.subscription_update_not))
//                }
            }
        }
        else if(IsSubscribed().equals("2")){
            if(  IsPlanId().equals("3")) {
                val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
                intent.putExtra("plan_id",IsPlanId())
                startActivity(intent)
            }
            else{
     //           if (mBillingProcessor!!.isSubscriptionUpdateSupported) {
                    subscriptionCallBack(PLATINUM_SUBSCRIPTION_PLAN_KEY)
                updated_plan_id="3"
//                } else {
//                    showToast(mActivity,getString(R.string.subscription_update_not))
//                }
            }
        }
    }
    /*
    * In App  Subscriptions
    * */
    open fun initializeSubscription() {
        mBillingProcessor = BillingProcessor(mActivity, LICENCE_KEY, this)
        mBillingProcessor!!.initialize()
    }
    private fun subscriptionCallBack(SUBSCRIPTION_PLAN_KEY: String) {
        // With Testing  PayLoads for Product Purchase & Subscription
        //mBillingProcessor.purchase(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        //mBillingProcessor.subscribe(getActivity(), "android.test.purchased", null /*or developer payload*/, extraParams);
        if (isNetworkAvailable(mActivity)) {
            val extraParams = Bundle()
            extraParams.putString("accountId", MERCHANT_ID)
            mBillingProcessor!!.subscribe(mActivity, SUBSCRIPTION_PLAN_KEY, null , extraParams)
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    override fun onProductPurchased(productId: String, mTransactionDetails: TransactionDetails?) {
        Log.e(TAG, "Purchase DATA :- " + mTransactionDetails!!.purchaseInfo.purchaseData)
        /*
         * Called when requested PRODUCT ID was successfully purchased
         */
        val mPurchaseData = mTransactionDetails!!.purchaseInfo.purchaseData
        var mSubscriptionPlanKey = mTransactionDetails!!.purchaseInfo.purchaseData.productId

        Log.e(TAG, "onProduct Purchased_Data: " + mTransactionDetails!!.purchaseInfo.purchaseData.toString())
        Log.e(TAG, "onProduct Purchased_OrderID: " + mPurchaseData!!.orderId)
        Log.e(TAG, "onProduct Purchased_Token: " + mPurchaseData!!.purchaseToken)
        Log.e(TAG, "onProduct Purchased_Time: " + mPurchaseData!!.purchaseTime)
        Log.e(TAG, "Subscriptions Plan Key: $mSubscriptionPlanKey")


//        if (mTransactionDetails!!.purchaseInfo == null) {
//            AppPrefrences().writeBoolean(mActivity, IS_SUBSCRIPTION, false)
//            showToast(mActivity,getString(R.string.some_things))
//        } else {
//            AppPrefrences().writeBoolean(mActivity, IS_SUBSCRIPTION, true)
//            showToast(mActivity,getString(R.string.purchase_subscription_Successfully))
//            if(isNetworkAvailable(mActivity!!)){
//                executeSubscriptionDataRequest(mPurchaseData!!.purchaseTime)
//            } else {
//                showToast(mActivity, getString(R.string.internet_connection_error))
//            }
//
//        }
        AppPrefrences().writeBoolean(mActivity, IS_SUBSCRIPTION, true)
        AppPrefrences().writeString(mActivity, PLANID,updated_plan_id)
        if(isNetworkAvailable(mActivity!!)){
            executeSubscriptionDataRequest(mPurchaseData.purchaseTime,mPurchaseData.productId,mPurchaseData!!.orderId)
        } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
    //    finish()
    }
    private fun mParam(dateString: String, productId: String, expire_time: String, orderId: String): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["plan_id"] = plan_id
        mMap["user_id"] = getLoggedInUserID()
        mMap["plan_price"] = subscription_price
        mMap["expire_date"] = ""
        mMap["plan_duration"] = "30"
        mMap["purchase_plan"] = "monthly"
        mMap["product_id"]= productId
        mMap["subscription_id"]= orderId
        mMap["type"]="2"
        mMap["expire_time"] = expire_time
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeSubscriptionDataRequest(
        purchaseTime: Date,
        productId: String,
        orderId: String
    ) {
        var date: Date? = purchaseTime
//        val formatter = SimpleDateFormat("dd/mm/yyyy HH:mm:ss zzzz")
//        formatter.setTimeZone(TimeZone.getTimeZone("GMT+05:30"));
//        val dateString :String =formatter.format(date)
        val dateString: String =""

        val expire_time:String=DateFormat.format("hh:mm:ss",date).toString()
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.getSubscriptionDataRequest(mHeaders,mParam(dateString,productId,expire_time,orderId))
        call.enqueue(object : Callback<SubscriptionModel> {
            override fun onFailure(call: Call<SubscriptionModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<SubscriptionModel>, response: Response<SubscriptionModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mSubscriptionModel=  response.body()!!
                if (mSubscriptionModel!!.status == 1) {
                    val intent = Intent(mActivity, SubscriptionFirstActivity::class.java)
                    intent.putExtra("plan_id",IsPlanId())
                    startActivity(intent)
                }
                else {
                    showAlertDialog(mActivity, mSubscriptionModel.message)
                }
            }
        })
    }
    override fun onPurchaseHistoryRestored() {

    }

    override fun onBillingError(errorCode: Int, error: Throwable?) {
        /*
        * Called when some error occurred. See Constants class for more details
        *
        * Note - this includes handling the case where the user canceled the buy dialog:
        * errorCode = Constants.BILLING_RESPONSE_RESULT_USER_CANCELED
        */
        Log.e(TAG, "onBillingError: ")
        when (errorCode) {
            0 -> Log.e("onBillingError", "> Success - BILLING_RESPONSE_RESULT_OK")
            1 -> Log.e(
                "onBillingError",
                "> User pressed back or canceled a dialog - BILLING_RESPONSE_RESULT_USER_CANCELED"
            )
            3 -> Log.e(
                "onBillingError",
                "> Billing API version is not supported for the type requested - BILLING_RESPONSE_RESULT_BILLING_UNAVAILABLE"
            )
            4 -> Log.e(
                "onBillingError",
                "> Requested product is not available for purchase - BILLING_RESPONSE_RESULT_ITEM_UNAVAILABLE"
            )
            5 -> Log.e(
                "onBillingError",
                "> Invalid arguments provided to the API. This error can also indicate that the application was not correctly signed or properly set up for In-app Billing in Google Play, or does not have the necessary permissions in its manifest - BILLING_RESPONSE_RESULT_DEVELOPER_ERROR"
            )
            6 -> Log.e(
                "onBillingError",
                "> Fatal error during the API action - BILLING_RESPONSE_RESULT_ERROR"
            )
            7 -> Log.e(
                "onBillingError",
                "> Failure to purchase since item is already owned - BILLING_RESPONSE_RESULT_ITEM_ALREADY_OWNED"
            )
            8 -> Log.e(
                "onBillingError",
                "> Failure to consume since item is not owned - BILLING_RESPONSE_RESULT_ITEM_NOT_OWNED"
            )
        }
    }

    override fun onBillingInitialized() {
        /*
        * Called when BillingProcessor was initialized and it's ready to purchase
        */
        Log.e(TAG, "onBillingInitialized: ");
    }

    override fun onDestroy() {
        if (mBillingProcessor != null) mBillingProcessor!!.release()
        super.onDestroy()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (!mBillingProcessor!!.handleActivityResult(requestCode, resultCode, data)) super.onActivityResult(
            requestCode,
            resultCode,
            data
        )
    }
}
