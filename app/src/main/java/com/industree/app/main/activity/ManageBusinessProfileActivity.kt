package com.industree.app.main.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.GetAllVendorByUserIdModel
import com.industree.app.main.Model.VendorCategoryItem
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.*
import com.industree.app.main.fragment.SubscriptionFragment
import com.industree.app.main.utils.SUBSCRIPTION_TAG
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ManageBusinessProfileActivity : BaseActivity() {

    @BindView(R.id.backIV)
    lateinit var backIV: ImageView

    @BindView(R.id.subsciptionPriceTV)
    lateinit var subsciptionPriceTV: TextView

    @BindView(R.id.subsciptionPlanTV)
    lateinit var subsciptionPlanTV: TextView

    @BindView(R.id.business_detailRV)
    lateinit var business_detailRV: RecyclerView

//    @BindView(R.id.vendorNameTV)
//    lateinit var vendorNameTV: TextView

    @BindView(R.id.vendorEmailTV)
    lateinit var vendorEmailTV: TextView

    @BindView(R.id.phoneNumberTV)
    lateinit var phoneNumberTV: TextView

    @BindView(R.id.websiteTV)
    lateinit var websiteTV: TextView

    @BindView(R.id.instagramTV)
    lateinit var instagramTV: TextView

    @BindView(R.id.facebookTV)
    lateinit var facebookTV: TextView

    @BindView(R.id.twiterTV)
    lateinit var twiterTV: TextView

    @BindView(R.id.youtubeTV)
    lateinit var youtubeTV: TextView

    var manageBussinessAdapter: ManageBussinessAdapter? = null
    var category: ArrayList<VendorCategoryItem?>? = ArrayList()
    var plan_id:String?=null

    var planID:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manage_business_profile)
         ButterKnife.bind(mActivity)
        planID=intent.getStringExtra("plan_id")
        if(planID.equals("1")){
            subsciptionPlanTV.setText("Bronze(1 Category)")
            subsciptionPriceTV.setText("$14.99")
        }
        else if(planID.equals("2")){
            subsciptionPlanTV.setText("Gold(2 Categories)")
            subsciptionPriceTV.setText("$19.99")
        }
        else if(planID.equals("3")){
            subsciptionPlanTV.setText("Platinum(3 Categories)")
            subsciptionPriceTV.setText("$24.99")
        }
    }

    override fun onResume() {
        super.onResume()
        if(isNetworkAvailable(mActivity)){
            executeGetAllVendorDataRequest()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeGetAllVendorDataRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.getAllVendorByUserIdDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<GetAllVendorByUserIdModel> {
            override fun onFailure(call: Call<GetAllVendorByUserIdModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<GetAllVendorByUserIdModel>, response: Response<GetAllVendorByUserIdModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mGetAllVendorByUserIdModel=  response.body()!!
                if (mGetAllVendorByUserIdModel!!.status == 1) {
                     plan_id=mGetAllVendorByUserIdModel.data!!.planId
                    if(plan_id.equals("1")){
                        subsciptionPlanTV.setText("Bronze(1 Category)")
                        subsciptionPriceTV.setText("$14.99")
                    }
                    else if(plan_id.equals("2")){
                        subsciptionPlanTV.setText("Gold(2 Categories)")
                        subsciptionPriceTV.setText("$19.99")
                    }
                    else if(plan_id.equals("3")){
                        subsciptionPlanTV.setText("Platinum(3 Categories)")
                        subsciptionPriceTV.setText("$24.99")
                    }
      //              vendorNameTV.text = mGetAllVendorByUserIdModel.data!!.vendorName
                    vendorEmailTV.text = mGetAllVendorByUserIdModel.data!!.email
                    phoneNumberTV.text = mGetAllVendorByUserIdModel.data!!.phoneNo
                    websiteTV.text = mGetAllVendorByUserIdModel.data!!.website
                    instagramTV.text = mGetAllVendorByUserIdModel.data!!.instagram
                    facebookTV.text = mGetAllVendorByUserIdModel.data!!.facebook
                    twiterTV.text = mGetAllVendorByUserIdModel.data!!.twitter
                    youtubeTV.text = mGetAllVendorByUserIdModel.data!!.youtube
                    category=mGetAllVendorByUserIdModel.category
                    setAdapter()
                }else if(mGetAllVendorByUserIdModel!!.status == 0){
             //       showAlertDialog(mActivity,getString(R.string.no_plans_purchased))
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }

            }
        })
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        business_detailRV.setLayoutManager(layoutManager)
        manageBussinessAdapter = ManageBussinessAdapter(mActivity!!, category)
        business_detailRV.setAdapter(manageBussinessAdapter)
    }

    @OnClick(
        R.id.backIV,R.id.editBusinessProfileIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backIV -> onBackPressed()
            R.id.editBusinessProfileIV -> onBusinessProfileClick()
        }
    }

    private fun onBusinessProfileClick() {
        val mainIntent = Intent(mActivity, EditBusinessProfileActivity::class.java)
        mainIntent.putExtra("plan_id",planID)
        startActivity(mainIntent)
    }
}

