package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.industree.app.R
import com.industree.app.main.Model.*
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.BookedDetailServicesAdapter
import com.industree.app.main.utils.PAYPAL_KEY
import com.paypal.checkout.PayPalCheckout
import com.paypal.checkout.approve.OnApprove
import com.paypal.checkout.config.CheckoutConfig
import com.paypal.checkout.config.Environment
import com.paypal.checkout.config.SettingsConfig
import com.paypal.checkout.createorder.CreateOrder
import com.paypal.checkout.createorder.CurrencyCode
import com.paypal.checkout.createorder.OrderIntent
import com.paypal.checkout.createorder.UserAction
import com.paypal.checkout.order.Amount
import com.paypal.checkout.order.AppContext
import com.paypal.checkout.order.Order
import com.paypal.checkout.order.PurchaseUnit
import com.paypal.checkout.paymentbutton.PayPalButton
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class ReviewOrderActivity : BaseActivity() {
    @BindView(R.id.backBookedServicesIV)
    lateinit var back: ImageView

    @BindView(R.id.nameBookedServicesTV)
    lateinit var nameBookedServicesTV: TextView


    @BindView(R.id.payPalButton)
    lateinit var payPalButton: PayPalButton

    @BindView(R.id.greyPayTV)
    lateinit var greyPayTV: TextView

    @BindView(R.id.addressBookedServicesTV)
    lateinit var addressBookedServicesTV: TextView

    @BindView(R.id.bookedServicesIV)
    lateinit var bookedServicesIV: ImageView

    @BindView(R.id.seviceDetailRV)
    lateinit var seviceDetailRV: RecyclerView

    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

    @BindView(R.id.bookedServicesSquareIV)
    lateinit var bookedServicesSquareIV: ImageView

    @BindView(R.id.bookedServicesCV)
    lateinit var bookedServicesCV: CardView

    @BindView(R.id.bookedServicesSquareCV)
    lateinit var bookedServicesSquareCV: CardView

    @BindView(R.id.distanceBookedServicesTV)
    lateinit var distanceBookedServicesTV: TextView

    @BindView(R.id.txtCashRL)
    lateinit var txtCashRL: RelativeLayout

    @BindView(R.id.txtPaypalRL)
    lateinit var txtPaypalRL: RelativeLayout

    @BindView(R.id.payLL)
    lateinit var payLL: LinearLayout

    @BindView(R.id.YellowPayTV)
    lateinit var YellowPayTV: TextView

    @BindView(R.id.cancelTV)
    lateinit var cancelTV: TextView


    var user_id:String?=null
    var booking_id:String?=null
    var type:String?=null
    var brand_name:String?=null
    var brand_image:String?=null
    var address:String?=null
    var email:String? = null
    var totalamount:String?=null
    var bookingServiceDetailList: ArrayList<BookingServiceDetail?>? = ArrayList()
    var bookedDetailServicesAdapter: BookedDetailServicesAdapter? = null
    private var paymentAmount: String? = null

    var payID: String? = null
    var orderID: String = ""
    var payerId: String = ""
    var mCurr: String = ""
    var value: String = ""
    var mAccesstoken: String = ""
    lateinit var currencyCode: CurrencyCode
     var dialogPrivacy: BottomSheetDialog?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_review_order)
        ButterKnife.bind(mActivity)
        getIntentData()
        if(isNetworkAvailable(mActivity)){
            executeBookingServiceDetail()
        }
        else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
        val config = CheckoutConfig(
            application = application,
            clientId = PAYPAL_KEY,
            environment = Environment.LIVE,
            returnUrl = "com.industree.app://paypalpay",
            currencyCode = CurrencyCode.USD,
            userAction = UserAction.PAY_NOW,
            settingsConfig = SettingsConfig(
                loggingEnabled = true
            )
        )
        PayPalCheckout.setConfig(config)
        getPayment()
    }
    private fun mParam(): MutableMap<String?, String?> {
            val mMap: MutableMap<String?, String?> = HashMap()
            mMap["user_id"] = user_id
            mMap["booking_id"] = booking_id
            mMap["type"]=type
            Log.e(TAG, "**PARAM**$mMap")
            return mMap
        }
        private fun executeBookingServiceDetail() {
            val mHeaders: MutableMap<String, String> = HashMap()
            mHeaders["Token"] = getAuthToken()
            showProgressDialog(mActivity)
            val call = RetrofitClient.apiInterface.BookingServiceDetailRequest(mHeaders,mParam())
            call.enqueue(object : Callback<BookingServiceDetailModel> {
                override fun onFailure(call: Call<BookingServiceDetailModel>, t: Throwable) {
                    Log.e(TAG, t.message.toString())
                    dismissProgressDialog()
                }
                @SuppressLint("NotifyDataSetChanged")
                override fun onResponse(
                    call: Call<BookingServiceDetailModel>,
                    response: Response<BookingServiceDetailModel>
                ) {
                    Log.e(TAG, response.body().toString())
                    dismissProgressDialog()
                    var mBookingServiceDetailModel = response.body()!!
                    if (mBookingServiceDetailModel.status == 1) {
                        nameBookedServicesTV.text=brand_name

                        if(mBookingServiceDetailModel.data!!.get(0)!!.brandImage.equals("") || mBookingServiceDetailModel.data!!.get(0)!!.brandImage.equals(null)){
                            bookedServicesCV.visibility=View.VISIBLE
                            bookedServicesSquareCV.visibility=View.GONE
                            Glide.with(mActivity!!)
                                .load(mBookingServiceDetailModel.data!!.get(0)!!.brandImage)
                                .centerCrop()
                                .placeholder(R.drawable.ic_placeholder)
                                .error(R.drawable.ic_placeholder)
                                .into(bookedServicesIV)
                        }
                        else {
                            bookedServicesCV.visibility=View.GONE
                            bookedServicesSquareCV.visibility=View.VISIBLE
                            Glide.with(mActivity!!)
                                .load(mBookingServiceDetailModel.data!!.get(0)!!.brandImage)
                                .centerCrop()
                                .placeholder(R.drawable.ic_placeholder)
                                .error(R.drawable.ic_placeholder)
                                .into(bookedServicesSquareIV)
                        }
                        bookingServiceDetailList=mBookingServiceDetailModel.data
                        txtPriceTV.text="$"+mBookingServiceDetailModel.totalAmount
                        paymentAmount=mBookingServiceDetailModel.totalAmount.toString()
//                        getPayment()
                        if(type.equals("1")){
                            addressBookedServicesTV.text=email
                            bookedServicesCV.visibility=View.VISIBLE
                            bookedServicesSquareCV.visibility=View.GONE
                            Glide.with(mActivity!!)
                                .load(R.drawable.ic_placeholder)
                                .centerCrop()
                                .placeholder(R.drawable.ic_placeholder)
                                .error(R.drawable.ic_placeholder)
                                .into(bookedServicesIV)
                        }
                        else if(type.equals("2")){
                            addressBookedServicesTV.text=address
                        }
                        if(mBookingServiceDetailModel.sessionEnd.equals("0")){
                            greyPayTV.visibility=View.VISIBLE
                            YellowPayTV.visibility=View.GONE
                            distanceBookedServicesTV.text="Booked"
//                            txtCashRL.visibility=View.GONE
//                            payPalButton.visibility=View.GONE
                        }
                        else if(mBookingServiceDetailModel.sessionEnd.equals("1")){
                            greyPayTV.visibility=View.GONE
                            YellowPayTV.visibility=View.VISIBLE
                            distanceBookedServicesTV.text="Payment Pending"
//                            txtCashRL.visibility=View.VISIBLE
//                            payPalButton.visibility=View.VISIBLE
                        }
                        else if(mBookingServiceDetailModel.sessionEnd.equals("2")){
                            greyPayTV.visibility=View.GONE
                            YellowPayTV.visibility=View.GONE
                            distanceBookedServicesTV.text="Paid"
//                            txtCashRL.visibility=View.VISIBLE
//                            payPalButton.visibility=View.VISIBLE
                        }
                        else if(mBookingServiceDetailModel.sessionEnd.equals("3")) {
                            greyPayTV.visibility=View.GONE
                            YellowPayTV.visibility=View.GONE
                            distanceBookedServicesTV.text = "Completed"
                        }
                        else{
                            greyPayTV.visibility=View.GONE
                            YellowPayTV.visibility=View.GONE
                            distanceBookedServicesTV.text="Completed"
                        }

                        // pay botton show/hide
                        if(mBookingServiceDetailModel.details!!.paypalEmail.equals("N/A")|| mBookingServiceDetailModel.details!!.paypalEmail.equals(""))
                        {
                         //   payPalButton.visibility=View.GONE
                            txtPaypalRL.visibility=View.GONE
                            txtCashRL.visibility=View.VISIBLE
                        }
                        else{
                          //  payPalButton.visibility=View.VISIBLE
                            txtPaypalRL.visibility=View.VISIBLE
                            txtCashRL.visibility=View.VISIBLE
                        }
                        setAdapter()
                    }else{
                        showToast(mActivity,mBookingServiceDetailModel.message)
                    }

                }
            })

        }


    private fun getIntentData() {
        user_id=intent.getStringExtra("user_id")
        booking_id=intent.getStringExtra("booking_id")
        type=intent.getStringExtra("type")
        brand_name=intent.getStringExtra("brand_name")
        brand_image=intent.getStringExtra("brand_image")
        address=intent.getStringExtra("address")
        email=intent.getStringExtra("email")
        totalamount=intent.getStringExtra("totalamount")
    }

    @OnClick(
        R.id.backBookedServicesIV,R.id.txtCashRL,R.id.YellowPayTV,R.id.cancelTV,R.id.txtPaypalRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backBookedServicesIV -> onBackPressed()
           R.id.txtCashRL -> onClickCashButton()
            R.id.paypalLL -> onPaypalPayButton()
            R.id.YellowPayTV -> onPayClick()
            R.id.cancelTV -> onCancelClick()
            R.id.txtPaypalRL -> onClickPayPal()
        }
    }

    private fun onClickPayPal() {
        onPaypalPayButton()
        payLL.visibility=View.GONE
    }

    private fun onCancelClick() {
        payLL.visibility=View.GONE
        YellowPayTV.isClickable=true
        YellowPayTV.isEnabled=true
    }

    private fun onPayClick() {
      payLL.visibility=View.VISIBLE
        greyPayTV.visibility=View.GONE
        YellowPayTV.visibility=View.VISIBLE
        YellowPayTV.isClickable=false
        YellowPayTV.isEnabled=false
    }

    private fun onClickCashButton() {
        if(isNetworkAvailable(mActivity)){
            executeCashPaymentDetail()
        }
        else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    private fun mParamCash(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["booking_id"] = booking_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeCashPaymentDetail() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.CashPaymentRequest(mHeaders,mParamCash())
        call.enqueue(object : Callback<CashPaymentModel> {
            override fun onFailure(call: Call<CashPaymentModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CashPaymentModel>,
                response: Response<CashPaymentModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mCashPaymentModel = response.body()!!
                if (mCashPaymentModel.status == 1) {
                    distanceBookedServicesTV.text="Paid"
                    YellowPayTV.visibility=View.GONE
                    payLL.visibility=View.GONE
                }else{
                    showToast(mActivity,mCashPaymentModel.message)
                }

            }
        })
    }

    private fun onPaypalPayButton() {
        payPalButton.performClick()
    }


    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        seviceDetailRV.setLayoutManager(layoutManager)
        bookedDetailServicesAdapter = BookedDetailServicesAdapter(mActivity,bookingServiceDetailList)
        seviceDetailRV.isNestedScrollingEnabled=false
        seviceDetailRV.setHasFixedSize(false)
        seviceDetailRV.setAdapter(bookedDetailServicesAdapter)
    }

//    private fun onClickPayButton() {
//          showPaymentBottomSheet()
//    }
//
//    private fun showPaymentBottomSheet() {
 //       val view: View = LayoutInflater.from(mActivity).inflate(R.layout.layout_payment_bottom_sheet, null)
//        dialogPrivacy = mActivity?.let { BottomSheetDialog(it, R.style.BottomSheetDialog) }
//        dialogPrivacy!!.setContentView(view)
//        dialogPrivacy!!.setCanceledOnTouchOutside(true)
//        //disabling the drag down of sheet
//        dialogPrivacy!!.setCancelable(true)
//        //cancel button click
//
//        val payPalButton: PayPalButton? = dialogPrivacy!!.findViewById(R.id.payPalButton)
//        val paypalLL: LinearLayout? =dialogPrivacy!!.findViewById(R.id.paypalLL)
////        val txtPaypalRL: RelativeLayout? = dialogPrivacy!!.findViewById(R.id.txtPaypalRL)
//        val txtCashRL: RelativeLayout? = dialogPrivacy!!.findViewById(R.id.txtCashRL)
//
//        paypalLL?.setOnClickListener {
//            getPayment()
////            payPalButton!!.performClick()
//        }
//        txtCashRL?.setOnClickListener {
//
//            dialogPrivacy!!.dismiss()
//        }
//
//        val cancelTV: TextView? = dialogPrivacy!!.findViewById(R.id.btnCancelTV)
//        cancelTV?.setOnClickListener {
//            dialogPrivacy!!.dismiss()
//        }
//        dialogPrivacy!!.show()
//    }

    private fun getPayment() {
//         Getting the amount from editText
        value = totalamount!!
//        if (mCurr == "$") {
//            currencyCode = CurrencyCode.USD
//        } else {
//            currencyCode = CurrencyCode.EUR
//        }
        payPalButton?.setup(
            createOrder = CreateOrder { createOrderActions ->
                val order = Order(
                    intent = OrderIntent.CAPTURE,
                    appContext = AppContext(
                        userAction = UserAction.PAY_NOW
                    ),
                    purchaseUnitList = listOf(
                        PurchaseUnit(
                            amount = Amount(
                                currencyCode = CurrencyCode.USD,
                                value = value

                            )
                        )
                    )

                )

                createOrderActions.create(order)
            },
            onApprove = OnApprove { approval ->
                approval.orderActions.capture { captureOrderResult ->
                    Log.i("CaptureOrder", "CaptureOrderResult: $captureOrderResult")
                    payID = approval.data.orderId

                    payID= approval.data.paymentId
                    payerId=approval.data.payerId
                    Log.e(
                        "IDDDD",
                        "iddd" + "orderID:  " + orderID + "payId:  " + payID + "payerid: " + payerId
                    )
                  getAccessToken()
                }

            }
        )
    }

    private fun getAccessToken() {
        if (isNetworkAvailable(this))
            executeGetAccessTokenRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }
    /*
        * Execute api param
        * */
    private fun mGetAccessTokenParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAccessTokenRequest() {
        showProgressDialog(this)
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.GetAccessTokenRequest(mHeaders,mGetAccessTokenParam())
        call.enqueue(object : Callback<GetAccessTokenModel> {
                override fun onFailure(call: Call<GetAccessTokenModel>, t: Throwable) {
                    dismissProgressDialog()
                }

                override fun onResponse(
                    call: Call<GetAccessTokenModel>,
                    response: Response<GetAccessTokenModel>
                ) {
                    dismissProgressDialog()
                    val mModel: GetAccessTokenModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status!!.equals(1)) {
                            mAccesstoken = mModel.accessToken!!
                            getTransDetails()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }

    private fun getTransDetails() {
        if (isNetworkAvailable(this))
            executeGetTransactionDetailsRequest()
        else
            showToast(this, getString(R.string.internet_connection_error))
    }
    /*
    * Execute api param
    * */
    private fun mGetTransParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["accessToken"] = mAccesstoken
        mMap["payID"] = payID
        mMap["booking_id"] = booking_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetTransactionDetailsRequest() {
       showProgressDialog(this)
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.GetPaypalTransactionDetailsRequest(mHeaders,mGetTransParam())
        call.enqueue(object : Callback<GetPaypalTransactionDetailsModel> {
            override fun onFailure(call: Call<GetPaypalTransactionDetailsModel>, t: Throwable) {
                dismissProgressDialog()
            }
                override fun onResponse(
                    call: Call<GetPaypalTransactionDetailsModel>,
                    response: Response<GetPaypalTransactionDetailsModel>
                ) {
                    dismissProgressDialog()
                    val mModel: GetPaypalTransactionDetailsModel? = response.body()
                    if (mModel != null) {
                        if (mModel.status!!.equals(1)) {
                 //           getPaymentStatusDetail()
                        } else {
                            showAlertDialog(mActivity, mModel.message)
                        }
                    }
                }
            })
    }
}