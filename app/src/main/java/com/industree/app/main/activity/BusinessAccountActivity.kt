package com.industree.app.main.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ImageView
import com.industree.app.R

class BusinessAccountActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_business_account)
        var back: ImageView = findViewById(R.id.backBusinessAccountIV)

        back.setOnClickListener {
            onBackPressed() }
    }
}