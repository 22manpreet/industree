package com.industree.app.main.adapter

import android.app.Activity
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.vendorDataItem
import com.industree.app.main.activity.BookingDetailsActivity
import java.util.ArrayList

class BusinessServAdapter(
    var mActivity: Activity,
    var bookedServiceItemList: ArrayList<vendorDataItem?>?,
    var loggedInUserID: String
) : RecyclerView.Adapter<BusinessServAdapter.ViewHolder>()  {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BusinessServAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.booking_services_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BusinessServAdapter.ViewHolder, position: Int) {
        var mModel=bookedServiceItemList!!.get(position)!!
        var name: ArrayList<String>? = ArrayList()
        if(mModel.services!!.size != 0) {
            var services = mModel.services
            for (i in 0..services!!.size-1) {
                name!!.add(services!!.get(i)!!.serviceName!!)
            }
        }
        val service_name: String = TextUtils.join(", ", name!!)
        holder.nameBookedServices.text= bookedServiceItemList!!.get(position)!!.firstName
        holder.addressBookedServices.text=service_name
        if(bookedServiceItemList!!.get(position)!!.totalAmount != null) { holder.textBookedServices.text = "$" + bookedServiceItemList!!.get(position)!!.totalAmount }
        else{ holder.textBookedServices.text = "$" }
        Glide.with(mActivity!!)
            .load(bookedServiceItemList?.get(position)?.profileImage)
            .centerCrop()
            .placeholder(R.drawable.review3)
            .error(R.drawable.review3)
            .into(holder.BookedServicesImage)

        holder.bookedServiceLL.setOnClickListener {
            val intent = Intent( mActivity, BookingDetailsActivity::class.java)
            intent.putExtra("user_id",loggedInUserID)
            intent.putExtra("booking_id",bookedServiceItemList!!.get(position)!!.bookingId)
            intent.putExtra("brand_name",bookedServiceItemList!!.get(position)!!.firstName+" "+bookedServiceItemList!!.get(position)!!.lastName)
            intent.putExtra("brand_image",bookedServiceItemList!!.get(position)!!.brandImage)
            intent.putExtra("address",bookedServiceItemList!!.get(position)!!.address)
            intent.putExtra("type","1")
            intent.putExtra("email",bookedServiceItemList!!.get(position)!!.email)
            mActivity!!.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return bookedServiceItemList!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var BookedServicesImage: ImageView
        var nameBookedServices: TextView
        var addressBookedServices: TextView
        var cardView: CardView
        var textBookedServices: TextView
        var bookedServiceLL: LinearLayout
        init {
            BookedServicesImage = itemView.findViewById(R.id.bookedServicesIV)
            nameBookedServices = itemView.findViewById(R.id.nameBookedServicesTV)
            addressBookedServices = itemView.findViewById(R.id.addressBookedServicesTV)
            textBookedServices = itemView.findViewById(R.id.textBookedServicesTV)
            cardView = itemView.findViewById(R.id.bookedServicesCV)
            bookedServiceLL=itemView.findViewById(R.id.bookedServiceLL)
        }
    }
}