package com.industree.app.main.fragment

import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.core.app.ActivityCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Unbinder
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.GetProfileDetailModel
import com.industree.app.main.Model.LogoutModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.activity.*
import com.industree.app.main.utils.AppPrefrences
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap


class ProfileFragment : BaseFragment() {
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.profileNameTV)
    lateinit var profileNameTV: TextView

    @BindView(R.id.profileEmailTV)
    lateinit var profileEmailTV: TextView

    @BindView(R.id.profileIV)
    lateinit var profileIV: CircleImageView

    @BindView(R.id.profileRL)
    lateinit var profileRL: RelativeLayout

    @BindView(R.id.bookedServicesRL)
    lateinit var bookedServicesRL: RelativeLayout

    @BindView(R.id.ManageBusinessProfile)
    lateinit var ManageBusinessProfile: RelativeLayout

    @BindView(R.id.accountInfoRL)
    lateinit var accountInfoRL: RelativeLayout

    @BindView(R.id.helpRL)
    lateinit var helpRL: RelativeLayout

    @BindView(R.id.paymentRL)
    lateinit var paymentRL: RelativeLayout

    @BindView(R.id.logoutRL)
    lateinit var logoutRL: RelativeLayout

    @BindView(R.id.changePwdRL)
    lateinit var changePwdRL: RelativeLayout

    @BindView(R.id.privacyPolicyRL)
    lateinit var privacyPolicyRL: RelativeLayout

    var mGetProfileDetailModel : GetProfileDetailModel? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_profile, container, false)
        mUnbinder = ButterKnife.bind(this, view)
    //    getProfileData()
        return view
    }
    override fun onResume() {
        super.onResume()
        getProfileData()
        executeProfileDetailDataRequest()
    }

    private fun executeProfileDetailDataRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.getProfileDetailDataRequest(mHeaders,mMap)
        call.enqueue(object : Callback<GetProfileDetailModel> {
            override fun onFailure(call: Call<GetProfileDetailModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<GetProfileDetailModel>, response: Response<GetProfileDetailModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                mGetProfileDetailModel  = response.body()!!
                if (mGetProfileDetailModel!!.status == 1) {

                }
                else {
                //    showAlertDialog(activity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    private fun getProfileData() {
        profileNameTV.setText(getFirstName()+" "+getLastName())
        profileEmailTV.setText(getUserEmail())
        Glide.with(requireActivity()).load(getUserProfilePic())
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(profileIV)
    }
    @OnClick(
        R.id.profileRL,
        R.id.bookedServicesRL,
        R.id.ManageBusinessProfile,
        R.id.accountInfoRL,
        R.id.helpRL,
        R.id.paymentRL,
        R.id.logoutRL,
        R.id.privacyPolicyRL,
        R.id.changePwdRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.profileRL -> performProfileClick()
            R.id.bookedServicesRL -> performBookedServiceClick()
            R.id.ManageBusinessProfile -> performManageBusinessClick()
            R.id.accountInfoRL -> performAccountInfoClick()
            R.id.helpRL -> performHelpClick()
            R.id.paymentRL -> performPaymentClick()
            R.id.logoutRL -> performLogoutClick()
            R.id.privacyPolicyRL -> performPrivacyClick()
            R.id.changePwdRL -> performChangePwdClick()
        }
    }

    private fun performChangePwdClick() {
        val intent = Intent(getActivity(), ChangePasswordActivity::class.java)
        getActivity()?.startActivity(intent)
    }


    private fun performLogoutClick() {
        showLogoutConfirmAlertDialog()
    }
    // - - Logout Alert Dialog
    private fun showLogoutConfirmAlertDialog() {
        val alertDialog = activity.let { Dialog(it!!) }
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_logout)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnYes = alertDialog.findViewById<TextView>(R.id.btnYes)
        val btnNo = alertDialog.findViewById<TextView>(R.id.btnNo)

        btnNo.setOnClickListener {
            alertDialog.dismiss()
        }
        btnYes.setOnClickListener {
            alertDialog.dismiss()
            executeLogoutRequest()
        }
        alertDialog.show()
    }
    private fun executeLogoutRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.logOutProfileRequest(mHeaders)
        call.enqueue(object : Callback<LogoutModel> {
            override fun onFailure(call: Call<LogoutModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<LogoutModel>, response: Response<LogoutModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mLogoutModel : LogoutModel = response.body()!!
                if (mLogoutModel!!.status == 1) {
                    showToast(activity,mLogoutModel!!.message)
                    val preferences: SharedPreferences = AppPrefrences().getPreferences(activity!!)
                    val editor = preferences.edit()
                    editor.clear()
                    editor.commit()
                    editor.apply()
                    val intent = Intent(activity, SigninActivity::class.java)
                    startActivity(intent)
                    activity?.let { ActivityCompat.finishAffinity(it) }
                }
                else {
                    showAlertDialog(activity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun performPrivacyClick() {
        val intent = Intent(getActivity(), PrivacyPolicy::class.java)
        getActivity()?.startActivity(intent)
    }

    private fun performPaymentClick() {
        if (mGetProfileDetailModel!!.data!!.paypalEmail.equals("N/A") || mGetProfileDetailModel!!.data!!.paypalID.equals("")){
            var intent = Intent(getActivity(), PaymentMethodActivity::class.java)
            getActivity()?.startActivity(intent)
        }
        else{
             showAlertDialog(activity,getString(R.string.you_already_connect_paypal_account))
        }
    }

    private fun performHelpClick() {
        val intent = Intent(getActivity(), HelpActivity::class.java)
        getActivity()?.startActivity(intent)
    }

    private fun performAccountInfoClick() {
        val intent = Intent(getActivity(), AccountInformationActivity::class.java)
        getActivity()?.startActivity(intent)
    }

    private fun performManageBusinessClick() {
        if(mGetProfileDetailModel!!.data!!.isSubscribed.equals("0")){
            showAlertDialog(activity,getString(R.string.no_plans_purchased))
        }
        else{
            val intent = Intent(getActivity(), ManageBusinessProfileActivity::class.java)
            intent.putExtra("plan_id",mGetProfileDetailModel!!.data!!.planId)
            getActivity()?.startActivity(intent)
        }
    }

    private fun performBookedServiceClick() {
        val intent = Intent(getActivity(), BookedServicesActivity::class.java)
        getActivity()?.startActivity(intent)
    }

    private fun performProfileClick() {
        val intent = Intent(getActivity(), EditProfileActivity::class.java)
        getActivity()?.startActivity(intent)
    }

}



