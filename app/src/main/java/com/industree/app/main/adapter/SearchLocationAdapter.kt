package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.industree.app.R
import com.industree.app.main.Model.PredictionsItem
import com.industree.app.main.`interface`.LocationtemClickListner

class SearchLocationAdapter(
   var  mActivity: Activity,
   var  mArrayList: ArrayList<PredictionsItem?>?,
   var  mLocationtemClickListner: LocationtemClickListner
): RecyclerView.Adapter<SearchLocationAdapter.ViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchLocationAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.search_location_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchLocationAdapter.ViewHolder, position: Int) {
        var mPredictionsItem : PredictionsItem = mArrayList!!.get(position)!!
        holder.nameTV.setText(mPredictionsItem.structuredFormatting?.mainText)
        holder.addressTV.setText(mPredictionsItem.structuredFormatting?.secondaryText)
        holder.itemView.setOnClickListener{
            mLocationtemClickListner.onItemClickListner(mPredictionsItem)
        }
    }

    override fun getItemCount(): Int {
        if (mArrayList != null && mArrayList!!.size > 0)
            return mArrayList!!.size
        else
            return 0
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView
        var addressTV: TextView


        init {
           nameTV=itemView.findViewById(R.id.nameTV)
            addressTV=itemView.findViewById(R.id.addressTV)

        }
    }
}