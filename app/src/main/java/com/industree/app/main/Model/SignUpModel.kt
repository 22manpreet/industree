package com.industree.app.main.Model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SignUpModel(
	val message: String? = null,
	val status: Int? = null
) : Parcelable
