package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.PaypalLoginModel
import com.industree.app.main.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class PaypalConnectActivity : BaseActivity() {

    @BindView(R.id.webView)
    lateinit var webView: WebView

    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_paypal_connect)
        ButterKnife.bind(mActivity)
        if (isNetworkAvailable(mActivity)) {
            executePayPalDataMethod()
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    @OnClick(
        R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executePayPalDataMethod() {
        showProgressDialog(mActivity)
      //  val user_id: RequestBody = getLoggedInUserID().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.payPalDataRequest(headers, mParam())

        call.enqueue(object : Callback<PaypalLoginModel> {
            override fun onFailure(call: Call<PaypalLoginModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<PaypalLoginModel>, response: Response<PaypalLoginModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var  mPaypalLoginModel = response.body()
                if (mPaypalLoginModel!!.status == 1) {
                    loadUrl(mPaypalLoginModel.loginUrl)
                    println("paypal : "+mPaypalLoginModel.loginUrl)
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun loadUrl(loginUrl: String?) {
        webView.webViewClient = WebViewClient()

        // this will load the url of the website
        webView.loadUrl(loginUrl!!)

        // this will enable the javascript settings
        webView.settings.javaScriptEnabled = true

        webView.settings.domStorageEnabled = true

        // if you want to enable zoom feature
        webView.settings.setSupportZoom(true)

    }
}