package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GetAllUserChatModel(

	@field:SerializedName("data")
	val data: ArrayList<ChatDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class ChatDataItem(

	@field:SerializedName("room_id")
	val roomId: String? = null,

	@field:SerializedName("messageCount")
	val messageCount: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("other_user_id")
	val otherUserId: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("lastMessage")
	val lastMessage: String? = null,

	@field:SerializedName("lastMessageTime")
	val lastMessageTime: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null
) : Parcelable
