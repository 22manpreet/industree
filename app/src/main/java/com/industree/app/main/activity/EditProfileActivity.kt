package com.industree.app.main.activity

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.github.dhaval2404.imagepicker.ImagePicker
import com.industree.app.R
import com.industree.app.main.Model.EditProfileModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.utils.*
import de.hdodenhof.circleimageview.CircleImageView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.IOException
import java.util.HashMap

class EditProfileActivity : BaseActivity() {
    @BindView(R.id.profileFirstNameET)
    lateinit var profileFirstNameET: EditText

    @BindView(R.id.profileLastNameET)
    lateinit var profileLastNameET: EditText

    @BindView(R.id.profileNumberTV)
    lateinit var profileNumberTV: EditText

    @BindView(R.id.profileEmailET)
    lateinit var profileEmailET: EditText

    @BindView(R.id.backRL)
    lateinit var backRL: ImageView

    @BindView(R.id.profileSaveTV)
    lateinit var profileSaveTV: TextView

    @BindView(R.id.editProfileIV)
    lateinit var editProfileIV: CircleImageView

    @BindView(R.id.editProfileCameraIV)
    lateinit var editProfileCameraIV: ImageView
    var mBitmap: Bitmap? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_profile)
        ButterKnife.bind(mActivity)
        getProfileData()
    }
    private fun getProfileData() {

        if (isNetworkAvailable(mActivity)) {
            profileFirstNameET.setText(getFirstName())
            profileLastNameET.setText(getLastName())
            profileEmailET.setText(getUserEmail())
            if(getUserEmail() == "" || getUserEmail() == null){
                profileEmailET.isClickable=true
                profileEmailET.isFocusable=true
            }
            else{
                profileEmailET.isClickable=false
                profileEmailET.isFocusable=false
            }
            profileNumberTV.setText(getPhoneNo())
            Glide.with(mActivity).load(getUserProfilePic())
                .placeholder(R.drawable.ic_placeholder)
                .error(R.drawable.ic_placeholder)
                .into(editProfileIV)

        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    @OnClick(
        R.id.backRL,
        R.id.profileSaveTV,R.id.editProfileCameraIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backRL -> onBackPressed()
            R.id.profileSaveTV -> performOnSaveClick()
            R.id.editProfileCameraIV -> performCameraClick()
        }
    }

    private fun performCameraClick() {
        ImagePicker.with(mActivity).galleryMimeTypes(arrayOf("image/*")).crop().maxResultSize(400, 400).start()
    }
    // Override this method too
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK && requestCode == ImagePicker.REQUEST_CODE){
            try {
                val imageStream = contentResolver.openInputStream(data?.data!!)
                val selectedImage = BitmapFactory.decodeStream(imageStream)
                Glide.with(mActivity).load(selectedImage)
                    .placeholder(R.drawable.ic_placeholder)
                    .error(R.drawable.ic_placeholder)
                    .into(editProfileIV)
                mBitmap = selectedImage
                editProfileIV.setImageBitmap(mBitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
    private fun performOnSaveClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeSubmitRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }
    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            profileFirstNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_first_name))
                flag = false
            }
            profileLastNameET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_last_name))
                flag = false
            }

            profileNumberTV.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_phone_no))
                flag = false
            }

            profileNumberTV.text.toString().trim { it <= ' ' }.length < 10 || profileNumberTV.text.toString().trim { it <= ' ' }.length > 10 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_phone_no))
                flag = false
            }
            profileEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_your_email))
                flag = false
            }
        }
        return flag
    }
    private fun executeSubmitRequest() {
        var mMultipartBody: MultipartBody.Part? = null
        if (mBitmap != null) {
            val requestFile1: RequestBody? = convertBitmapToByteArrayUncompressed(mBitmap!!)?.let {
                RequestBody.create(
                    "multipart/form-data".toMediaTypeOrNull(),
                    it
                )
            }
            mMultipartBody = requestFile1?.let {
                MultipartBody.Part.createFormData(
                    "profileImage",
                    getAlphaNumericString()!!.toString() + ".png",
                    it
                )
            }
        }
        val user_id: RequestBody = getLoggedInUserID().toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val first_name: RequestBody = profileFirstNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val last_name: RequestBody = profileLastNameET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val phone_no: RequestBody =  profileNumberTV.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())
        val email: RequestBody = profileEmailET.text.toString().trim()
            .toRequestBody("multipart/form-data".toMediaTypeOrNull())

        showProgressDialog(mActivity)
        val headers: MutableMap<String, String> = HashMap()
        headers["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.EditProfileDataRequest(
            headers,
            user_id,
            first_name,
            last_name,
            phone_no,
            email,
            mMultipartBody
        )

        call.enqueue(object : Callback<EditProfileModel> {
            override fun onFailure(call: Call<EditProfileModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(call: Call<EditProfileModel>, response: Response<EditProfileModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
               var  mEditProfileModel = response.body()
                if (mEditProfileModel!!.status == 1) {
//                    showEditProfileActivityAlertDialog(mActivity, mEditUserProfileModel!!.message)
                    AppPrefrences().writeString(mActivity, FIRSTNAME, mEditProfileModel!!.data!!.firstName)
                    AppPrefrences().writeString(mActivity, LASTNAME,  mEditProfileModel!!.data!!.lastName)
                    AppPrefrences().writeString(mActivity, EMAIL,  mEditProfileModel!!.data!!.email)
                    AppPrefrences().writeString(mActivity, PHONENO,  mEditProfileModel!!.data!!.phoneNo)
                    AppPrefrences().writeString(mActivity, PROFILEPIC, mEditProfileModel!!.data!!.profileImage)
                    showToast(mActivity, getString(R.string.profile_updated_successfully))
                    finish()
                } else if (mEditProfileModel!!.status == 0) {
                    showAlertDialog(mActivity, mEditProfileModel!!.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
}