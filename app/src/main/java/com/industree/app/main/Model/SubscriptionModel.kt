package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubscriptionModel(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class Data(

	@field:SerializedName("purchase_plan")
	val purchasePlan: String? = null,

	@field:SerializedName("isSubscribed")
	val isSubscribed: String? = null,

	@field:SerializedName("expire_date")
	val expireDate: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("plan_price")
	val planPrice: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null,

	@field:SerializedName("plan_duration")
	val planDuration: String? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null
) : Parcelable
