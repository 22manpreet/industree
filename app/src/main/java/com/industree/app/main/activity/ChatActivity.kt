package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.gson.Gson
import com.industree.app.R
import com.industree.app.main.Model.*
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.ChatActivityListAdapter
import com.industree.app.main.utils.IndustreeApplication
import de.hdodenhof.circleimageview.CircleImageView
import io.socket.client.Socket
import io.socket.emitter.Emitter
import kotlinx.android.synthetic.main.activity_chat.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ChatActivity : BaseActivity() {
    @BindView(R.id.chatActivityRV)
    lateinit var chatActivityRV: RecyclerView

    @BindView(R.id.backIV)
    lateinit var backIV: ImageView

    @BindView(R.id.editMessageET)
    lateinit var editMessageET: EditText

    @BindView(R.id.profileTectView)
    lateinit var profileTectView: TextView

    @BindView(R.id.sendCIV)
    lateinit var sendCIV: CircleImageView

    @BindView(R.id.avLoading)
    lateinit var avLoading: RelativeLayout

    var room_id: String?=null
    var chat_user_name:String?=null
    var chatActivityListAdapter: ChatActivityListAdapter? = null
    var mSocket: Socket? = null
    var mPageNo: Int = 1
    var mPerPage: Int = 10
    var mIsPagination = true
    var mMsgArrayList : ArrayList<ChatMessageData?>? = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        ButterKnife.bind(mActivity)
        getIntentData()
        setEditFocus()
        setTopSwipeRefresh()
    }
    private fun setTopSwipeRefresh() {
        mSwipeRefreshSR.setColorSchemeColors(resources.getColor(R.color.yellow),resources.getColor(R.color.black),resources.getColor(R.color.black));
        mSwipeRefreshSR.setOnRefreshListener {
            if (mIsPagination == true){
                ++mPageNo
                getAllMsgesData()
            }else{
                mSwipeRefreshSR.isRefreshing = false
            }
        }
    }

    private fun getIntentData() {
        if (intent != null){
            room_id=intent.getStringExtra("room_id")
            chat_user_name=intent.getStringExtra("other_user_name")
            setUpSocketData()
            setDataOnWidgets()
        }
    }

    @OnClick(
        R.id.backIV,R.id.sendCIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backIV -> onBackButtonClick()
            R.id.sendCIV -> onSendClick()
        }
    }

    private fun onBackButtonClick() {
        executeUpdateMessageSeen(room_id)
        onBackPressed()
    }

    private fun executeUpdateMessageSeen(roomId: String?) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["room_id"] = roomId
        Log.e(TAG, "**Param**$mMap")

        val call = RetrofitClient.apiInterface.UpdateMessageSeenRequest(mHeaders,mMap)
        call.enqueue(object : Callback<UpdateMessageSeenModel> {
            override fun onFailure(call: Call<UpdateMessageSeenModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }
            override fun onResponse(
                call: Call<UpdateMessageSeenModel>,
                response: Response<UpdateMessageSeenModel>
            ) {
                Log.e(TAG, response.body().toString())
                val mUpdateMessageSeenModel = response.body()
                if (mUpdateMessageSeenModel!!.status == 1) {
                }else{
                    showToast(mActivity,mUpdateMessageSeenModel?.message)
                }
            }
        })
    }

    private fun onSendClick() {
        if (editMessageET.text.toString().trim().length > 0) {
            executeSendMessageRequest()
            sendCIV.visibility=View.GONE
            avLoading.visibility=View.VISIBLE
        }
        else{
            sendCIV.visibility=View.VISIBLE
            avLoading.visibility=View.GONE
        }
    }
    private fun isValidate(): Boolean {
        var flag = true
        if (TextUtils.isEmpty(editMessageET.text.trim())) {
            flag = false
        }
        return flag
    }
    private fun executeSendMessageRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["message"] = editMessageET.text.toString().trim()
        mMap["room_id"] = room_id
        Log.e(TAG, "**Param**$mMap")

        val call = RetrofitClient.apiInterface.sendMessageRequest(mHeaders,mMap)
        call.enqueue(object : Callback<SendMessageModel> {
            override fun onFailure(call: Call<SendMessageModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
            }
            override fun onResponse(
                call: Call<SendMessageModel>,
                response: Response<SendMessageModel>
            ) {
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel!!.status == 1) {
                    mMsgArrayList!!.add(mModel.data)
                    chatActivityListAdapter!!.notifyDataSetChanged()
                    scrollToBottom()
                    // Perform the sending message attempt.
                    val gson = Gson()
                    val mOjectString = gson.toJson(mModel.data)
                    var mObject : JSONObject? = null
                    try {
                        mObject = JSONObject(mOjectString)
                    } catch (t: Throwable) {
                        Log.e(TAG,"**Error**"+t.toString())
                    }
                    Log.e(TAG,"*Msg*"+mOjectString)
                    mSocket!!.emit("newMessage", room_id,mObject)
                    editMessageET.setText("")
                    sendCIV.visibility=View.VISIBLE
                    avLoading.visibility=View.GONE
                }else{
                    showToast(mActivity,mModel?.message)
                }
            }
        })
    }
    private fun setDataOnWidgets() {

        profileTectView.setText(chat_user_name)

        //Set Adapter
        chatActivityListAdapter = ChatActivityListAdapter(mActivity, mMsgArrayList,getLoggedInUserID())
        chatActivityRV.setNestedScrollingEnabled(false)
        val mLayoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity)
        chatActivityRV.setLayoutManager(mLayoutManager)
        chatActivityRV.setItemAnimator(DefaultItemAnimator())
        chatActivityRV.setAdapter(chatActivityListAdapter)

        if (editMessageET.hasFocus()) {
            chatActivityRV.scrollToPosition(mMsgArrayList!!.size - 1)
        }

        chatActivityRV.scrollToPosition(mMsgArrayList!!.size - 1)
    }
    /*
    * Socket Chat Implementations:
    * */

    /*
    * Set Up Socket
    * */
    private fun setUpSocketData() {
        val app: IndustreeApplication = application as IndustreeApplication
        mSocket = app.getSocket()
        mSocket!!.emit("ConncetedChat", room_id)
        mSocket!!.on(Socket.EVENT_CONNECT, onConnect)
        mSocket!!.on(Socket.EVENT_DISCONNECT, onDisconnect)
        mSocket!!.on(Socket.EVENT_CONNECT_ERROR, onConnectError)
        mSocket!!.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError)
        mSocket!!.on("newMessage", onNewMessage)
        //mSocket.on("ChatStatus", onUserJoined);
        mSocket!!.on("leaveChat", onUserLeft)
        //mSocket.on("type", onTyping);
        //mSocket.on("stop typing", onStopTyping);
        mSocket!!.connect()

        getAllMsgesData()
    }
    private fun getAllMsgesData() {
        if (isNetworkAvailable(mActivity))
            executeGetAllMegesRequest()
        else
            showToast(mActivity, getString(R.string.internet_connection_error))
    }

    private fun mGetMsgesParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["room_id"] = room_id
        mMap["pageNo"] = "1"
        mMap["perPage"] = "10"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllMegesRequest() {
        mSwipeRefreshSR.isRefreshing = true
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        val call = RetrofitClient.apiInterface.getAllChatMsgesRequest(mHeaders,mGetMsgesParam())
        call.enqueue(object : Callback<GetAllChatMessageModel> {
            override fun onFailure(call: Call<GetAllChatMessageModel>, t: Throwable) {
                mSwipeRefreshSR.isRefreshing = false
                Log.e(TAG, t.message.toString())
            }
            override fun onResponse(
                call: Call<GetAllChatMessageModel>,
                response: Response<GetAllChatMessageModel>
            ) {
                mSwipeRefreshSR.isRefreshing = false
                Log.e(TAG, response.body().toString())
                val mModel = response.body()
                if (mModel!!.status == 1) {
                    mSwipeRefreshSR.isRefreshing = false
                    Log.e(TAG, response.body().toString())
                    val mModel = response.body()
                    if (mModel?.status!! == 1) {
                        if (mModel?.data!!.size < 10){
                            mIsPagination = false
                        }
                        mMsgArrayList!!.addAll(0, mModel.data!!)
                        mMsgArrayList!!.sortedWith(compareBy({ it!!.created}))
                        chatActivityListAdapter!!.notifyDataSetChanged()
                        if (mPageNo == 1){
                            chatActivityRV.scrollToPosition(mMsgArrayList!!.size - 1)
                        }
                    } else if (mModel?.status!! == 0) {
                        chatActivityListAdapter!!.notifyDataSetChanged()
                    }
                } else if (mModel!!.status == 0) {
                    chatActivityListAdapter!!.notifyDataSetChanged()
                } else {
                    println("data :  "+mModel!!.message)
                }
            }
        })
    }
    /*
* Set Edit Message Focus
* */
    private fun setEditFocus() {
        editMessageET.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {
                chatActivityRV.scrollToPosition(mMsgArrayList!!.size - 1)
            }

            override fun onTextChanged(
                charSequence: CharSequence,
                i: Int,
                i1: Int,
                i2: Int
            ) {

            }

            override fun afterTextChanged(editable: Editable) {}
        })
    }
    private fun scrollToUp() {
        //        chatListRV.scrollToPosition(mMsgArrayList!!.size - 1)
        chatActivityRV.scrollToPosition(0)

    }

    private fun scrollToBottom() {
        chatActivityRV.scrollToPosition(chatActivityListAdapter!!.getItemCount() - 1)

    }

    private val onConnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onDisconnect = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onConnectError = Emitter.Listener {
        runOnUiThread(Runnable { Log.e(TAG, "Error connecting") })
    }

    private val onNewMessage = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            try {
                val mGson = Gson()
                val mChatMessageData : ChatMessageData = mGson.fromJson(args.get(1).toString(), ChatMessageData::class.java)
                mMsgArrayList!!.add(mChatMessageData)
                chatActivityListAdapter!!.notifyDataSetChanged()
                editMessageET.setText("")
                scrollToBottom()
            } catch (e: Exception) {
                e.printStackTrace()
            }
        })
    }
    private val onUserJoined = Emitter.Listener { args ->
        runOnUiThread(Runnable {
            var mJsonData: JSONObject? = null
            try {
                mJsonData = JSONObject(args[1].toString())
                scrollToBottom()
                //   scrollToUp()
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        })
    }
    private val onUserLeft = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }

    private val onTyping =
        Emitter.Listener { args -> runOnUiThread(Runnable { val isTyping = args[1] as Boolean }) }
    private val onStopTyping = Emitter.Listener {
        runOnUiThread(
            Runnable { })
    }
    private val onTypingTimeout = Runnable { }

}