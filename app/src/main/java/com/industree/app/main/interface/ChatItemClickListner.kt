package com.industree.app.main.`interface`

import com.industree.app.main.Model.ChatDataItem

interface ChatItemClickListner {
    open fun onItemClickListner(mModel: ChatDataItem?)
}