package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.ReviewItem
import com.whinc.widget.ratingbar.RatingBar
import kotlinx.android.synthetic.main.review_list_item.view.*
import java.lang.NumberFormatException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class ReviewAdapter(
    var mActivity: Activity,
    var reviewArrayList: ArrayList<ReviewItem>
): RecyclerView.Adapter<ReviewAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.review_list_item, null)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReviewAdapter.MyViewHolder, position: Int) {
        if(reviewArrayList!!.get(position)!!.firstName.equals(null)){
            holder.nameTV.visibility=View.GONE
        }
        else{
            holder.nameTV.visibility=View.VISIBLE
        }
        holder.nameTV.text=reviewArrayList!!.get(position)!!.firstName
           holder.reviewTV.text=reviewArrayList!!.get(position)!!.review
        val today = Date()
        val timestamp: Long = reviewArrayList!!.get(position)!!.created!!.toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val review_date: Date?=sdf.parse(formattedDate)

        val diff: Long = today!!.time - review_date!!.time
        val days: Long = TimeUnit.MILLISECONDS.toDays(diff)
        if(days.toInt() == 0){
            holder.timesTV.setText("Today")
        }
        else if(days.toInt()<7){
            holder.timesTV.setText(""+days.toInt()+" days ago")
        }
        else{
            holder.timesTV.setText(""+ days.toInt()/7+" week ago")
        }
        Glide.with(mActivity!!)
            .load(reviewArrayList?.get(position)?.profileImage)
            .centerCrop()
            .placeholder(R.drawable.ic_review)
            .error(R.drawable.ic_review)
            .into(holder.reviewIV)
       try{
            holder.exp_rating.count = (reviewArrayList?.get(position)?.rating!!.toInt())
        }
       catch (e:NumberFormatException){

       }
    }

    override fun getItemCount(): Int {
       return reviewArrayList!!.size
    }
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView = itemView.nameTV
        var reviewTV: TextView = itemView.reviewTV
        var timesTV : TextView = itemView.timesTV
        var exp_rating : RatingBar = itemView.exp_rating
        var reviewIV:ImageView = itemView.reviewIV
    }

}