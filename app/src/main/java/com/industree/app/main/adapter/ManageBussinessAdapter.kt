package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.VendorCategoryItem
import com.industree.app.main.Model.vendorServicesItem

class ManageBussinessAdapter(var mActivity: Activity, var category: ArrayList<VendorCategoryItem?>?) : RecyclerView.Adapter<ManageBussinessAdapter.ViewHolder>() {
    var services: ArrayList<vendorServicesItem?>? = ArrayList()
    var servicesAdapter: ServicesAdapter? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ManageBussinessAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.manage_business_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ManageBussinessAdapter.ViewHolder, position: Int) {
        holder.categoryET.setText(category!!.get(position)!!.categoryName)
        holder.brandNameET.setText(category!!.get(position)!!.brandName)
        holder.locationET.setText(category!!.get(position)!!.location)
        Glide.with(mActivity!!)
            .load(category?.get(position)?.brandImage)
            .centerCrop()
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .into(holder.ManageBusinessProfileIV)
        services = category!!.get(position)!!.services

        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
        holder.servicesRV.setLayoutManager(layoutManager)
        servicesAdapter = ServicesAdapter(mActivity!!, services)
        holder.servicesRV.setAdapter(servicesAdapter)
    }


    override fun getItemCount(): Int {
        return category!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
       var ManageBusinessProfileIV: ImageView
       var categoryET : TextView
       var brandNameET : TextView
       var locationET : TextView
       var servicesRV : RecyclerView
        init {
            ManageBusinessProfileIV = itemView.findViewById(R.id.ManageBusinessProfileIV)
            categoryET = itemView.findViewById(R.id.categoryET)
            brandNameET = itemView.findViewById(R.id.brandNameET)
            locationET = itemView.findViewById(R.id.locationET)
            servicesRV = itemView.findViewById(R.id.servicesRV)
        }
    }

}