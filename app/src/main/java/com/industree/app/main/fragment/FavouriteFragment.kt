package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.industree.app.R
import com.industree.app.main.Model.FavouriteDataItem
import com.industree.app.main.Model.FavouriteUnfavouriteModel
import com.industree.app.main.Model.GetAllfavouriteVendorModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.FavouriteClickListener
import com.industree.app.main.adapter.FavVerticalListAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class FavouriteFragment : BaseFragment() {
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.favVerticalListRV)
    lateinit var favVerticalListRV: RecyclerView

    var favouriteDetailItemList: ArrayList<FavouriteDataItem?>? = ArrayList()
    var favVerticalListAdapter: FavVerticalListAdapter? = null

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_favourite, container, false)
        mUnbinder = ButterKnife.bind(this, view)

        return view
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onResume() {
        super.onResume()
        if (isNetworkAvailable(activity!!)) {
            executeFavouriteDataRequest()
        } else {
            showToast(activity!!, getString(R.string.internet_connection_error))
        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["pageNo"] = "1"
        mMap["perPage"] = "10"
        return mMap
    }
    private fun executeFavouriteDataRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.FavouriteDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<GetAllfavouriteVendorModel> {
            override fun onFailure(call: Call<GetAllfavouriteVendorModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<GetAllfavouriteVendorModel>,
                response: Response<GetAllfavouriteVendorModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mGetAllfavouriteVendorModel = response.body()!!

                if (mGetAllfavouriteVendorModel.status == 1) {

                    favouriteDetailItemList=mGetAllfavouriteVendorModel.data
                    if(favouriteDetailItemList!!.size==0){
                        favVerticalListRV.visibility=View.GONE
                        showAlertDialog(activity,getString(R.string.you_have_not_saved_any_vendor))
                    }
                    else{
                        setAdapter()
                        favVerticalListRV.visibility=View.VISIBLE
                    }
                }else if (mGetAllfavouriteVendorModel.status == 3){
                    showAccountDisableAlertDialog(activity, mGetAllfavouriteVendorModel.message!!)
                }else{
                    showAlertDialog(activity,getString(R.string.no_favourite_vendor))
                    favVerticalListRV.visibility=View.GONE
                }
            }
        })
    }

    private fun setAdapter() {
        val layoutManager2: RecyclerView.LayoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        favVerticalListRV.setLayoutManager(layoutManager2)
        favVerticalListAdapter = FavVerticalListAdapter(activity,favouriteDetailItemList,getLoggedInUserID(),mFavouriteClickListener)
        favVerticalListRV.setAdapter(favVerticalListAdapter)

    }
    var mFavouriteClickListener : FavouriteClickListener = object : FavouriteClickListener {
        override fun onItemClickListener(
            vendorId: String?,
            categoryId: String?,
            position: Int,
            brandName: String?
        ) {
            if (isNetworkAvailable(activity!!)){
                executeFavUnfavExecute(vendorId,categoryId,position,brandName)
            }else{
                showToast(activity,getString(R.string.internet_connection_error))
            }
        }

    }
    private fun mParameters(vendorId: String?, categoryId: String?): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["vendor_id"] = vendorId
        mMap["category_id"] = categoryId
        return mMap
    }
    private fun executeFavUnfavExecute(
        vendorId: String?,
        categoryId: String?,
        position: Int,
        brandName: String?
    ) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.FavouriteUnfavouriteRequest(mHeaders,mParameters(vendorId,categoryId))
        call.enqueue(object : Callback<FavouriteUnfavouriteModel> {
            override fun onFailure(call: Call<FavouriteUnfavouriteModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(call: Call<FavouriteUnfavouriteModel>, response: Response<FavouriteUnfavouriteModel>) {
                dismissProgressDialog()
                val mFavouriteUnfavouriteModel = response.body()
                if (mFavouriteUnfavouriteModel!!.status == 1) {
                    if ((mFavouriteUnfavouriteModel?.isFav).equals("0")) {
                        favouriteDetailItemList!!.get(position)!!.isFav ="0"
                        showFavouriteAlertDialog(activity,brandName + " removed from favorites",position)

                    } else if ((mFavouriteUnfavouriteModel?.isFav).equals("1")){

                    }
                } else if (mFavouriteUnfavouriteModel.status == 0) {
                    showAlertDialog(activity, mFavouriteUnfavouriteModel.message)
                } else {
                    showAlertDialog(activity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    // - - To Show favourite Alert Dialog
    @SuppressLint("NotifyDataSetChanged")
    fun showFavouriteAlertDialog(mActivity: Activity?, strMessage: String?, position: Int) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            favouriteDetailItemList!!.removeAt(position)
            favVerticalListAdapter?.notifyDataSetChanged()
            if(favouriteDetailItemList!!.size==0){
                showAlertDialog(activity,getString(R.string.you_have_not_saved_any_vendor))
            }
        }
        alertDialog.show()
    }
}
