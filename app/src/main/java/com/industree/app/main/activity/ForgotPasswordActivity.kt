package com.industree.app.main.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.ForgetPasswordModel
import com.industree.app.main.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Response
import java.util.HashMap

class ForgotPasswordActivity : BaseActivity() {
    @BindView(R.id.txtSubmitTV)
    lateinit var txtSubmitTV: TextView

    @BindView(R.id.forgotPasswordEmail)
    lateinit var editEmailET: EditText

    @BindView(R.id.backRL)
    lateinit var backRL: RelativeLayout
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
       ButterKnife.bind(mActivity)
    }
    @OnClick(
        R.id.txtSubmitTV,R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtSubmitTV -> onSubmitClick()
            R.id.backRL -> onBackPressed()
        }
    }

    private fun onSubmitClick() {
        if (isValidate()) {
            ExecuteForgotRequest()
//            startActivity(Intent(mActivity,SigninActivity::class.java))
//            finish()
        }

    }
    private fun isValidate(): Boolean {
        var flag=true
        when {
            editEmailET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity,getString(R.string.please_enter_your_email))
                flag=false
            }
            !isValidEmaillId(editEmailET.text.toString().trim { it <= ' ' }) -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_valid_email_address))
                flag = false
            }
        }
        return flag
    }
    private fun mParam(): MutableMap<String?, String?>? {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["email"]=editEmailET.text.toString().trim({it <= ' '})
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun ExecuteForgotRequest() {
        showProgressDialog(mActivity)
        val call= RetrofitClient.apiInterface.ForgetPasswordDataRequest(mParam())
        call.enqueue(object : retrofit2.Callback<ForgetPasswordModel> {
            override fun onResponse(
                call: Call<ForgetPasswordModel>,
                response: Response<ForgetPasswordModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mForgotPwdModel = response.body()
                if (mForgotPwdModel!!.status == 1) {
                    showFinishOpenPreviousActivityAlertDialog(mActivity, mForgotPwdModel.message)
                    editEmailET.setText("")
                } else if (mForgotPwdModel.status == 0) {
                    showAlertDialog(mActivity, mForgotPwdModel.message)
                } else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }

            override fun onFailure(call: Call<ForgetPasswordModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

        })
    }


}