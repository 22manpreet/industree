package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.ReviewDataItem
import com.whinc.widget.ratingbar.RatingBar
import kotlinx.android.synthetic.main.review_list_item.view.*
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

class ReviewListAdapter(var mActivity: Activity,var reviewList: ArrayList<ReviewDataItem?>?): RecyclerView.Adapter<ReviewListAdapter.MyViewHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ReviewListAdapter.MyViewHolder {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.review_list_item, null)
        return ReviewListAdapter.MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: ReviewListAdapter.MyViewHolder, position: Int) {
        if(reviewList!!.get(position)!!.firstName.equals(null)){
            holder.nameTV.visibility=View.GONE
        }
        else{
            holder.nameTV.visibility=View.VISIBLE
        }
        holder.nameTV.text=reviewList!!.get(position)!!.firstName
        holder.reviewTV.text=reviewList!!.get(position)!!.review
        val today = Date()
        val timestamp: Long = reviewList!!.get(position)!!.created!!.toLong()
        // convert seconds to milliseconds
        val date = Date(timestamp * 1000)
        val sdf = SimpleDateFormat("yyyy-MM-dd")
        sdf.timeZone = TimeZone.getTimeZone("GMT +05:30")
        val formattedDate = sdf.format(date)

        val review_date: Date?=sdf.parse(formattedDate)

        val diff: Long = today!!.time - review_date!!.time
        val days: Long = TimeUnit.MILLISECONDS.toDays(diff)
        if(days.toInt() == 0){
            holder.timesTV.setText("Today")
        }
        else if(days.toInt()<7){
            holder.timesTV.setText(""+days+" days ago")
        }
        else if(days.toInt()>=7){
            holder.timesTV.setText(""+ days.toInt()/7+" week ago")
        }
        Glide.with(mActivity!!)
            .load(reviewList?.get(position)?.profileImage)
            .centerCrop()
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(holder.reviewIV)

//        println(reviewList?.get(position)?.rating!!.toInt())
        if(reviewList?.get(position)?.rating !="") {
            holder.exp_rating.count = reviewList?.get(position)?.rating!!.toInt()
        }
    }

    override fun getItemCount(): Int {
       return reviewList!!.size
    }
    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var nameTV: TextView =itemView.nameTV
        var reviewTV: TextView =itemView.reviewTV
        var timesTV : TextView =itemView.timesTV
        var exp_rating : RatingBar =itemView.exp_rating
        var reviewIV: ImageView =itemView.reviewIV
    }

}