package com.industree.app.main.activity

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.BookingListingDataItem
import com.industree.app.main.Model.BookingListingModel
import com.industree.app.main.Model.BookingServicesDeatilsItem
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.BookedServicesAdapter
import com.industree.app.main.fragment.*
import com.industree.app.main.utils.*
import kotlinx.android.synthetic.main.activity_home.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class BookedServicesActivity : BaseActivity() {
    @BindView(R.id.backBookedServicesIV)
    lateinit var back: ImageView

    @BindView(R.id.allBookingTV)
    lateinit var allBookingTV: TextView

    @BindView(R.id.ongoingBookingTV)
    lateinit var ongoingBookingTV: TextView

    @BindView(R.id.completedBookingTV)
    lateinit var completedBookingTV: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booked_services)
        ButterKnife.bind(mActivity)
        setUpFirstFragment()

    }

    private fun setUpFirstFragment() {
        onAllBookingServiceClick()
    }




    @OnClick(
        R.id.backBookedServicesIV,R.id.allBookingTV,R.id.ongoingBookingTV,R.id.completedBookingTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backBookedServicesIV -> onBackPressed()
            R.id.allBookingTV -> onAllBookingServiceClick()
            R.id.ongoingBookingTV -> onGoingBookingServiceClick()
            R.id.completedBookingTV -> onCompleteBookingServiceClick()
        }
    }

    private fun onCompleteBookingServiceClick() {
        tabSelection(2)
        switchBookingFragment(CompleteBookingFragment(), COMPLETE_BOOKING_TAG, false, null)
    }

    private fun onGoingBookingServiceClick() {
        tabSelection(1)
        switchBookingFragment(OnGoingBookingFragment(), ON_GOING_BOOKING_TAG, false, null)
    }

    private fun onAllBookingServiceClick() {
        tabSelection(0)
        switchBookingFragment(AllBookedServicesFragment(), ALL_BOOKING_TAG, false, null)
    }
    private fun tabSelection(mPos: Int) {
        when(mPos){
            0 ->{
                allBookingTV.setBackgroundResource(R.drawable.bg_tab_review)
                ongoingBookingTV.setBackgroundColor(Color.TRANSPARENT)
                completedBookingTV.setBackgroundColor(Color.TRANSPARENT)
            }
            1 ->{
                ongoingBookingTV.setBackgroundResource(R.drawable.bg_tab_review)
                completedBookingTV.setBackgroundColor(Color.TRANSPARENT)
                allBookingTV.setBackgroundColor(Color.TRANSPARENT)
            }
            2 ->{
                completedBookingTV.setBackgroundResource(R.drawable.bg_tab_review)
                ongoingBookingTV.setBackgroundColor(Color.TRANSPARENT)
                allBookingTV.setBackgroundColor(Color.TRANSPARENT)
            }
        }
    }
}