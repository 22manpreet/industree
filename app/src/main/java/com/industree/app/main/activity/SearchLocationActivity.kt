package com.industree.app.main.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.PlacesLatLongModel
import com.industree.app.main.Model.PlacesModel
import com.industree.app.main.Model.PredictionsItem
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.LocationtemClickListner
import com.industree.app.main.adapter.SearchLocationAdapter
import com.industree.app.main.utils.*
import kotlinx.android.synthetic.main.activity_search_location.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchLocationActivity : BaseActivity() {
    val mLocationArrayList: ArrayList<PredictionsItem?>? = ArrayList()
    var mAdapter : SearchLocationAdapter? = null
    var mGettingFrom = ""

    @BindView(R.id.imgCancelIV)
    lateinit var imgCancelIV: ImageView

    @BindView(R.id.cancelTV)
    lateinit var cancelTV: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_location)
        ButterKnife.bind(this)
   //     statusBarColor()
   //     setUpToolBar()
        getIntentData()
        setUpSearch()
    }
    private fun getIntentData() {
        if (intent != null){
            mGettingFrom = intent.getStringExtra(LOCATION_ROLE)!!
        }
    }

//    private fun setUpToolBar() {
//        txtHeadingTV.setText(getString(R.string.select_location))
//    }

    private fun setUpSearch() {
        editSearchET.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length > 0){
                    imgCancelIV.visibility = View.VISIBLE
                }else{
                    imgCancelIV.visibility = View.GONE
                }
            }
        })


        editSearchET.setOnEditorActionListener(TextView.OnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                searchLocation()
                return@OnEditorActionListener true
            }
            false
        })
    }

    private fun searchLocation() {
        if (isNetworkAvailable(mActivity)){
            executePlacesRequest()
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    @OnClick(
      ///  R.id.imgBackIV,
        R.id.imgCancelIV,R.id.cancelTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
          //  R.id.imgBackIV -> onBackPressed()
            R.id.imgCancelIV -> performCloseClick()
            R.id.cancelTV -> finish()
        }
    }

    private fun performCloseClick() {
        editSearchET.setText("")
        imgCancelIV.visibility = View.GONE
        mLocationArrayList?.clear()
        mAdapter?.notifyDataSetChanged()
        noDataFountLL.visibility = View.VISIBLE
    }

    private fun executePlacesRequest() {
        showProgressDialog(mActivity)
        val mSearhText = editSearchET.text.toString().trim { it <= ' ' }
        val mApiUrl: String = GOOGLE_PLACES_SEARCH + "input=" + mSearhText + "&fields=name,address_component,place_id&key=" + PLACES_API_KEY
        println("api url "+mApiUrl)
        val mApiInterface: Call<PlacesModel>? = RetrofitClient.apiInterface.searchPlacesRequest(mApiUrl)
        mApiInterface?.enqueue(object : Callback<PlacesModel> {
            override fun onResponse(call: Call<PlacesModel>, response: Response<PlacesModel>) {
                dismissProgressDialog()
                var mPlacesModel = response.body()
                mLocationArrayList?.clear()
                if (mPlacesModel?.status == "OK"){
                    mLocationArrayList?.addAll(mPlacesModel.predictions!!)
                    if (mLocationArrayList!!.size > 0){
                        setAdapter()
                        noDataFountLL.visibility = View.GONE
                    }else{
                        noDataFountLL.visibility = View.VISIBLE
                    }
                }else{
                    noDataFountLL.visibility = View.VISIBLE
                    showToast(mActivity,getString(R.string.location_not_found))
                }
            }

            override fun onFailure(call: Call<PlacesModel>, t: Throwable) {
                dismissProgressDialog()
                noDataFountLL.visibility = View.VISIBLE
                Log.e(TAG,"**Error**"+t.message)
            }
        })
    }

    private fun setAdapter() {
        mAdapter = SearchLocationAdapter(mActivity, mLocationArrayList, mLocationtemClickListner)
        locationsRV.layoutManager = LinearLayoutManager(this)
        locationsRV.setHasFixedSize(true)
        locationsRV.adapter = mAdapter
    }

    var mLocationtemClickListner : LocationtemClickListner = object : LocationtemClickListner{
        override fun onItemClickListner(mModel: PredictionsItem) {
            getPlaceLatLong(mModel)
        }
//        override fun onItemClickListner(mModel: PredictionsItem) {
//            super.onItemClickListner(mModel)
//            getPlaceLatLong(mModel)
//        }
    }

    private fun getPlaceLatLong(mModel: PredictionsItem){
        if (isNetworkAvailable(mActivity)){
            executePlaceLatLongRequest(mModel)
        }else{
            showToast(mActivity,getString(R.string.internet_connection_error))
        }
    }

    private fun executePlaceLatLongRequest(mModel: PredictionsItem){
        showProgressDialog(mActivity)
        val mApiUrl: String = GOOGLE_PLACES_LAT_LONG +mModel.placeId+"&fields=name,geometry"+ "&key=" + PLACES_API_KEY
        val mApiInterface: Call<PlacesLatLongModel>? = RetrofitClient.apiInterface.searchPlacesLatLongRequest(mApiUrl)
        mApiInterface?.enqueue(object : Callback<PlacesLatLongModel>{
            override fun onResponse(call: Call<PlacesLatLongModel>, response: Response<PlacesLatLongModel>) {
                dismissProgressDialog()
                var mPlacesLatLongModel = response.body()
                if (mPlacesLatLongModel?.status == "OK"){
                    setUpBackResponse(mModel,mPlacesLatLongModel)
                }else{
                    showAlertDialog(mActivity,getString(R.string.place_lat_long_not_found))
                }
            }
            override fun onFailure(call: Call<PlacesLatLongModel>, t: Throwable) {
                dismissProgressDialog()
                Log.e(TAG,"**Error**"+t.message)
            }
        })
    }

    private fun setUpBackResponse(mModel :PredictionsItem, mPlacesLatLongModel: PlacesLatLongModel) {
        if (mGettingFrom == FROM_PARENT){
            val intent = Intent()
            intent.putExtra(FULL_ADDRESS,mModel.structuredFormatting?.secondaryText)
            intent.putExtra(LATITUDE,mPlacesLatLongModel.result?.geometry?.location?.lat)
            intent.putExtra(LONGITUDE,mPlacesLatLongModel.result?.geometry?.location?.lng)
            intent.putExtra(LOCATION, mModel.structuredFormatting?.mainText)
            setResult(666, intent)
            finish()
        }
    //else if (mGettingFrom == FROM_BABYSITTER){
//            val intent = Intent()
//            intent.putExtra(FULL_ADDRESS,mModel.structuredFormatting?.secondaryText)
//            intent.putExtra(LATITUDE,mPlacesLatLongModel.result?.geometry?.location?.lat)
//            intent.putExtra(LONGITUDE,mPlacesLatLongModel.result?.geometry?.location?.lng)
//            setResult(777, intent)
//            finish()
//        }else if (mGettingFrom == FROM_PLAYDATES){
//            val intent = Intent()
//            intent.putExtra(FULL_ADDRESS,mModel.structuredFormatting?.secondaryText)
//            intent.putExtra(LATITUDE,mPlacesLatLongModel.result?.geometry?.location?.lat)
//            intent.putExtra(LONGITUDE,mPlacesLatLongModel.result?.geometry?.location?.lng)
//            setResult(888, intent)
//            finish()
//        }
    }

}