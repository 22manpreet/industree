package com.industree.app.main.adapter

import android.app.Activity
import android.app.Dialog
import android.content.ContentValues.TAG
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.industree.app.R
import com.industree.app.main.Model.DeleteServiceModel
import com.industree.app.main.Model.vendorServicesItem
import com.industree.app.main.Retrofit.RetrofitClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.IndexOutOfBoundsException
import java.util.*
import kotlin.collections.ArrayList

class BusinessServicesAdapter(
    var mActivity: Activity,
    var services: ArrayList<vendorServicesItem?>?,
    var authToken: String
): RecyclerView.Adapter<BusinessServicesAdapter.ViewHolder>() {
    var progressDialog: Dialog? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BusinessServicesAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.business_service_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: BusinessServicesAdapter.ViewHolder, position: Int) {
        holder.serviceNameTV.setText(services!!.get(position)!!.serviceName)
        holder.txtPriceTV.setText("$"+services!!.get(position)!!.servicePrice)
        holder.txtDetailTV.setText(services!!.get(position)!!.serviceDescription)
        holder.deleteBtn.setOnClickListener {
            var serviceId =services!!.get(position)!!.serviceId
            try {
                executeDeleteServiceRequest(serviceId, position)
            }catch (e: IndexOutOfBoundsException)
            {

            }
        }
    }

    override fun getItemCount(): Int {
        return services!!.size
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var serviceNameTV: TextView
        var txtPriceTV: TextView
        var txtDetailTV : TextView
        var deleteBtn : TextView
        init {
            serviceNameTV=itemView.findViewById(R.id.serviceNameTV)
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV)
            txtDetailTV = itemView.findViewById(R.id.txtDetailTV)
            deleteBtn = itemView.findViewById(R.id.deleteBtn)
        }
    }
    private fun mParam(serviceId: String?): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["service_id"] = serviceId
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeDeleteServiceRequest(serviceId: String?, position: Int) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = authToken
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.DeleteServiceDataRequest(mHeaders,mParam(serviceId))

        call.enqueue(object : Callback<DeleteServiceModel> {
            override fun onFailure(call: Call<DeleteServiceModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
               dismissProgressDialog()
            }

            override fun onResponse(call: Call<DeleteServiceModel>, response: Response<DeleteServiceModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mSignUpModel = response.body()
                if (mSignUpModel!!.status == 1) {
                    services!!.removeAt(position)
                    notifyItemRemoved(position)
                } else {
            //    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    /*
   * Show Progress Dialog
   * */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }
    /*
  * Hide Progress Dialog
  * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

}