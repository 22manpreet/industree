package com.industree.app.main.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.ChangePasswordModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.utils.AppPrefrences
import com.industree.app.main.utils.PASSWORD
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class ChangePasswordActivity : BaseActivity() {

    @BindView(R.id.oldPasswordET)
    lateinit var oldPasswordET: EditText

    @BindView(R.id.newPasswordET)
    lateinit var newPasswordET: EditText

    @BindView(R.id.confirmPasswordET)
    lateinit var confirmPasswordET: EditText

    @BindView(R.id.txtChangePwdTV)
    lateinit var txtChangePwdTV: TextView

    @BindView(R.id.backRL)
    lateinit var backRL: ImageView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        ButterKnife.bind(mActivity)
    }
    @OnClick(
        R.id.txtChangePwdTV,R.id.backRL
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.txtChangePwdTV -> performChangePasswordClick()
            R.id.backRL -> onBackPressed()
        }
    }

    private fun performChangePasswordClick() {
        if (isValidate()) {
            if (isNetworkAvailable(mActivity)) {
                executeChangePwdRequest()
            } else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["password"] = oldPasswordET.text.toString()
        mMap["newPassword"] = newPasswordET.text.toString()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeChangePwdRequest() {
         val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.changePasswordDataRequest(mHeaders,mParam())

        call.enqueue(object : Callback<ChangePasswordModel> {
            override fun onFailure(call: Call<ChangePasswordModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<ChangePasswordModel>, response: Response<ChangePasswordModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mChangePasswordModel = response.body()
                if (mChangePasswordModel!!.status == 1) {
                    showToast(mActivity, mChangePasswordModel.message)
                    oldPasswordET.setText("")
                    newPasswordET.setText("")
                    confirmPasswordET.setText("")
                    AppPrefrences().writeString(mActivity, PASSWORD,newPasswordET.text.toString())
                    finish()
                } else if (mChangePasswordModel.status == 0) {
                    showAlertDialog(mActivity, mChangePasswordModel.message)
                } else {
                }
            }
        })
    }

    // - - Validations for Sign Up fields
    private fun isValidate(): Boolean {
        var flag = true
        when {
            oldPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_old_password))
                flag = false
            }
            oldPasswordET.text.toString().trim { it <= ' ' } != getPassword() -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_correct_old_password))
                flag = false
            }
            newPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_new_password))
                flag = false
            }
            confirmPasswordET.text.toString().trim { it <= ' ' } == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_confirm_password))
                flag = false
            }
            newPasswordET.text.toString().length < 6 -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_minimum_6_))
                flag = false
            }
            confirmPasswordET.text.toString().trim() == "" -> {
                showAlertDialog(mActivity, getString(R.string.please_enter_retype_password))
                flag = false
            }
            newPasswordET!!.text.toString().trim() != confirmPasswordET.text.toString()
                .trim() -> {
                showAlertDialog(mActivity, getString(R.string.the_new_re_typed))
                flag = false
            }

        }
        return flag
    }

}