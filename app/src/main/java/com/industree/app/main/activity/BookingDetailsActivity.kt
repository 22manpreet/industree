package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.industree.app.R
import com.industree.app.main.Model.*
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.BookedDetailServicesAdapter
import com.paypal.checkout.createorder.CurrencyCode
import de.hdodenhof.circleimageview.CircleImageView
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList

class BookingDetailsActivity : BaseActivity() {
    @BindView(R.id.backBookedServicesIV)
    lateinit var back: ImageView

    @BindView(R.id.nameBookedServicesTV)
    lateinit var nameBookedServicesTV: TextView

    @BindView(R.id.addressBookedServicesTV)
    lateinit var addressBookedServicesTV: TextView

    @BindView(R.id.bookedServiceIV)
    lateinit var bookedServiceIV: CircleImageView

    @BindView(R.id.seviceDetailRV)
    lateinit var seviceDetailRV: RecyclerView

    @BindView(R.id.txtPriceTV)
    lateinit var txtPriceTV: TextView

//    @BindView(R.id.bookedServiceSquareIV)
//    lateinit var bookedServiceSquareIV: ImageView
//
//    @BindView(R.id.bookedServiceCV)
//    lateinit var bookedServiceCV: CardView
//
//    @BindView(R.id.bookedServiceSquareCV)
//    lateinit var bookedServiceSquareCV: CardView

    @BindView(R.id.distanceBookedServicesTV)
    lateinit var distanceBookedServicesTV: TextView

    @BindView(R.id.endSessionTV)
    lateinit var endSessionTV: TextView

    @BindView(R.id.markAsPaidTV)
    lateinit var markAsPaidTV: TextView

    var user_id:String?=null
    var booking_id:String?=null
    var type:String?=null
    var brand_name:String?=null
    var brand_image:String?=null
    var address:String?=null
    var email:String? = null
    var bookingServiceDetailList: ArrayList<BookingServiceDetail?>? = ArrayList()
    var bookedDetailServicesAdapter: BookedDetailServicesAdapter? = null
    private var paymentAmount: String? = null

    var payID: String? = null
    var orderID: String = ""
    var payerId: String = ""
    var mCurr: String = ""
    var value: String = ""
    var mAccesstoken: String = ""
    lateinit var currencyCode: CurrencyCode
    var dialogPrivacy: BottomSheetDialog?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_booking_details)
        ButterKnife.bind(mActivity)
        getIntentData()
        if(isNetworkAvailable(mActivity)){
            executeBookingServiceDetail()
        }
        else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = user_id
        mMap["booking_id"] = booking_id
        mMap["type"]=type
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeBookingServiceDetail() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.BookingServiceDetailRequest(mHeaders,mParam())
        call.enqueue(object : Callback<BookingServiceDetailModel> {
            override fun onFailure(call: Call<BookingServiceDetailModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<BookingServiceDetailModel>,
                response: Response<BookingServiceDetailModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mBookingServiceDetailModel = response.body()!!
                if (mBookingServiceDetailModel.status == 1) {
                    nameBookedServicesTV.text=brand_name

                    Glide.with(mActivity!!)
                        .load(mBookingServiceDetailModel.userDetails!!.profileImage)
                        .centerCrop()
                        .placeholder(R.drawable.ic_placeholder)
                        .error(R.drawable.ic_placeholder)
                        .into(bookedServiceIV)

//                    if(mBookingServiceDetailModel.data!!.get(0)!!.brandImage.equals("") || mBookingServiceDetailModel.data!!.get(0)!!.brandImage.equals(null)){
//                        bookedServiceCV.visibility=View.VISIBLE
//                        bookedServiceSquareCV.visibility=View.GONE
//                        Glide.with(mActivity!!)
//                            .load(mBookingServiceDetailModel.userDetails!!.profileImage)
//                            .centerCrop()
//                            .placeholder(R.drawable.ic_placeholder)
//                            .error(R.drawable.ic_placeholder)
//                            .into(bookedServiceIV)
//                    }
//                    else {
//                        bookedServiceCV.visibility=View.GONE
//                        bookedServiceSquareCV.visibility=View.VISIBLE
//                        Glide.with(mActivity!!)
//                            .load(mBookingServiceDetailModel.data!!.get(0)!!.brandImage)
//                            .centerCrop()
//                            .placeholder(R.drawable.ic_placeholder)
//                            .error(R.drawable.ic_placeholder)
//                            .into(bookedServiceSquareIV)
//                    }
                    bookingServiceDetailList=mBookingServiceDetailModel.data
                    txtPriceTV.text="$"+mBookingServiceDetailModel.totalAmount
                    paymentAmount=mBookingServiceDetailModel.totalAmount.toString()
                    if(type.equals("1")){
                        addressBookedServicesTV.text=email
//                        bookedServiceCV.visibility= View.VISIBLE
//                        bookedServiceSquareCV.visibility= View.GONE
//                        Glide.with(mActivity!!)
//                            .load(mBookingServiceDetailModel.data!!.get(0)!!.brandImage)
//                            .centerCrop()
//                            .placeholder(R.drawable.ic_placeholder)
//                            .error(R.drawable.ic_placeholder)
//                            .into(bookedServiceIV)
                    }
                    else if(type.equals("2")){
                        addressBookedServicesTV.text=address
                    }

                    if(mBookingServiceDetailModel.sessionEnd.equals("0")){
                        distanceBookedServicesTV.text="Booked"
                        endSessionTV.visibility=View.VISIBLE
                    }
                    else if(mBookingServiceDetailModel.sessionEnd.equals("1")){
                        distanceBookedServicesTV.text="Payment Pending"
                    }
                    else if(mBookingServiceDetailModel.sessionEnd.equals("2")){
                        distanceBookedServicesTV.text="Paid"
                        endSessionTV.visibility=View.GONE
                        markAsPaidTV.visibility=View.VISIBLE
                    }
                    else if(mBookingServiceDetailModel.sessionEnd.equals("3")) {
                        distanceBookedServicesTV.text = "Completed"
                    }
                    else{
                        distanceBookedServicesTV.text="Completed"
                    }
                    setAdapter()
                }else{
                    showToast(mActivity,mBookingServiceDetailModel.message)
                }

            }
        })

    }


    private fun getIntentData() {
        user_id=intent.getStringExtra("user_id")
        booking_id=intent.getStringExtra("booking_id")
        type=intent.getStringExtra("type")
        brand_name=intent.getStringExtra("brand_name")
        brand_image=intent.getStringExtra("brand_image")
        address=intent.getStringExtra("address")
        email=intent.getStringExtra("email")
    }

    @OnClick(
        R.id.backBookedServicesIV,R.id.endSessionTV,R.id.markAsPaidTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.backBookedServicesIV -> onBackPressed()
            R.id.endSessionTV -> onClickEndSessionButton()
            R.id.markAsPaidTV -> onClickMarkAsPaid()
        }
    }

    private fun onClickMarkAsPaid() {
        if(isNetworkAvailable(mActivity)){
            executeMarkAsPaidRequest()
        }
        else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    private fun mParampaid(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["booking_id"] = booking_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeMarkAsPaidRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.MarkAsPaidRequest(mHeaders,mParampaid())
        call.enqueue(object : Callback<MarkAsPaidModel> {
            override fun onFailure(call: Call<MarkAsPaidModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<MarkAsPaidModel>,
                response: Response<MarkAsPaidModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mMarkAsPaidModel = response.body()!!
                if (mMarkAsPaidModel.status == 1) {
                    endSessionTV.visibility=View.GONE
                    markAsPaidTV.visibility=View.GONE
                    distanceBookedServicesTV.text="Completed"
                }else{
                    showToast(mActivity,mMarkAsPaidModel.message)
                }

            }
        })
    }

    private fun onClickEndSessionButton() {
        if(isNetworkAvailable(mActivity)){
            executeEndSessionRequest()
        }
        else{
            showToast(mActivity, getString(R.string.internet_connection_error))
        }

    }
    private fun mParamEndSession(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["booking_id"] = booking_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    private fun executeEndSessionRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.SessionEndRequest(mHeaders,mParamEndSession())
        call.enqueue(object : Callback<SessionEndModel> {
            override fun onFailure(call: Call<SessionEndModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<SessionEndModel>,
                response: Response<SessionEndModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mBookingServiceDetailModel = response.body()!!
                if (mBookingServiceDetailModel.status == 1) {
                    endSessionTV.visibility=View.GONE
                    distanceBookedServicesTV.text="Payment Pending"
                }else{
                    showToast(mActivity,mBookingServiceDetailModel.message)
                }

            }
        })
    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        seviceDetailRV.setLayoutManager(layoutManager)
        bookedDetailServicesAdapter = BookedDetailServicesAdapter(mActivity,bookingServiceDetailList)
        seviceDetailRV.isNestedScrollingEnabled=false
        seviceDetailRV.setHasFixedSize(false)
        seviceDetailRV.setAdapter(bookedDetailServicesAdapter)
    }

}