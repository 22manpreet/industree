package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class BookingListingModel(

	@field:SerializedName("lastPage")
	val lastPage: String? = null,

	@field:SerializedName("data")
	val data: ArrayList<BookingListingDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class BookingDeatils(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("payment_status")
	val paymentStatus: String? = null,

	@field:SerializedName("end_time")
	val endTime: String? = null,

	@field:SerializedName("book_date")
	val bookDate: String? = null,

	@field:SerializedName("payment_method")
	val paymentMethod: String? = null,

	@field:SerializedName("session_end")
	val sessionEnd: String? = null
) : Parcelable

@Parcelize
data class BookingListingDataItem(

	@field:SerializedName("youtube")
	val youtube: String? = null,

	@field:SerializedName("phone_no")
	val phoneNo: String? = null,

	@field:SerializedName("distance")
	val distance: String? = null,

	@field:SerializedName("latitude")
	val latitude: String? = null,

	@field:SerializedName("rating")
	val rating: String? = null,

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("instagram")
	val instagram: String? = null,

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("twitter")
	val twitter: String? = null,

	@field:SerializedName("brand_image")
	val brandImage: String? = null,

	@field:SerializedName("bookingServicesDeatils")
	val bookingServicesDeatils: ArrayList<BookingServicesDeatilsItem?>? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("longitude")
	val longitude: String? = null,

	@field:SerializedName("session_end")
	val sessionEnd: String? = null,

	@field:SerializedName("bookingDeatils")
	val bookingDeatils: BookingDeatils? = null,

	@field:SerializedName("website")
	val website: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("facebook")
	val facebook: String? = null,

	@field:SerializedName("count")
	val count: String? = null,

	@field:SerializedName("vendor_name")
	val vendorName: String? = null,

	@field:SerializedName("brand_name")
	val brandName: String? = null,

	@field:SerializedName("isFav")
	val isFav: String? = null,

	@field:SerializedName("totalAmount")
	val totalAmount: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null
) : Parcelable

@Parcelize
data class BookingServicesDeatilsItem(

	@field:SerializedName("service_quantity")
	val serviceQuantity: String? = null,

	@field:SerializedName("service_description")
	val serviceDescription: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("service_price")
	val servicePrice: String? = null,

	@field:SerializedName("log")
	val log: String? = null,

	@field:SerializedName("service_name")
	val serviceName: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("brand_name")
	val brandName: String? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null,

	@field:SerializedName("isRemoved")
	val isRemoved: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("brand_image")
	val brandImage: String? = null,

	@field:SerializedName("service_id")
	val serviceId: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null
) : Parcelable
