package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.graphics.drawable.DrawableCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.SubmitReportModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.whinc.widget.ratingbar.RatingBar
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.HashMap

class SubmitRatingActivity : BaseActivity() {
    @BindView(R.id.exp_rating)
    lateinit var exp_rating: RatingBar

    @BindView(R.id.crossIV)
    lateinit var cross: ImageView

    @BindView(R.id.submitRatingTV)
    lateinit var submit: TextView

    @BindView(R.id.editRateET)
    lateinit var editRateET: EditText

    var str_exp_rating:String?=""
    var vendor_id:String?=null
    var brand_id:String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_submit_rating)
        ButterKnife.bind(this)
        vendor_id=intent.getStringExtra("vendor_id")
        brand_id = intent.getStringExtra("brand_id")
        exp_rating.setOnRatingChangeListener { ratingBar, i, i1 -> str_exp_rating = i1.toString() }
    }
    @OnClick(
        R.id.crossIV,R.id.submitRatingTV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.crossIV -> onBackPressed()
            R.id.submitRatingTV -> SumitRatingClickPerformed()
        }
    }

    private fun SumitRatingClickPerformed() {
        if(isValidate())
            if(isNetworkAvailable(mActivity)){
                executeAddRatingRequest()
            }
            else {
                showToast(mActivity, getString(R.string.internet_connection_error))
            }
    }

        /*
         * Check Validations of views
         * */
        fun isValidate(): Boolean {
            var flag = true
            when {
                editRateET.text.toString().trim { it <= ' ' } == "" -> {
                    showAlertDialog(mActivity,getString(R.string.please_enter_review))
                    flag = false
                }
                str_exp_rating!!.trim { it <= ' ' } == "" -> {
                    showAlertDialog(mActivity,getString(R.string.please_add_rating))
                    flag = false
                }
            }
            return flag
        }

    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["vendor_id"]= vendor_id
        mMap["rating"]= str_exp_rating
        mMap["review"] = editRateET.text.toString()
        mMap["brand_id"] = brand_id
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeAddRatingRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AddRatingRequest(mHeaders,mParam())
        call.enqueue(object : Callback<SubmitReportModel> {
            override fun onFailure(call: Call<SubmitReportModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<SubmitReportModel>,
                response: Response<SubmitReportModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mAddReviewModel = response.body()!!
                if (mAddReviewModel!!.status == 1) {
                    showReviewAlertDialog(mActivity,getString(R.string.rated_successfully))
                }else{
                    showAlertDialog(mActivity,mAddReviewModel.message)
                }
            }
        })
    }
    /*
*
* Review Alert Dialog
* */
    fun showReviewAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener {
            alertDialog.dismiss()
            finish()
        }
        alertDialog.show()
    }
}
