package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CheckPlanModel(

	@field:SerializedName("data")
	val data: planData? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class planData(

	@field:SerializedName("purchase_plan")
	val purchasePlan: String? = null,

	@field:SerializedName("isSubscribed")
	var isSubscribed: String? = null,

	@field:SerializedName("expire_date")
	val expireDate: String? = null,

	@field:SerializedName("plan_price")
	val planPrice: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null,

	@field:SerializedName("start_date")
	val startDate: String? = null
) : Parcelable
