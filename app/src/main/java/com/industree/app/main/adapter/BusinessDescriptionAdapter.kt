package com.industree.app.main.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.*
import androidx.activity.result.contract.ActivityResultContracts
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.DataItemList
import com.industree.app.main.Model.VendorCategoryItem
import com.industree.app.main.Model.vendorServicesItem
import com.industree.app.main.`interface`.AddServiceClickListener
import com.industree.app.main.activity.SearchLocationActivity
import com.industree.app.main.activity.SubscriptionFirstActivity
import com.industree.app.main.utils.*
import kotlin.collections.ArrayList
import java.lang.IndexOutOfBoundsException

class BusinessDescriptionAdapter(
    var mActivity: Activity,
    var category: ArrayList<VendorCategoryItem?>?,
    var loggedInUserID: String,
    var authToken: String,
    var CategoryList: ArrayList<DataItemList?>?,
    var mAddServiceClickListener: AddServiceClickListener,
    var plan_id: String?
) : RecyclerView.Adapter<BusinessDescriptionAdapter.ViewHolder>() {
    var services: ArrayList<vendorServicesItem?>? = ArrayList()
    var businessServicesAdapter: BusinessServicesAdapter? = null

    //    lateinit var adapter: ArrayAdapter<String>
    var id: ArrayList<String?>? = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BusinessDescriptionAdapter.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.business_description_list, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("NewApi")
    override fun onBindViewHolder(holder: BusinessDescriptionAdapter.ViewHolder, @SuppressLint("RecyclerView") position: Int) {
        var categoryName: ArrayList<String?>? = ArrayList()
        println("size of catgoery : "+category!!.size)
        // set Spinner Category
        for (i in 0..(CategoryList!!.size - 1)) {
            categoryName!!.add(CategoryList!!.get(i)!!.title)
        }
        var adapter: ArrayAdapter<String> =
            ArrayAdapter(mActivity, R.layout.spinner_item_selected, categoryName!!)
        holder.spinner.adapter = adapter

        try {
            if(category!!.size > 0) {

                var compareValue = category!!.get(position)!!.categoryName
                if (compareValue != null) {
                    val spinnerPosition = adapter.getPosition(compareValue)
                    holder.spinner.setSelection(spinnerPosition)
                }
                holder.spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                    override fun onItemSelected(
                        parent: AdapterView<*>,
                        view: View,
                        position1: Int,
                        id: Long
                    ) {
                        category!!.get(position)!!.categoryId = CategoryList!!.get(position1)!!.categoryId
                        category!!.get(position)!!.categoryName = parent.getItemAtPosition(position1).toString()
                        //  Toast.makeText(mActivity, parent.getItemAtPosition(position1).toString(), Toast.LENGTH_LONG).show()
                    }

                    override fun onNothingSelected(parent: AdapterView<*>) {

                    }
                }

                holder.brandNameETnew.setText(category!!.get(position)!!.brandName)

//                var selectLocationLauncher =  registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result ->
//                    if (result.resultCode == 888) {
//                        // There are no request codes
//                        val mIntentData: Intent? = result.data
//                        locationTV.text = mIntentData?.getStringExtra(FULL_ADDRESS)
//                        latitude = mIntentData?.getDoubleExtra(LATITUDE, 0.0)!!
//                        longitude = mIntentData?.getDoubleExtra(LONGITUDE, 0.0)!!
//                    }
//                }


                holder.locationET.setText(category!!.get(position)!!.location)

                holder.locationET.setOnClickListener{
                    mAddServiceClickListener.onLocationClick(position)
//                    val intent = Intent(mActivity, SearchLocationActivity::class.java)
//                    intent.putExtra(LOCATION_ROLE, FROM_PARENT)
//                    selectLocationLauncher.launch(intent)
                }



                if (category?.get(position)?.brandImage!!.contains("http")) {
                    Glide.with(mActivity!!)
                        .load(category?.get(position)?.brandImage)
                        .placeholder(R.drawable.upload_image)
                        .error(R.drawable.upload_image)
                        .into(holder.ManageBusinessProfileIV)

                } else if (category?.get(position)?.brandImage!!.equals("")) {
                    Glide.with(mActivity!!)
                        .load(R.drawable.upload_image)
                        .placeholder(R.drawable.upload_image)
                        .error(R.drawable.upload_image)
                        .into(holder.ManageBusinessProfileIV)
                } else {
//            val decodedString = Base64.decode(category?.get(position)?.brandImage, Base64.DEFAULT)
//            holder.ManageBusinessProfileIV.setImageBitmap(BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size))
                    holder.ManageBusinessProfileIV.setImageURI(Uri.parse(category?.get(position)?.brandImage))

                }

                services = category!!.get(position)!!.services
                if(services != null ) {
                    val layoutManager: RecyclerView.LayoutManager =
                        LinearLayoutManager(mActivity, LinearLayoutManager.VERTICAL, false)
                    holder.servicesRV.setLayoutManager(layoutManager)
                    businessServicesAdapter = BusinessServicesAdapter(mActivity!!, services, authToken)
                    holder.servicesRV.setAdapter(businessServicesAdapter)
                }
            }
        } catch (e: IndexOutOfBoundsException) {
            if (category!!.isEmpty() || position >= category!!.size) {
                // Adding new item to list.
            }
        }
        holder.brandNameETnew.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                var brandName = s.toString()
                Log.i("FileName: ", brandName)
                try{
                    category!!.get(position)!!.brandName=brandName
                }
                catch (e: IndexOutOfBoundsException) {
                    //      category!!.add(VendorCategoryItem(category!!.get(position)!!.address,category!!.get(position)!!.categoryName,category!!.get(position)!!.categoryId,category!!.get(position)!!.servicePrice,category!!.get(position)!!.brandImage,category!!.get(position)!!.imageByteArray,category!!.get(position)!!.vendorId,brandName,category!!.get(position)!!.location,category!!.get(position)!!.services,category!!.get(position)!!.brandId))
                    category!!.add(VendorCategoryItem("","","","","","","",brandName,"",null,"","",""))

                }
                println("size of catgoery : "+category!!.size)
         //       notifyItemChanged(position)
            }
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

            }
        })
//        holder.locationET.addTextChangedListener(object : TextWatcher {
//            override fun afterTextChanged(s: Editable?) {
//                var location = holder.locationET.text.toString()
//                Log.i("FileName: ", location)
//                try {
//                    category!!.get(position)!!.location = location
//                }catch (e: IndexOutOfBoundsException) {
//                  //  category!!.add(VendorCategoryItem(category!!.get(position)!!.address,category!!.get(position)!!.categoryName,category!!.get(position)!!.categoryId,category!!.get(position)!!.servicePrice,category!!.get(position)!!.brandImage,category!!.get(position)!!.imageByteArray,category!!.get(position)!!.vendorId,category!!.get(position)!!.brandName,location,category!!.get(position)!!.services,category!!.get(position)!!.brandId))
//                    category!!.add(VendorCategoryItem("","","","","","","","",location,null,""))
//
//                }
//            }
//
//            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
//            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
//        })

        holder.addServiceTV.setOnClickListener {
            mAddServiceClickListener.onItemClickListener(services, position)
        }

        holder.ManageBusinessProfileIV.setOnClickListener {
            mAddServiceClickListener.onClickListener(position)
        }
    }



    private fun showBrandNamePopUp(toString: String, position: Int) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.edit_category_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var categoryET = alertDialog.findViewById<EditText>(R.id.categoryET)
        categoryET.setText(toString)
        val editTV = alertDialog.findViewById<TextView>(R.id.editTV)
        editTV.setOnClickListener {
            category!!.get(position)!!.brandName = categoryET.text.toString()
            notifyItemChanged(position)
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    private fun showLocationPopUp(toString: String, position: Int) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.edit_category_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        var categoryET = alertDialog.findViewById<EditText>(R.id.categoryET)
        categoryET.setText(toString)
        val editTV = alertDialog.findViewById<TextView>(R.id.editTV)
        editTV.setOnClickListener {
            category!!.get(position)!!.location = categoryET.text.toString()
            notifyItemChanged(position)
            alertDialog.dismiss()
        }
        alertDialog.show()
    }

    override fun getItemCount(): Int {
        return plan_id!!.toInt()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var ManageBusinessProfileIV: ImageView
        var spinner: Spinner
        var brandNameETnew: EditText
        var locationET: TextView
        var servicesRV: RecyclerView
        var addServiceTV: TextView
        var imageCardView:CardView

        init {
            ManageBusinessProfileIV = itemView.findViewById(R.id.ManageBusinessProfileIV)
            spinner = itemView.findViewById(R.id.spinner)
            brandNameETnew = itemView.findViewById(R.id.brandNameETnew)
            locationET = itemView.findViewById(R.id.locationET)
            servicesRV = itemView.findViewById(R.id.servicesRV)
            addServiceTV = itemView.findViewById(R.id.addServiceTV)
            imageCardView=itemView.findViewById(R.id.imageCardView)
        }
    }

}