package com.industree.app.main.utils

import android.app.Activity
import android.content.ContentValues
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import java.io.OutputStream

fun Activity.convertBitmapToUri(
    bitmap: Bitmap,
    fileName: String
) : Uri? {
    //Generating a file name
    val filename = "${fileName}.jpg"

    //Output stream
    var fos: OutputStream? = null

    var imageUri: Uri? = null

    //getting the contentResolver
    this.contentResolver?.also { resolver ->

        //Content resolver will process the contentvalues
        val contentValues = ContentValues().apply {

            //putting file information in content values
            put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
                put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
            }
        }

        //Inserting the contentValues to contentResolver and getting the Uri
        imageUri =
            resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

        //Opening an outputstream with the Uri that we got
        fos = imageUri?.let { resolver.openOutputStream(it) }
    }

    fos?.use {
        //Finally writing the bitmap to the output stream that we opened
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
    }

    return imageUri
}