package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.EditText
import android.widget.TextView
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.industree.app.R
import com.industree.app.main.Model.*
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.AddServiceClickListener
import com.industree.app.main.adapter.BusinessAdapter
import com.industree.app.main.utils.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.FileDescriptor
import java.io.IOException
import java.lang.NullPointerException

class SubscriptionFirst : BaseActivity() {
    @BindView(R.id.continueSubscritionTV)
    lateinit var continueSubscritionTV: TextView

    @BindView(R.id.business_detailRV)
    lateinit var business_detailRV: RecyclerView

    var imagePosition:Int = 0
    var dialogPrivacy: BottomSheetDialog? = null
    private val SELECT_IMAGE = 1
    val REQUEST_CODE_GALLERY = 100
    var mBitmap: Bitmap? = null

    var categoryInt:String?=null
    var businessAdapter: BusinessAdapter? = null
    var CategoryList: ArrayList<DataItemList?>? = ArrayList()
    var category: ArrayList<VendorCategoryItem?>? = ArrayList()
    lateinit var mGetAllVendorByUserIdModel: GetAllVendorByUserIdModel
    var location_position:Int?=0
    private val permissions = arrayOf(
        "android.permission.WRITE_EXTERNAL_STORAGE",
        "android.permission.READ_EXTERNAL_STORAGE",
        "android.permission.CAMERA"
    )
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_subscription_first2)
        ButterKnife.bind(mActivity)
        categoryInt=intent.getStringExtra("category")
        for (i in 1..categoryInt!!.toInt()) {
            category!!.add(VendorCategoryItem("","","","","","","","","",null,""))
        }

        if (isNetworkAvailable(mActivity!!)) {
            executeGetAllVendorDataRequest()
        } else {
            showToast(mActivity!!, getString(R.string.internet_connection_error))
        }
//     if (isNetworkAvailable(mActivity!!)) {
//            executeCategoryListingDataRequest()
//        } else {
//            showToast(mActivity!!, getString(R.string.internet_connection_error))
//        }
        val requestCode = 200
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(permissions, requestCode)
        }
    }

    @OnClick(
        R.id.continueSubscritionTV, R.id.subsciptionIV1
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.subsciptionIV1 -> onBackPressed()
            R.id.continueSubscritionTV -> onCountinueClick()
        }
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = java.util.HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeGetAllVendorDataRequest() {
        val mHeaders: MutableMap<String, String> = java.util.HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.getAllVendorByUserIdDataRequest(mHeaders, mParam())
        call.enqueue(object : Callback<GetAllVendorByUserIdModel> {
            override fun onFailure(call: Call<GetAllVendorByUserIdModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            override fun onResponse(
                call: Call<GetAllVendorByUserIdModel>,
                response: Response<GetAllVendorByUserIdModel>
            ) {
                Log.e(TAG, response.body().toString())
               dismissProgressDialog()
                mGetAllVendorByUserIdModel = response.body()!!
                if (mGetAllVendorByUserIdModel!!.status == 1) {
                    category = mGetAllVendorByUserIdModel.category
                    executeCategoryListingDataRequest()
                }
                else if(mGetAllVendorByUserIdModel!!.status ==0){
                    executeCategoryListingDataRequest()
                }
                else {
                    showAlertDialog(mActivity, getString(R.string.internal_server_error))
                }
            }
        })
    }
    private fun onCountinueClick() {
        if(category!!.size == 0){
            showAlertDialog(mActivity,getString(R.string.please_add_business))
        }
        else if(category!!.size > 0){
            for(i in 0..(category!!.size-1)){
                if(category!!.get(i)!!.brandName == ""){
                    showAlertDialog(mActivity,getString(R.string.please_add_brand_name))
                }
                else{
                    if(category!!.get(i)!!.location == ""){
                        showAlertDialog(mActivity,getString(R.string.please_add_location))
                    }
                    else{
                        if(category!!.get(i)!!.services == null){
                            showAlertDialog(mActivity,getString(R.string.please_add_atleast_onr_service))
                        }
                        else{
                            val intent = Intent(mActivity, SubscriptionSecond::class.java)
                            intent.putParcelableArrayListExtra("category", category)
                            if (mGetAllVendorByUserIdModel.data != null) {
                                intent.putExtra("vendor_name", mGetAllVendorByUserIdModel.data!!.vendorName)
                                intent.putExtra("email", mGetAllVendorByUserIdModel.data!!.email)
                                intent.putExtra("phone_num", mGetAllVendorByUserIdModel.data!!.phoneNo)
                                intent.putExtra("description", mGetAllVendorByUserIdModel.data!!.description)
                                intent.putExtra("website", mGetAllVendorByUserIdModel.data!!.website)
                                intent.putExtra("instagrem", mGetAllVendorByUserIdModel.data!!.instagram)
                                intent.putExtra("facebook", mGetAllVendorByUserIdModel.data!!.facebook)
                                intent.putExtra("twiter", mGetAllVendorByUserIdModel.data!!.twitter)
                                intent.putExtra("youtube", mGetAllVendorByUserIdModel.data!!.youtube)
                                intent.putExtra("plan_id", mGetAllVendorByUserIdModel.data!!.planId)
                                intent.putExtra("vendor_id", mGetAllVendorByUserIdModel.data!!.vendorId)
                            }
                            intent.putExtra("plan_id", categoryInt)
                            startActivity(intent)
                        }
                    }
                }
            }
        }
        else{

        }

    }

    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        business_detailRV.setLayoutManager(layoutManager)
        businessAdapter = BusinessAdapter(mActivity,categoryInt,CategoryList,category,mAddServiceClickListener, getAuthToken())
        business_detailRV.setAdapter(businessAdapter)
    }
    var mAddServiceClickListener: AddServiceClickListener = object : AddServiceClickListener {
        override fun onItemClickListener(
            services: ArrayList<vendorServicesItem?>?,
            position: Int
        )
        {
            addServiceAlertDialog(mActivity, services,position)
        }

        override fun onClickListener(position: Int) {
            imagePosition=position
            showBottomSheet()
        }

        var selectLocationLauncher =   registerForActivityResult(
            ActivityResultContracts.StartActivityForResult()) { result ->
            if (result.resultCode == 666) {
                // There are no request codes
                val mIntentData: Intent? = result.data
                category!!.get(location_position!!)!!.location = mIntentData?.getStringExtra(LOCATION)
                category!!.get(location_position!!)!!.address = mIntentData?.getStringExtra(LOCATION)+","+mIntentData?.getStringExtra(FULL_ADDRESS)

                var  latitude = mIntentData?.getDoubleExtra(LATITUDE, 0.0)!!
                var  longitude = mIntentData?.getDoubleExtra(LONGITUDE, 0.0)!!

                category!!.get(location_position!!)!!.latitude = latitude.toString()
                category!!.get(location_position!!)!!.longitude = longitude.toString()

                businessAdapter!!.notifyItemChanged(location_position!!)

            }
        }

        override fun onLocationClick(position: Int) {
            val intent = Intent(mActivity, SearchLocationActivity::class.java)
            location_position=position
            intent.putExtra(LOCATION_ROLE, FROM_PARENT)
            selectLocationLauncher.launch(intent)
        }
    }
    @SuppressLint("NotifyDataSetChanged")
    fun addServiceAlertDialog(
        mActivity: Activity?,
        servicess: ArrayList<vendorServicesItem?>?,
        position: Int
    ) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.add_service_dialog)
        alertDialog.setCanceledOnTouchOutside(true)
        alertDialog.setCancelable(true)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val serviceNameET = alertDialog.findViewById<EditText>(R.id.serviceNameET)
        val serviceCostET = alertDialog.findViewById<EditText>(R.id.serviceCostET)
        val serviceDescriptionET = alertDialog.findViewById<EditText>(R.id.serviceDescriptionET)

        val submitTV = alertDialog.findViewById<TextView>(R.id.submitTV)
        submitTV.setOnClickListener {
            var  services: ArrayList<vendorServicesItem?>? = ArrayList()
            val service_name = serviceNameET.text.toString()
            val service_cost = serviceCostET.text.toString()
            val service_description = serviceDescriptionET.text.toString()
            services!!.add(vendorServicesItem(service_description, "", service_cost, service_name, "", "", ""))
            if (service_name.equals("") && service_cost.equals("")) {
                showAlertDialog(mActivity, getString(R.string.service_and_cost_field_required))
            } else {
//  //              category!![position]!!.services!!.add(vendorServicesItem(service_description, "", service_cost, service_name, "", "", ""))
//               services!!.add(vendorServicesItem(service_description, "", service_cost, service_name, "", "", ""))
//               category!!.set(position,VendorCategoryItem(
//                    category!!.get(position)?.address,
//                   category!!.get(position)!!.categoryName,
//                    category!!.get(position)!!.categoryId,
//                    category!!.get(position)!!.servicePrice,
//                    category!!.get(position)!!.brandImage,
//                   category!!.get(position)!!.imageByteArray,
//                    category!!.get(position)!!.vendorId,
//                    category!!.get(position)!!.brandName,
//                    category!!.get(position)!!.location,
//                   services,
//                    category!!.get(position)!!.brandId))
                try {
                    var mModel=category!!.get(position)
                    if(mModel!=null) {
                        category!!.set(position,VendorCategoryItem(category!!.get(position)!!.address,category!!.get(position)!!.categoryName,category!!.get(position)!!.categoryId,category!!.get(position)!!.servicePrice,category!!.get(position)!!.brandImage,category!!.get(position)!!.imageByteArray,category!!.get(position)!!.vendorId,category!!.get(position)!!.brandName,category!!.get(position)!!.location,services,category!!.get(position)!!.brandId,
                            category!!.get(position)!!.latitude,category!!.get(position)!!.longitude))
                    }

                }
                catch (e: NullPointerException){
                    category!!.add(VendorCategoryItem("","","","","","","","","",services,"","",""))
                }
                catch (e: java.lang.IndexOutOfBoundsException) {
                    if (category!!.isEmpty() || position >= category!!.size) {
                        // Adding new item to list.
                        category!!.add(VendorCategoryItem("","","","","","","","","",services,"","",""))
                        var mModel=category!!.get(position)!!.services
                        mModel!!.add(vendorServicesItem(service_description, "", service_cost, service_name, "", "", ""))

                    }
                }
                businessAdapter!!.notifyItemChanged(position)
//                 businessAdapter!!.notifyDataSetChanged()
                alertDialog.dismiss()
            }
        }
        alertDialog.show()
    }
    private fun showBottomSheet() {
        val view: View = LayoutInflater.from(mActivity).inflate(R.layout.layout_status_bottom_sheet, null)
        dialogPrivacy = mActivity?.let { BottomSheetDialog(it, R.style.BottomSheetDialog) }
        dialogPrivacy!!.setContentView(view)
        dialogPrivacy!!.setCanceledOnTouchOutside(true)
        //disabling the drag down of sheet
        dialogPrivacy!!.setCancelable(true)
        //cancel button click

        val txtCameraTV: TextView? = dialogPrivacy!!.findViewById(R.id.txtCameraTV)
        val txtGalleryTV: TextView? = dialogPrivacy!!.findViewById(R.id.txtGalleryTV)

        txtCameraTV?.setOnClickListener {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent,SELECT_IMAGE)
            dialogPrivacy!!.dismiss()
        }
        txtGalleryTV?.setOnClickListener {
            val intent = Intent(Intent.ACTION_PICK)
            intent.type="image/*"
            startActivityForResult(intent, REQUEST_CODE_GALLERY)
            dialogPrivacy!!.dismiss()
        }

        val cancelTV: TextView? = dialogPrivacy!!.findViewById(R.id.btnCancelTV)
        cancelTV?.setOnClickListener {
            dialogPrivacy!!.dismiss()
        }
        dialogPrivacy!!.show()
    }
    // Override this method too
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SELECT_IMAGE) {
            if (resultCode == RESULT_OK) {
                if (data != null) {
                    try {
                        mBitmap =data?.getParcelableExtra("data")
                        var mImageUri = convertBitmapToUri(mBitmap!!,"CAT1")

                        try{
                            category?.get(imagePosition)?.brandImage = mImageUri.toString()
                        }
                        catch (e: IndexOutOfBoundsException) {
                            category!!.add(imagePosition,VendorCategoryItem("","","","",mImageUri.toString(),"","","","",null,""))
                        }
                     //                        val byteArrayOutputStream = ByteArrayOutputStream()
//                        mBitmap!!.compress(Bitmap.CompressFormat.PNG, 30, byteArrayOutputStream)
//                        var byteArray: ByteArray = byteArrayOutputStream.toByteArray()
//                        var encoded: String = Base64.encodeToString(byteArray, Base64.DEFAULT)
//                        category?.get(imagePosition)?.brandImage = encoded
                        businessAdapter?.notifyDataSetChanged()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            } else if (resultCode == RESULT_CANCELED) {
                // Toast.makeText(mActivity, "Cancel", Toast.LENGTH_SHORT).show()
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_CODE_GALLERY) {
            //handle image
            if (data != null) {
                try {
                    val selectedMediaUri: Uri = data!!.getData()!!
                    Log.e(TAG,"**Uri**"+selectedMediaUri.toString())

                    try{
                        category?.get(imagePosition)?.brandImage = selectedMediaUri.toString()
                    }
                    catch (e: IndexOutOfBoundsException) {
                        category!!.add(imagePosition,VendorCategoryItem("","","","",selectedMediaUri.toString(),"","","","",null,""))
                    }

//                    mBitmap= uriToBitmap(selectedMediaUri!!)
//                    val byteArrayOutputStream = ByteArrayOutputStream()
//                    mBitmap!!.compress(Bitmap.CompressFormat.PNG, 30, byteArrayOutputStream)
//                    var byteArray: ByteArray = byteArrayOutputStream.toByteArray()
//                    var encoded: String = Base64.encodeToString(byteArray, Base64.DEFAULT)
//                    category?.get(imagePosition)?.brandImage = encoded
                    businessAdapter?.notifyDataSetChanged()
                } catch (e: IOException) {
                    e.printStackTrace()
                }
            }
        }
    }
    private fun uriToBitmap(selectedFileUri: Uri): Bitmap? {
        try {
            val parcelFileDescriptor = contentResolver.openFileDescriptor(selectedFileUri, "r")
            val fileDescriptor: FileDescriptor = parcelFileDescriptor!!.fileDescriptor
            val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
            parcelFileDescriptor.close()
            return image
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return null
    }


    private fun mParams(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }

    private fun executeCategoryListingDataRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        //   showProgressDialog(activity)
        val call = RetrofitClient.apiInterface.categoryListingDataRequest(mHeaders, mParams())
        call.enqueue(object : Callback<CategoryListingModel> {
            override fun onFailure(call: Call<CategoryListingModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                //            dismissProgressDialog()
            }

            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<CategoryListingModel>,
                response: Response<CategoryListingModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mCategoryListingModel: CategoryListingModel = response.body()!!
                if (mCategoryListingModel.status == 1) {
                    CategoryList = mCategoryListingModel.data
                    //   println("list size : "+CategoryList!!.size)
                    setAdapter()
                    //      setSpinner()
                } else {
                    showToast(mActivity, mCategoryListingModel.message)
                }
            }
        })
    }

    override fun onBackPressed() {
        val i = Intent(mActivity, HomeActivity::class.java)
        startActivity(i)
        finish()
    }
}