package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NotificationBadgeCountModel(

	@field:SerializedName("chatCount")
	val chatCount: String? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("badgeCount")
	val badgeCount: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable
