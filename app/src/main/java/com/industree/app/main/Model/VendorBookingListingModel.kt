package com.industree.app.main.Model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class VendorBookingListingModel(

	@field:SerializedName("lastPage")
	val lastPage: String? = null,

	@field:SerializedName("data")
	val data: ArrayList<vendorDataItem?>? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable

@Parcelize
data class vendorDataItem(

	@field:SerializedName("phone_no")
	val phoneNo: String? = null,

	@field:SerializedName("distance")
	val distance: String? = null,

	@field:SerializedName("email_verification")
	val emailVerification: String? = null,

	@field:SerializedName("paypalName")
	val paypalName: String? = null,

	@field:SerializedName("profileImage")
	val profileImage: String? = null,

	@field:SerializedName("facebook_token")
	val facebookToken: String? = null,

	@field:SerializedName("paypalStreet1")
	val paypalStreet1: String? = null,

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("isSubscribed")
	val isSubscribed: String? = null,

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("paypalState")
	val paypalState: String? = null,

	@field:SerializedName("apple_token")
	val appleToken: String? = null,

	@field:SerializedName("brand_image")
	val brandImage: String? = null,

	@field:SerializedName("customerId")
	val customerId: String? = null,

	@field:SerializedName("purchasePlan")
	val purchasePlan: String? = null,

	@field:SerializedName("expireDate")
	val expireDate: String? = null,

	@field:SerializedName("booking_details")
	val bookingDetails: BookingDetails? = null,

	@field:SerializedName("paypalID")
	val paypalID: String? = null,

	@field:SerializedName("first_name")
	val firstName: String? = null,

	@field:SerializedName("email")
	val email: String? = null,

	@field:SerializedName("payment_method")
	val paymentMethod: String? = null,

	@field:SerializedName("paypalToken")
	val paypalToken: String? = null,

	@field:SerializedName("session_end")
	val sessionEnd: String? = null,

	@field:SerializedName("paypalMiddleName")
	val paypalMiddleName: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("payment_status")
	val paymentStatus: String? = null,

	@field:SerializedName("last_name")
	val lastName: String? = null,

	@field:SerializedName("services")
	val services: ArrayList<VendorServiceItem?>? = null,

	@field:SerializedName("paypalEmail")
	val paypalEmail: String? = null,

	@field:SerializedName("accessToken")
	val accessToken: String? = null,

	@field:SerializedName("paypalCity")
	val paypalCity: String? = null,

	@field:SerializedName("totalAmount")
	val totalAmount: String? = null,

	@field:SerializedName("paypalPostalCode")
	val paypalPostalCode: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("disable")
	val disable: String? = null,

	@field:SerializedName("google_token")
	val googleToken: String? = null,

	@field:SerializedName("paypalVerifiedAccount")
	val paypalVerifiedAccount: String? = null,

	@field:SerializedName("paypalCountry")
	val paypalCountry: String? = null,

	@field:SerializedName("verification_code")
	val verificationCode: String? = null,

	@field:SerializedName("plan_id")
	val planId: String? = null,

	@field:SerializedName("refreshToken")
	val refreshToken: String? = null
) : Parcelable

@Parcelize
data class VendorServiceItem(

	@field:SerializedName("service_quantity")
	val serviceQuantity: String? = null,

	@field:SerializedName("service_description")
	val serviceDescription: String? = null,

	@field:SerializedName("address")
	val address: String? = null,

	@field:SerializedName("service_price")
	val servicePrice: String? = null,

	@field:SerializedName("log")
	val log: String? = null,

	@field:SerializedName("service_name")
	val serviceName: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("brand_name")
	val brandName: String? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null,

	@field:SerializedName("isRemoved")
	val isRemoved: String? = null,

	@field:SerializedName("category_id")
	val categoryId: String? = null,

	@field:SerializedName("brand_image")
	val brandImage: String? = null,

	@field:SerializedName("service_id")
	val serviceId: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("location")
	val location: String? = null,

	@field:SerializedName("lat")
	val lat: String? = null
) : Parcelable

@Parcelize
data class BookingDetails(

	@field:SerializedName("booking_id")
	val bookingId: String? = null,

	@field:SerializedName("start_time")
	val startTime: String? = null,

	@field:SerializedName("amount")
	val amount: String? = null,

	@field:SerializedName("user_id")
	val userId: String? = null,

	@field:SerializedName("created")
	val created: String? = null,

	@field:SerializedName("vendor_id")
	val vendorId: String? = null,

	@field:SerializedName("payment_status")
	val paymentStatus: String? = null,

	@field:SerializedName("end_time")
	val endTime: String? = null,

	@field:SerializedName("book_date")
	val bookDate: String? = null,

	@field:SerializedName("payment_method")
	val paymentMethod: String? = null,

	@field:SerializedName("brand_id")
	val brandId: String? = null,

	@field:SerializedName("session_end")
	val sessionEnd: String? = null
) : Parcelable
