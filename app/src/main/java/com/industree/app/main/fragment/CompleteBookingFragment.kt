package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.Unbinder
import com.industree.app.R
import com.industree.app.main.Model.BookingListingDataItem
import com.industree.app.main.Model.BookingListingModel
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.BookedServicesAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class CompleteBookingFragment : BaseFragment() {
    // - - Initialize Widgets
    lateinit var mUnbinder: Unbinder

    @BindView(R.id.bookedServicesRV)
    lateinit var verticalRecyclerView: RecyclerView

    var bookedServicesAdapter: BookedServicesAdapter? = null
    var bookingServiceItemList: ArrayList<BookingListingDataItem?>? = ArrayList()
    @SuppressLint("UseRequireInsteadOfGet")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View =   inflater.inflate(R.layout.fragment_complete_booking, container, false)
        mUnbinder = ButterKnife.bind(this, view)
        if(isNetworkAvailable(activity!!)){
            executeBookingServiceRequest()
        } else {
            showToast(activity, getString(R.string.internet_connection_error))
        }
        return view
    }
    private fun mParam(): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["pageNo"] = "1"
        mMap["perPage"] = "20"
        mMap["latitude"] = getLatitude()
        mMap["longitude"] = getLongitude()
        mMap["type"] = "2"
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun executeBookingServiceRequest() {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(activity!!)
        val call = RetrofitClient.apiInterface.bookingServiceDataRequest(mHeaders,mParam())
        call.enqueue(object : Callback<BookingListingModel> {
            override fun onFailure(call: Call<BookingListingModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<BookingListingModel>, response: Response<BookingListingModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mBookingListingModel : BookingListingModel = response.body()!!
                if (mBookingListingModel!!.status == 1) {
                    bookingServiceItemList=mBookingListingModel.data
                    setAdapter()
                } else if (mBookingListingModel!!.status == 2) {
                    showAlertDialog(activity!!, mBookingListingModel.message)
                    // showDoubleButtonAlertDialog(mActivity, mSignInModel.message, editEmailET.text.toString())
                } else if (mBookingListingModel!!.status == 0) {
                    showAlertDialog(activity!!, mBookingListingModel.message)
                }else {
                    showAlertDialog(activity!!, getString(R.string.internal_server_error))
                }
            }
        })
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter() {
        val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        verticalRecyclerView.setLayoutManager(layoutManager)
        bookedServicesAdapter = BookedServicesAdapter(activity!!,bookingServiceItemList,getLoggedInUserID())
        verticalRecyclerView.setAdapter(bookedServicesAdapter)
    }

}