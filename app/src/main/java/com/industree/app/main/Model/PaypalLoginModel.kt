package com.industree.app.main.Model

import com.google.gson.annotations.SerializedName

data class PaypalLoginModel(

	@field:SerializedName("login_url")
	val loginUrl: String? = null,

	@field:SerializedName("Accountstatus")
	val accountstatus: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)
