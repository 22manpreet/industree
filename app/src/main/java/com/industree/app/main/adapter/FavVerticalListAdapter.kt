package com.industree.app.main.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.FavouriteDataItem
import com.industree.app.main.`interface`.FavouriteClickListener
import com.industree.app.main.activity.DetailActivity
import java.util.ArrayList

class FavVerticalListAdapter(
    var activity: FragmentActivity?,
    var favouriteDetailItemList: ArrayList<FavouriteDataItem?>?,
    var loggedInUserID: String,
    var mFavouriteClickListener: FavouriteClickListener
) : RecyclerView.Adapter<FavVerticalListAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.fav_vertical_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(activity!!)
            .load(favouriteDetailItemList?.get(position)?.brandImage)
            .centerCrop()
            .placeholder(R.drawable.image_placeholder)
            .error(R.drawable.image_placeholder)
            .into(holder.favVerticalListImage)
        holder.nameFavVerticalList.setText(favouriteDetailItemList?.get(position)?.brandName)
        holder.addressFavVerticalList.setText(favouriteDetailItemList?.get(position)?.address)
        holder.distanceFavVerticalList.setText(favouriteDetailItemList?.get(position)?.distance)
        if(favouriteDetailItemList?.get(position)?.isFav.equals("0")){
            holder.favVerticalListImage1.setImageResource(R.drawable.ic_unfav)
        }
        else if(favouriteDetailItemList?.get(position)?.isFav.equals("1")){
            holder.favVerticalListImage1.setImageResource(R.drawable.stroke_heart)
        }
        holder.favVerticalListImage1.setOnClickListener {
            mFavouriteClickListener.onItemClickListener(favouriteDetailItemList?.get(position)!!.vendorId, favouriteDetailItemList!!.get(position)!!.categoryId,position,favouriteDetailItemList?.get(position)?.brandName)
         //   executeFavUnfavExecute(favouriteDetailItemList?.get(position)!!.vendorId, favouriteDetailItemList!!.get(position)!!.categoryId)
        }
        holder.favouriteVerticalListLL.setOnClickListener {
            val intent = Intent( activity, DetailActivity::class.java)
            intent.putExtra("user_id", loggedInUserID)
            intent.putExtra("vendor_id",favouriteDetailItemList!!.get(position)?.vendorId)
            intent.putExtra("brand_id", favouriteDetailItemList!!.get(position)?.brandId)
            intent.putExtra("brand_image", favouriteDetailItemList!!.get(position)?.brandImage)
            intent.putExtra("categoryId",favouriteDetailItemList!!.get(position)?.categoryId)
            activity!!.startActivity(intent)
        }
    }
    override fun getItemCount(): Int {
        return favouriteDetailItemList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var favVerticalListImage: ImageView
        var favVerticalListImage1: ImageView
        var nameFavVerticalList: TextView
        var distanceFavVerticalList: TextView
        var addressFavVerticalList: TextView
        var favouriteVerticalListLL:LinearLayout

        init {
            favVerticalListImage1 = itemView.findViewById(R.id.favVerticalListIV1)
            favVerticalListImage = itemView.findViewById(R.id.favVerticalListIV)
            nameFavVerticalList = itemView.findViewById(R.id.nameFavVerticalListTV)
            distanceFavVerticalList = itemView.findViewById(R.id.distanceFavVerticalListTV)
            addressFavVerticalList = itemView.findViewById(R.id.addressFavVerticalListTV)
            favouriteVerticalListLL=itemView.findViewById(R.id.favouriteVerticalListLL)
        }
    }


}