package com.industree.app.main.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.industree.app.R
import com.industree.app.main.adapter.BookingHistoryAdapter
import com.industree.app.main.adapter.FavVerticalListAdapter


class BookingHistoryFragment : Fragment() {

    var bookingHistoryAdapter: BookingHistoryAdapter? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater.inflate(R.layout.fragment_booking_history, container, false)
        initRecyclerView(view)

        return view
    }
    private fun initRecyclerView(view: View) {
        var verticalRecyclerView: RecyclerView = view.findViewById(R.id.bookingHistoryRV)
        val layoutManager2: RecyclerView.LayoutManager =
            LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        verticalRecyclerView.setLayoutManager(layoutManager2)
        bookingHistoryAdapter = BookingHistoryAdapter()
        verticalRecyclerView.setAdapter(bookingHistoryAdapter)
    }

}