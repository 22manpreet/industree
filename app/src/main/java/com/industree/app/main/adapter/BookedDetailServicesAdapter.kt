package com.industree.app.main.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.industree.app.R
import com.industree.app.main.Model.BookingServiceDetail
import java.util.ArrayList

class BookedDetailServicesAdapter(
    var mActivity: Activity,
    var bookingServiceDetailList: ArrayList<BookingServiceDetail?>?
): RecyclerView.Adapter<BookedDetailServicesAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.booked_services_detail_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
         holder.serviceNameTV.text=bookingServiceDetailList!!.get(position)!!.serviceName
        holder.txtPriceTV.text="$"+bookingServiceDetailList!!.get(position)!!.servicePrice
        holder.txtDetailTV.text=bookingServiceDetailList!!.get(position)!!.serviceDescription
    }

    override fun getItemCount(): Int {
       return bookingServiceDetailList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var serviceNameTV: TextView
        var txtPriceTV: TextView
        var txtDetailTV: TextView

        init {

            serviceNameTV = itemView.findViewById(R.id.serviceNameTV)
            txtPriceTV = itemView.findViewById(R.id.txtPriceTV)
            txtDetailTV = itemView.findViewById(R.id.txtDetailTV)

        }
    }

}