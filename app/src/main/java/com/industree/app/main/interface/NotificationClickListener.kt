package com.industree.app.main.`interface`

interface NotificationClickListener {
    fun onItemClickListener(
        notificationType: String?,
        otherUserId: String?,
        userName: String?,
        roomId: String?,
        userName1: String?
    )
}