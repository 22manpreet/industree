package com.industree.app.main.fcm

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Intent
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.gson.Gson
import com.industree.app.R
import com.industree.app.main.activity.BusinessServicesActivity
import com.industree.app.main.activity.ChatActivity
import com.industree.app.main.activity.HomeActivity
import com.industree.app.main.activity.NotificationActivity
import org.json.JSONObject

@SuppressLint("MissingFirebaseInstanceTokenRefresh")
class FirebaseService : FirebaseMessagingService() {

    val TAG = "FirebaseMessagingService"
    var mNotificationModel : CustomNotiModel? = null
    var mTitle = ""
    var mBody = ""
    var mIntent : Intent? = null
    @SuppressLint("LongLogTag")
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        var mData = remoteMessage.data
        Log.e(TAG,"*****DATA*****$mData")
        if (remoteMessage.data != null) {
            try{
                var mJSONData : JSONObject = JSONObject(mData as Map<*, *>?)
                var data=   mJSONData.get("data")
                mNotificationModel = Gson().fromJson(data.toString(), CustomNotiModel::class.java)
            }catch (e: Exception){
                e.printStackTrace()
            }
            sendNotifications()
        }
    }

    @SuppressLint("ResourceAsColor")
    private fun sendNotifications() {
        //Title
        if (mNotificationModel?.title!!.isNotEmpty()){
            mTitle = mNotificationModel?.title!!
        }
//        else{
//            mTitle = getString(R.string.app_name)
//        }
        //Message
        mBody = mNotificationModel?.description!!

         if (mNotificationModel?.notificationType.equals("1")) {
              mIntent  = Intent(this, ChatActivity::class.java)
            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
             mIntent!!.putExtra("room_id",mNotificationModel!!.roomId)
             mIntent!!.putExtra("other_user_name",mNotificationModel!!.userName)

        }else if (mNotificationModel?.notificationType.equals("2")) {
             mIntent = Intent(this, BusinessServicesActivity::class.java)
            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
             mIntent!!.putExtra("Notification_type","1")
        }else if (mNotificationModel?.notificationType.equals("3")) {
             mIntent  = Intent(this, HomeActivity::class.java)
            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
             mIntent!!.putExtra("Notification_type","1")
        }
         else if (mNotificationModel?.notificationType.equals("4")) {
             mIntent  = Intent(this, NotificationActivity::class.java)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

         }else if (mNotificationModel?.notificationType.equals("5")) {
             mIntent  = Intent(this, NotificationActivity::class.java)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)

         }else if (mNotificationModel?.notificationType.equals("6")) {
             mIntent  = Intent(this, NotificationActivity::class.java)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
         }
         else{
             mIntent  = Intent(this, NotificationActivity::class.java)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
             mIntent!!.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        val pendingIntent = PendingIntent.getActivity(this, 0, mIntent, PendingIntent.FLAG_ONE_SHOT)
        val channelId = getString(R.string.default_notification_channel_id)
        val defaultSoundUri: Uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder: NotificationCompat.Builder =
            NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification_icon)
                .setColor(ContextCompat.getColor(this,R.color.yellow))
                .setContentTitle(mBody)
                .setContentText(mTitle)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationManager != null) {
                val channel = NotificationChannel(
                    channelId, getString(R.string.default_notification_channel_name),
                    NotificationManager.IMPORTANCE_DEFAULT)
                channel.enableVibration(true)
                notificationManager.createNotificationChannel(channel)
            }
        }
        notificationManager?.notify(getAlphaNumeric()!!, notificationBuilder.build())
    }

    open fun getAlphaNumeric(): Int? {
        val n = 3
        // chose a Character random from this String
        val AlphaNumericString = ("0123456789")
        // create StringBuffer size of AlphaNumericString
        val sb = StringBuilder(n)
        for (i in 0 until n) {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            val index = (AlphaNumericString.length * Math.random()).toInt()
            // add Character one by one in end of sb
            sb.append(
                AlphaNumericString[index]
            )
        }
        return sb.toString().toInt()
    }


}