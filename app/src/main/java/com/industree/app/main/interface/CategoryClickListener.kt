package com.industree.app.main.`interface`

import com.industree.app.main.Model.DataItemList

interface CategoryClickListener {
    fun onItemClickListener(mModel: DataItemList?)
}