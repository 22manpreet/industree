package com.industree.app.main.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.FragmentActivity
import com.industree.app.R
import com.industree.app.main.activity.SigninActivity
import com.industree.app.main.utils.*
import java.util.*

open class BaseFragment : Fragment() {
    // - - Get Class Name
    open var TAG = this@BaseFragment.javaClass.simpleName
    var progressDialog: Dialog? = null


    /*
   * Check Internet Connections
   * */
    fun isNetworkAvailable(mContext: Context): Boolean {
        val connectivityManager =
            mContext.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetworkInfo = connectivityManager.activeNetworkInfo
        return activeNetworkInfo != null && activeNetworkInfo.isConnected
    }
    /*
* Toast Message
* */
    fun showToast(mActivity: Activity?, strMessage: String?) {
        Toast.makeText(mActivity, strMessage, Toast.LENGTH_SHORT).show()
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }
    /*
        * Hide Progress Dialog
        * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }
  /*
    * Error Alert Dialog
    * */
    fun showAccountDisableAlertDialog(mActivity: Activity?, mMessage : String) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_access_token_decline)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        txtMessageTV.text = mMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            val preferences: SharedPreferences =
                mActivity?.let { AppPrefrences().getPreferences(it) }!!
            val editor = preferences.edit()
            editor.clear()
            editor.apply()
            editor.commit()
            val mIntent = Intent(mActivity, SigninActivity::class.java)
            TaskStackBuilder.create(mActivity).addNextIntentWithParentStack(mIntent)
                .startActivities()
            mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP // To clean up all activities
            mIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            mIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(mIntent)
            requireActivity().finish()
            requireActivity().finishAffinity()
        }
        alertDialog.show()
    }
    /*
*
* Get User Name
* */
    fun getFirstName(): String {
        return AppPrefrences().readString(requireActivity(), FIRSTNAME, "")!!
    }
    /*
*
* Get User Name
* */
    fun getLastName(): String {
        return AppPrefrences().readString(requireActivity(), LASTNAME, "")!!
    }

    /*
*
* Get User Email
* */
    fun getUserEmail(): String {
        return AppPrefrences().readString(requireActivity(), EMAIL, "")!!
    }
    /*
  * Get User Profile Pic
  * */
    fun getUserProfilePic(): String {
        return AppPrefrences().readString(requireActivity(), PROFILEPIC, "")!!
    }

    /*
   * Getting User Auth Token
  * */
    fun getAuthToken(): String {
        return AppPrefrences().readString(requireActivity(), AUTHTOKEN, "")!!
    }
    /*
* Getting User ID
* */
    fun getLoggedInUserID(): String {
        return AppPrefrences().readString(requireActivity(), USERID, "")!!
    }
    /*
* Getting Latitude
* */
    fun getLatitude(): String {
        return AppPrefrences().readString(requireActivity(), LATITUDE, "")!!
    }
    /*
* Getting Longitude
* */
    fun getLongitude(): String {
        return AppPrefrences().readString(requireActivity(), LONGITUDE, "")!!
    }
    /*
*
* Get User PlanId
* */
    fun getPlanId(): String {
        return AppPrefrences().readString(requireActivity(), PLANID, "")!!
    }
    fun getIsSubscribed(): String {
        return AppPrefrences().readString(requireActivity(), ISSUBSCRIBED, "")!!
    }


    // - - To Show Alert Dialog
    fun showAlertDialog(mActivity: Activity?, strMessage: String?) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss() }
        alertDialog.show()
    }

}