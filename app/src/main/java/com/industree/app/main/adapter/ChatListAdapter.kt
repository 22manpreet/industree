package com.industree.app.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.ChatDataItem
import com.industree.app.main.`interface`.ChatItemClickListner
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList


class ChatListAdapter(
    var activity: FragmentActivity?,
    var ChatDetailList: ArrayList<ChatDataItem?>?,
    var mChatItemClickListner: ChatItemClickListner
    ): RecyclerView.Adapter<ChatListAdapter.MyViewHolder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChatListAdapter.MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.chat_list, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChatListAdapter.MyViewHolder, position: Int) {
        var mModel  = ChatDetailList?.get(position)
        holder.chatNameTV.text=ChatDetailList?.get(position)!!.name
        holder.chatMessageTV.text=ChatDetailList?.get(position)!!.lastMessage

        val Timestamp: Long = ChatDetailList?.get(position)!!.lastMessageTime!!.toLong()
        val timeD = Date(Timestamp * 1000)
        val sdf = SimpleDateFormat("hh:mm a")
        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
        val Time: String = sdf.format(timeD)
         holder.timeTV.text=Time

        Glide.with(activity!!).load(ChatDetailList?.get(position)?.profileImage)
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into((holder.chatProfileIV))
        if(ChatDetailList?.get(position)!!.messageCount!!.equals("0")){
            holder.Chat_badgeRL.visibility=View.GONE
        }
        else{
            holder.Chat_badgeRL.visibility=View.VISIBLE
            holder.badgeTV.text=ChatDetailList?.get(position)!!.messageCount!!
        }
        holder.chatListLL.setOnClickListener {
//            val context=holder.chatListLL.context
//            val intent = Intent( context, ChatActivity::class.java)
//            context.startActivity(intent)
            mChatItemClickListner.onItemClickListner(mModel)
        }
    }
    override fun getItemCount(): Int {
        return ChatDetailList!!.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var chatProfileIV: ImageView = itemView.findViewById(R.id.chatProfileIV)
        var chatNameTV: TextView = itemView.findViewById(R.id.chatNameTV)
        var chatMessageTV: TextView =itemView.findViewById(R.id.chatMessageTV)
        var chatListLL : LinearLayout=itemView.findViewById(R.id.chatListLL)
        var timeTV : TextView = itemView.findViewById(R.id.timeTV)
        var Chat_badgeRL : RelativeLayout=itemView.findViewById(R.id.Chat_badgeRL)
        var badgeTV :TextView =itemView.findViewById(R.id.badgeTV)
    }

}