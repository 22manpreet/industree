package com.industree.app.main.adapter

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues.TAG
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.industree.app.R
import com.industree.app.main.Model.AcceptRejectbookingModel
import com.industree.app.main.Model.NotificationDataItem
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.`interface`.NotificationClickListener
import com.industree.app.main.`interface`.PaginationHomeInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class NotificationListAdapter(
    var mActivity: Activity,
    var NotifiList: ArrayList<NotificationDataItem?>?,
    var mNotificationClickListener: NotificationClickListener,
    var mInterfaceData: PaginationHomeInterface,
    var authToken: String
) : RecyclerView.Adapter<NotificationListAdapter.ViewHolder>() {
    var progressDialog: Dialog? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view: View = layoutInflater.inflate(R.layout.notification_list, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var mModel = NotifiList!!.get(position)

        if (NotifiList!!.size % 10 == 0 && NotifiList!!.size - 1 == position){
            mInterfaceData.mHomePagination(mModel)
        }

        holder.textNotificationTV.text=NotifiList!!.get(position)!!.description

        val Timestamp: Long = NotifiList?.get(position)!!.created!!.toLong()
        val timeD = Date(Timestamp * 1000)
        val sdf = SimpleDateFormat("MMM dd, hh:mm aa")
        sdf.timeZone = TimeZone.getTimeZone("GMT+05:30")
        val Time: String = sdf.format(timeD)
        holder.dateNotificationTV.text=Time

        Glide.with(mActivity!!)
            .load(NotifiList?.get(position)?.profileImage)
            .centerCrop()
            .placeholder(R.drawable.ic_placeholder)
            .error(R.drawable.ic_placeholder)
            .into(holder.imageNotificationIV)

        if(NotifiList!!.get(position)!!.notificationType=="4"){
             holder.accept_rejectLL.visibility=View.VISIBLE
        }
        else{
            holder.accept_rejectLL.visibility=View.GONE
        }
        holder.acceptTV.setOnClickListener {
            executeAcceptRejectRequest(position,"1",NotifiList!!.get(position)!!.notificationId)
        }
       holder.rejectTV.setOnClickListener {
           executeAcceptRejectRequest(position,"2",NotifiList!!.get(position)!!.notificationId)
       }
        holder.itemView.setOnClickListener {
            mNotificationClickListener.onItemClickListener(mModel!!.notificationType,mModel.otherUserId,mModel.userName,mModel.roomId,mModel.userName)
        }
    }

    private fun executeAcceptRejectRequest(position: Int, type: String, notificationId: String?) {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["notification_id"] = notificationId
        mMap["type"] = type
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = authToken
        showProgressDialog(mActivity)
        val call = RetrofitClient.apiInterface.AcceptRejectbookingRequest(mHeaders,mMap)
        call.enqueue(object : Callback<AcceptRejectbookingModel> {
            override fun onFailure(call: Call<AcceptRejectbookingModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }
            @SuppressLint("NotifyDataSetChanged")
            override fun onResponse(
                call: Call<AcceptRejectbookingModel>,
                response: Response<AcceptRejectbookingModel>
            ) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                var mAcceptRejectbookingModel = response.body()!!
                Log.e("Log", mAcceptRejectbookingModel.status.toString())
                if (mAcceptRejectbookingModel.status == 1) {

                    showAlertDialog(mActivity,mAcceptRejectbookingModel.message,position)
                }
            }
        })
    }



    override fun getItemCount(): Int {
        return NotifiList!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageNotificationIV: ImageView
        var textNotificationTV: TextView
        var dateNotificationTV: TextView
        var accept_rejectLL:LinearLayout
        var acceptTV: TextView
        var rejectTV: TextView
        init {

            imageNotificationIV = itemView.findViewById(R.id.imageNotificationIV)
            textNotificationTV = itemView.findViewById(R.id.textNotificationTV)
            dateNotificationTV = itemView.findViewById(R.id.dateNotificationTV)
            accept_rejectLL = itemView.findViewById(R.id.accept_rejectLL)
            acceptTV = itemView.findViewById(R.id.acceptTV)
            rejectTV = itemView.findViewById(R.id.rejectTV)
        }
    }
    fun showAlertDialog(mActivity: Activity?, strMessage: String?, position: Int) {
        val alertDialog = Dialog(mActivity!!)
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        alertDialog.setContentView(R.layout.dialog_alert)
        alertDialog.setCanceledOnTouchOutside(false)
        alertDialog.setCancelable(false)
        alertDialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        // set the custom dialog components - text, image and button
        val txtMessageTV = alertDialog.findViewById<TextView>(R.id.txtMessageTV)
        val btnDismiss = alertDialog.findViewById<TextView>(R.id.btnDismiss)
        txtMessageTV.text = strMessage
        btnDismiss.setOnClickListener { alertDialog.dismiss()
            NotifiList!!.removeAt(position)
            notifyDataSetChanged()
        }
        alertDialog.show()
    }
    /*
    * Show Progress Dialog
    * */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    fun showProgressDialog(mActivity: Activity?) {
        progressDialog = Dialog(mActivity!!)
        progressDialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        progressDialog!!.setContentView(R.layout.dialog_progress)
        Objects.requireNonNull(progressDialog!!.window)!!
            .setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progressDialog!!.setCanceledOnTouchOutside(false)
        progressDialog!!.setCancelable(false)
        if (progressDialog != null) progressDialog!!.show()
    }
    /*
  * Hide Progress Dialog
  * */
    fun dismissProgressDialog() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }
}