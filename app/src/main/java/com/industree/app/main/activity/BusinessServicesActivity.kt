package com.industree.app.main.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.industree.app.R
import com.industree.app.main.Model.VendorBookingListingModel
import com.industree.app.main.Model.vendorDataItem
import com.industree.app.main.Retrofit.RetrofitClient
import com.industree.app.main.adapter.BusinessServAdapter
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import java.util.HashMap

class BusinessServicesActivity : BaseActivity()  {
    @BindView(R.id.bookedServicesRV)
    lateinit var bookedServicesRV: RecyclerView


    @BindView(R.id.allBookingTV)
    lateinit var allBookingTV: TextView

    @BindView(R.id.ongoingBookingTV)
    lateinit var ongoingBookingTV: TextView

    @BindView(R.id.completedBookingTV)
    lateinit var completedBookingTV: TextView

    @BindView(R.id.manageProfileTV)
    lateinit var manageProfileTV: TextView

    @BindView(R.id.backIV)
    lateinit var backIV: ImageView

    var businessServAdapter: BusinessServAdapter? = null
    var bookedServiceItemList: ArrayList<vendorDataItem?>? = ArrayList()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_business_services)
        ButterKnife.bind(mActivity)
        setUpFirstFragment()
    }
    private fun setUpFirstFragment() {
        onAllBookedServiceClick()
    }

    @OnClick(
        R.id.allBookingTV,R.id.ongoingBookingTV,R.id.completedBookingTV,R.id.manageProfileTV,R.id.backIV
    )
    fun onViewClicked(view: View) {
        when (view.id) {
            R.id.allBookingTV -> onAllBookedServiceClick()
            R.id.ongoingBookingTV -> onGoingBookedServiceClick()
            R.id.completedBookingTV -> onCompleteBookedServiceClick()
            R.id.manageProfileTV -> onClickManageProfile()
            R.id.backIV -> onBackPressed()
        }
    }

    private fun onClickManageProfile() {
        val intent = Intent(mActivity, ManageBusinessProfileActivity::class.java)
        intent.putExtra("plan_id",getPlanId())
        startActivity(intent)
    }



    @SuppressLint("UseRequireInsteadOfGet")
    private fun onCompleteBookedServiceClick() {
        completedBookingTV.setBackgroundResource(R.drawable.bg_tab_review)
        ongoingBookingTV.setBackgroundColor(Color.TRANSPARENT)
        allBookingTV.setBackgroundColor(Color.TRANSPARENT)
        if(isNetworkAvailable(mActivity!!)){
            executeBookedServiceRequest("2")
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }



    @SuppressLint("UseRequireInsteadOfGet")
    private fun onGoingBookedServiceClick() {
        ongoingBookingTV.setBackgroundResource(R.drawable.bg_tab_review)
        completedBookingTV.setBackgroundColor(Color.TRANSPARENT)
        allBookingTV.setBackgroundColor(Color.TRANSPARENT)
        if(isNetworkAvailable(mActivity!!)){
            executeBookedServiceRequest("1")
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun onAllBookedServiceClick() {
        allBookingTV.setBackgroundResource(R.drawable.bg_tab_review)
        ongoingBookingTV.setBackgroundColor(Color.TRANSPARENT)
        completedBookingTV.setBackgroundColor(Color.TRANSPARENT)
        if(isNetworkAvailable(mActivity!!)){
            executeBookedServiceRequest("0")
        } else {
            showToast(mActivity, getString(R.string.internet_connection_error))
        }
    }
    private fun mParam(type: String): MutableMap<String?, String?> {
        val mMap: MutableMap<String?, String?> = HashMap()
        mMap["user_id"] = getLoggedInUserID()
        mMap["pageNo"] = "1"
        mMap["perPage"] = "20"
        mMap["latitude"] = getLatitude()
        mMap["longitude"] = getLongitude()
        mMap["type"] = type
        Log.e(TAG, "**PARAM**$mMap")
        return mMap
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun executeBookedServiceRequest(type: String) {
        val mHeaders: MutableMap<String, String> = HashMap()
        mHeaders["Token"] = getAuthToken()
        showProgressDialog(mActivity!!)
        val call = RetrofitClient.apiInterface.VendorBookingListingDataRequest(mHeaders,mParam(type))
        call.enqueue(object : Callback<VendorBookingListingModel> {
            override fun onFailure(call: Call<VendorBookingListingModel>, t: Throwable) {
                Log.e(TAG, t.message.toString())
                dismissProgressDialog()
            }

            override fun onResponse(call: Call<VendorBookingListingModel>, response: Response<VendorBookingListingModel>) {
                Log.e(TAG, response.body().toString())
                dismissProgressDialog()
                val mVendorBookingListingModel : VendorBookingListingModel = response.body()!!
                if (mVendorBookingListingModel!!.status == 1) {
                    bookedServicesRV.visibility= View.VISIBLE
                    bookedServiceItemList=mVendorBookingListingModel.data
                    setAdapter()
                } else if (mVendorBookingListingModel!!.status == 2) {
                    showAlertDialog(mActivity!!, mVendorBookingListingModel.message)
                    // showDoubleButtonAlertDialog(mActivity, mSignInModel.message, editEmailET.text.toString())
                } else if (mVendorBookingListingModel!!.status == 0) {
                    bookedServicesRV.visibility= View.GONE
                    showAlertDialog(mActivity!!, mVendorBookingListingModel.message)
                }else {
                    showAlertDialog(mActivity!!, getString(R.string.internal_server_error))
                }
            }
        })
    }
    @SuppressLint("UseRequireInsteadOfGet")
    private fun setAdapter() {
        if (mActivity!=null){
            val layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(mActivity!!, LinearLayoutManager.VERTICAL, false)
            bookedServicesRV.setLayoutManager(layoutManager)
            businessServAdapter = BusinessServAdapter(mActivity!!,bookedServiceItemList,getLoggedInUserID())
            bookedServicesRV.setAdapter(businessServAdapter)
        }}

}